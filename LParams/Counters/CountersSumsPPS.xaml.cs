﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Windows.Threading;

namespace StClient
{
    /// <summary>
    /// Interaction logic for CountersSumsPPS.xaml
    /// </summary>
    public partial class CountersSumsPPS : UserControl
    {


        CounterSumsLogic _cslogic;

        public CountersSumsPPS(CounterSumsLogic cslogic)
        {

            InitializeComponent();
            _cslogic = cslogic;
            this.DataContext = cslogic;


        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _cslogic.Count = _cslogic.PPS = 0;
        }
    }

   

    public class CounterSumsLogic : INotifyPropertyChanged
    {
        DispatcherTimer ppsTimer = new DispatcherTimer();//todo kuku lele sta je ovo tajmera
        public CounterSumsLogic()
        {
            ppsTimer.Interval = TimeSpan.FromMilliseconds(1000);
            ppsTimer.Tick += new EventHandler(ppsTimer_Tick);
            ppsTimer.Start();
        }

        string _name;
        int _count, _countPrevious, _pps;
        public string Names { get { return _name; } set { if (_name != value) { _name = value; OnNotify("Names"); } } }
        public int Count { get { return _count; } set { if (_count != value) { _count = value; OnNotify("Count"); } } }
        public int PPS { get { return _pps; } set { if (_pps != value) { _pps = value; OnNotify("PPS"); } } }

        void ppsTimer_Tick(object sender, EventArgs e)
        {
            PPS = _count-_countPrevious;
            _countPrevious = _count;
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
        #endregion
    }
}
