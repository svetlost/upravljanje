﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using LogAlertHB;


namespace StClient
{
    public partial class DiagGUI : UserControl
    {
        public MotorLogic _motor;
        Commands commands = new Commands();


        public DiagGUI(MotorLogic motor)
        {
            InitializeComponent();

            lbTripHistory.SelectionChanged += new SelectionChangedEventHandler(lbTripHistory_SelectionChanged);
            this.DataContext = motor;
            _motor = motor;

            for (byte i = 0; i < Properties.Settings.Default.NumberOfGroups; i++)
                combo.Items.Add(i.ToString());

            foreach (DiagItemValues diags in _motor.DiagItemsList)
                DiagInfo.Children.Add(new DiagItemUserControl(diags));



            //Binding b = new Binding();
            //b.Source = motor;
            //b.Path = new PropertyPath("TripHistoryGet");
            //lbTripHistory.SetBinding(ListView.ItemsSourceProperty, b);

        }

        void lbTripHistory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //return;//todo 2014

            TripHistoryItem a = lbTripHistory.SelectedItem as TripHistoryItem;
            if (a == null) return;

            var _trip = Sys.StaticListOfTripsLenze9400.Find(t => t.Number == Convert.ToInt32(a.Error));
            if (_trip == null) return;

            //commands.ShowTripInfo(a.ErrorCauseRemedy);
            commands.ShowTripInfo(_trip.Number.ToString());
        }

        private void resetTrip_Click(object sender, RoutedEventArgs e)
        {
            commands.resetTrip(_motor.toList());
        }

        private void Stop_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.Stop(_motor.toList());
        }

        private void resetGroup_Click(object sender, RoutedEventArgs e)
        {
            _motor.GroupNo = -1;
        }

        private void refreshHistory9400_Click(object sender, RoutedEventArgs e)
        {
            _motor.ResetTripHistory9400();
        }

        private void resetRef_Click(object sender, RoutedEventArgs e)
        {
            commands.ResetReference(_motor);
        }
    }
}
