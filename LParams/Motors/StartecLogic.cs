﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace StClient
{
    public class StartecLogic : INotifyPropertyChanged
    {

        int _motorID = 10, _row = 0, _column = 0;
        //, _SecBrkControlAddress = 1, _SecBrkControlBit=1;
        short _encodermotorid = 10;
        string _title = "hoist";
        double _cp = 0;
        double _coef = 1;
        long _up = 0, _down = 0;
        //MotorBrake _SecBrkControlLock = MotorBrake.Locked;

        [XmlAttribute("UP")]
        public long Up { get { return _up; } set { _up = value; } }
        [XmlAttribute("DOWN")]
        public long Down { get { return _down; } set { _down = value; } }
        [XmlAttribute("EncoderMotorID")]
        public short EncoderMotorID { get { return _encodermotorid; } set { _encodermotorid = value; } }
        [XmlAttribute("Title")]
        public string Title { get { return _title; } set { if (_title != value) { _title = value; OnNotify("Title"); } } }
        [XmlAttribute("MotorID")]
        public int MotorID { get { return _motorID; } set { _motorID = value; } }

        int _CanAdr, _CanNet;
        string _CabinetTitle;
        [XmlAttribute("CanAdr")]
        public int CanAdr { get { return _CanAdr; } set { _CanAdr = value; } }
        [XmlAttribute("CanNet")]
        public int CanNet { get { return _CanNet; } set { _CanNet = value; } }
        [XmlAttribute("CabinetTitle")]
        public string CabinetTitle { get { return _CabinetTitle; } set { _CabinetTitle = value; } }



        [XmlAttribute("ROW")]
        public int Row { get { return _row; } set { _row = value; } }
        [XmlAttribute("COLUMN")]
        public int Column { get { return _column; } set { _column = value; } }
        [XmlAttribute("COEFICIENT")]
        public double Coeficient { get { return _coef; } set { _coef = value; } }
        /*
        [XmlAttribute("SecBrkControlAddress")]
        public int SecBrkControlAddress { get { return _SecBrkControlAddress; } set { _SecBrkControlAddress = value; } }
        [XmlAttribute("SecBrkControlBit")]
        public int SecBrkControlBit { get { return _SecBrkControlBit; } set { _SecBrkControlBit = value; } }
        

        [XmlIgnore]
        public MotorBrake SecBrkControlLock
        {
            get { return _SecBrkControlLock; }
            set
            {
                if (_SecBrkControlLock != value)
                {
                    MessageBox.Show("zavrsi!!!!!!");
                    _SecBrkControlLock = value;
                    //SecondaryBreakControl sbc = Sys.SecundaryBreakControlList.Find(qq => qq.CanChannel == CanChannel);
                    //sbc.Output = MotorLogic.SetBitInByteArray2(sbc.Output, SecBrkControlBit, value);
                    OnNotify("SecBrkControlLock");
                }
            }
        }
        */
        bool ischkchecked = false;
        [XmlIgnore]
        public bool isCHKchecked { get { return ischkchecked; } set { if (ischkchecked != value) { ischkchecked = value; OnNotify("isCHKchecked"); } } }

        [XmlIgnore]
        public double CP { get { return _cp; } set { if (_cp != value) { _cp = value / Coeficient; OnNotify("CP"); } } }

        public StartecLogic()
        {
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion
    }
}
