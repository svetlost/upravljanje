﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StClient.Wagons;

namespace StClient
{
    /// <summary>
    /// Interaction logic for Vagoni003.xaml
    /// </summary>
    public partial class Vagoni003 : UserControl
    {
       
        VagonLogic003 vl;

        public Vagoni003(VagonLogic003 vagon)
        {
            InitializeComponent();

            this.DataContext = vl = vagon;

        }
        public Vagoni003()
        {
            InitializeComponent();
        }

        private void ToggleBB_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { ((ToggleButton)sender).IsChecked = true; }
        private void ToggleBB_MouseUp(object sender, MouseButtonEventArgs e) { ((ToggleButton)sender).IsChecked = false; }
    }
}
