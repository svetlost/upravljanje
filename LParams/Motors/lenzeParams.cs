﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace StClient
{
    class lenzeParams : INotifyPropertyChanged
    {

        class Device : INotifyPropertyChanged
        {
            double _canAdr;
            [XmlAttribute("canAdr")]
            public double canAdr { get { return _canAdr; } set { if (_canAdr != value) { _canAdr = value; OnNotify("canAdr"); } } }
            string _name;
            [XmlAttribute("name")]
            public string name { get { return _name; } set { if (_name != value) { _name = value; OnNotify("name"); } } }

            #region INotifyPropertyChanged Members
            public event PropertyChangedEventHandler PropertyChanged;

            private void OnNotify(String parameter)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(parameter));
                }
            }
            #endregion
        }

        class Lparam : INotifyPropertyChanged
        {
            double _code;
            [XmlAttribute("code")]
            public double code { get { return _code; } set { if (_code != value) { _code = value; OnNotify("code"); } } }
            double _subCode;
            [XmlAttribute("subCode")]
            public double subCode { get { return _subCode; } set { if (_subCode != value) { _subCode = value; OnNotify("subCode"); } } }
            double _factor;
            [XmlAttribute("factor")]
            public double factor { get { return _factor; } set { if (_factor != value) { _factor = value; OnNotify("factor"); } } }
            string _unit;
            [XmlAttribute("unit")]
            public string unit { get { return _unit; } set { if (_unit != value) { _unit = value; OnNotify("unit"); } } }

            double _currentValue;
            [XmlAttribute("currentValue")]
            public double currentValue { get { return _currentValue; } set { if (_currentValue != value) { _currentValue = value; OnNotify("currentValue"); } } }
            double _newValue;
            [XmlAttribute("newValue")]
            public double newValue { get { return _newValue; } set { if (_newValue != value) { _newValue = value; OnNotify("newValue"); } } }


            #region INotifyPropertyChanged Members
            public event PropertyChangedEventHandler PropertyChanged;

            private void OnNotify(String parameter)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(parameter));
                }
            }
            #endregion
        }


        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion
    }
}
