﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Threading;

namespace StClient
{
    class ApplicationStateLogic : INotifyPropertyChanged
    {
        private static volatile ApplicationStateLogic instance;
        private static object syncRoot = new Object();

        //bool _isappenabled = false;
        bool _isappenabled = true;  //todo vrati na false test 20130929
        public bool isAppEnabled { get { return _isappenabled; } set { if (_isappenabled != value) { _isappenabled = value; OnNotify("isAppEnabled"); } } }

        private ApplicationStateLogic()
        { }

        public static ApplicationStateLogic Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new ApplicationStateLogic();
                    }
                }

                return instance;
            }
        }

       


        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }     

        #endregion
    }
}
