﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using LogAlertHB;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Media;
using System.Diagnostics;

namespace StClient
{

    public class Commands
    {
        static string Message;
        //PopUpWindow popupWin;

        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;

        //Point rootPoint;
        double coef = 1;
        double tempMV;
        double Tollerance = Properties.Settings.Default.PositionTollerance; //ovde

        public void AutoStarting(List<MotorLogic> Motors, object Iclass)
        //public void AutoStarting(List<MotorLogic> Motors, ISpSv _iSpSv, ISyncIMotorsList _iCalcEnabled)
        {
            if (Motors.Count == 0) return;

            //foreach (MotorLogic motor in Motors)
            //{
            //    //Sys.printStack(motor);
            //    //motor.iSpSv = _iSpSv;
            //    //motor.calculatesEnabledType = _iCalcEnabled;

            //    motor.iSpSv = (ISpSv)Iclass;
            //    //if (Iclass.GetType() == typeof(CueItem)) //stimung 
            //    //    motor.calculatesEnabledType = (ISyncIMotorsList)CueLogic.UniqueInstance;
            //    //else
            //    //{
            //    //    motor.calculatesEnabledType = (ISyncIMotorsList)Iclass;
            //    //    motor.iMv = (IMv)Iclass; //todo todo 20110221 ja ga skinuo (kakve veze ima mv sa cue) pa ga onda i vratio
            //    //    //motor.calculatesEnabledType
            //    //}


            //    if (Math.Abs(motor.iSpSv.SP - motor.CP) > Tollerance && Motors.All(qq => qq.calculatesEnabledType.autoEnabled))
            //    {
            //        motor.SbcLock = MotorBrake.Unlocked;
            //        //coef = motor.calculatesEnabledType.IsSync ? motor.calculatesEnabledType.SyncValue / motor.MaxV : 1;
            //        coef = 1;
            //        //coef = motor.calculatesEnabledType.IsSync ? motor.calculatesEnabledType.SyncValue / motor.MaxV : 1;
            //        UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.SpIndex, CanSett.SpSubIndex, motor.iSpSv.SP, SdoPriority.hi);
            //        UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.SvIndex, CanSett.SvSubIndex, motor.iSpSv.SV * coef, SdoPriority.hi);
            //        UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandAuto, CanSett.SdoProcTime * 2, 8);
            //        //Debug.WriteLine(string.Format("AUTO: {2}, MOTOR: {0}, SP:{1}, STATE: {3}, BRZINA: {4}", motor.MotorID, motor.positionVelocitySetType.SP, motor.GroupNo, motor.state, motor.positionVelocitySetType.SV * coef));
            //        motor.state = states.AutoStarting2;
            //    }
            //    else
            //        motor.state = states.Idle;

            //    //motor.autoStartedTimeStamp = DateTime.Now;
            //    Message += string.Format(" Motor={0}, ID={1}, SP={2} SV={3} CP={4};", motor.Title, motor.MotorID, motor.iSpSv.SP, motor.iSpSv.SV, motor.CP);
            //    //Sys.printStack(motor);
            //}
            if (Message != "")
            {
                Log.Write("SM(AutoStarting). " + Message, EventLogEntryType.Information);
                Message = "";

            }
        }

        //public void Stop(List<MotorLogic> motorsList)
        //{
        //    //if (motorsList.Count > 0 && motorsList.All(qq => qq.calculatesEnabledType.stopEnabled))
        //    //{
        //    //    foreach (MotorLogic motor in motorsList)
        //    //    {
        //    //        Stopping(motor);
        //    //        Message += string.Format(" Motor={0}, MotID={1}, CP={2}, CV={3},", motor.Title, motor.MotorID, motor.CP, motor.CV);
        //    //    }


        //    //    if (Message != "")
        //    //    {
        //    //        Log.Write("SM(Stopping)." + Message, EventLogEntryType.Information);
        //    //        Message = "";
        //    //    }
        //    //}
        //}

        //public void ManualStarting(List<MotorLogic> motorsList, object Iclass)
        //{
        //    if (motorsList.Count == 0 || motorsList.Any(qq => qq.iMotorIndicators.manEnabled == false)) return;

        //    foreach (MotorLogic motor in motorsList)
        //    {
        //        motor.iMv = (IMv)Iclass;

        //        if (motor.iMv == null) continue;

        //        motor.SbcLock = MotorBrake.Unlocked;

        //        //if (Iclass.GetType() == typeof(StJoystick))
        //        //{
        //        //    motor.calculatesEnabledType = Sys.StaticGroupList[((StJoystick)Iclass).Group];
        //        //    UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8); // ovo je button joystika //02092010 darko vratio, cima nema. prvo mora da se posalje man, pa manpos/manneg
        //        //    motor.PreviousDirection = CanSett.CanCommandStop;
        //        //    motor.state = states.JoyStarting;
        //        //}
        //        //else
        //        //{
        //        //    motor.calculatesEnabledType = (ISyncIMotorsList)Iclass;
        //        //    motor.iMotorIndicators.ManTimeOut = DateTime.Now + TimeSpan.FromSeconds(5);
        //        //    //coef = motor.calculatesEnabledType.IsSync ? motor.calculatesEnabledType.SyncValue / motor.MaxV : 1;//20140516 ne treba vise da se racuna, 9400 radi u cm/s
        //        //    coef = 1;
        //        //    tempMV = coef * Math.Abs(motor.iMv.MV);
        //        //    UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.MvIndex, CanSett.MvSubIndex, tempMV, SdoPriority.hi);
        //        //    //UdpTx.UniqueInstance.SendPDO(motor.MotorID, motor.iMv.Direction, 0, 5);  //todo 20140509 povecan dlc na 8 zbog 9400
        //        //    UdpTx.UniqueInstance.SendPDO(motor.MotorID, motor.iMv.Direction, 0, 8);
        //        //    motor.state = states.ManStarted2;
        //        //}
        //        Debug.WriteLine(string.Format("manstarting    GRUPA: {2}, MOTOR: {0}, PRAVAC:{1}, manv: {3}", motor.MotorID, motor.iMv.Direction, motor.GroupNo, tempMV));

        //        Message += string.Format(" Motor={0}, MotID={1}, CP={2};", motor.Title, motor.MotorID, motor.CP);
        //    }

        //    if (Message != "")
        //    {
        //        Log.Write("SM(ManualStarting)." + Message, EventLogEntryType.Information);
        //        Message = "";
        //    }
        //}

        //public void Up_PreviewMouseLeftButtonDown(List<MotorLogic> motorsList)
        //{
        //    foreach (MotorLogic motor in motorsList)
        //    {
        //        if (motorsList.Count > 0 && motorsList.All(qq => qq.iMotorIndicators.manPosNegEnabled))
        //        {
        //            UdpTx.UniqueInstance.SendPDO(motor.MotorID, motor.PosDirection, 0, 8);
        //            motor.state = states.ManStarted2;
        //            Message += string.Format(" Motor={0}, MotID={1}, CP={2}, CV={3};", motor.Title, motor.MotorID, motor.CP, motor.CV);
        //        }
        //    }
        //    if (Message != "")
        //    {
        //        Log.Write("(Up_PreviewMouseLeftButtonDown)." + Message, EventLogEntryType.Information);
        //        Message = "";
        //    }
        //}

        //public void Up_PreviewMouseLeftButtonUp(List<MotorLogic> motorsList)
        //{
        //    foreach (MotorLogic motor in motorsList)
        //    {
        //        ResetTimer(motor);
        //        Message += string.Format(" Motor={0}, MotID={1}, CP={2}, CV={3};", motor.Title, motor.MotorID, motor.CP, motor.CV);
        //    }
        //    if (Message != "")
        //    {
        //        Log.Write("(Up_PreviewMouseLeftButtonUp)." + Message, EventLogEntryType.Information);
        //        Message = "";
        //    }
        //}

        //public void Down_PreviewMouseLeftButtonDown(List<MotorLogic> motorsList)
        //{
        //    foreach (MotorLogic motor in motorsList)
        //    {
        //        if (motorsList.All(qq => qq.iMotorIndicators.manPosNegEnabled))
        //        {
        //            UdpTx.UniqueInstance.SendPDO(motor.MotorID, motor.NegDirection, 0, 8);
        //            motor.state = states.ManStarted2;
        //            Message += string.Format(" Motor={0}, MotID={1}, CP={2}, CV={3};", motor.Title, motor.MotorID, motor.CP, motor.CV);
        //        }
        //    }

        //    if (Message != "")
        //    {
        //        Log.Write("(Down_PreviewMouseLeftButtonDown)." + Message, EventLogEntryType.Information);
        //        Message = "";
        //    }
        //}

        //public void Down_PreviewMouseLeftButtonUp(List<MotorLogic> motorsList)
        //{
        //    foreach (MotorLogic motor in motorsList)
        //    {
        //        ResetTimer(motor);
        //        Message += string.Format(" Motor={0}, MotID={1}, CP={2}, CV={3};", motor.Title, motor.MotorID, motor.CP, motor.CV);
        //    }

        //    if (Message != "")
        //    {
        //        Log.Write("(Down_PreviewMouseLeftButtonUp)." + Message, EventLogEntryType.Information);
        //        Message = "";
        //    }
        //}

        //public void SP_Popup(List<MotorLogic> motorsList, TextBlock sender, bool isSenderCue)
        //{
        //    //transform = sender.TransformToAncestor(App.Current.MainWindow);
        //    //rootPoint = transform.Transform(new Point(0, 0));
        //    if (PopupAlreadyOpen()) return;
        //    //if (!isSenderCue)
        //    //{
        //    //    if (motorsList.Count > 0 && motorsList.All(qq => qq.iMotorIndicators.manEnabled))
        //    //        popupWin = new PopUpWindow(motorsList.Min(t => t.LimNeg), motorsList.Max(t => t.LimPos), sender, 500, 500);
        //    //}
        //    //else
        //    //    popupWin = new PopUpWindow(motorsList.Max(t => t.LimNeg), motorsList.Min(t => t.LimPos), sender, 500, 500);

        //    //todo 20140522 vidista se ovde desava. ako su dva uredjaja sa neukljucujucim domenima, ondaje problem. da se klipuje vrednost, i ispise alert?????

        //}

        //public void SVMV_Popup(List<MotorLogic> motorsList, TextBlock sender, bool isSenderCue, SyncMode _syncMode, ShowPercButtons showPercButtons)
        //{
        //    if (PopupAlreadyOpen()) return;
        //    if (motorsList.Count < 1) return;

        //    double _maxSV;
        //    if (_syncMode == SyncMode.NoSync) _maxSV = motorsList.Max(q => q.MaxV);
        //    else _maxSV = motorsList.Min(q => q.MaxV);

        //    //if (isSenderCue)
        //    //    popupWin = new PopUpWindow(_maxSV * .01, _maxSV, sender, 500, 500, ShowPercButtons.Show);
        //    //else
        //    //    if (motorsList.All(qq => qq.iMotorIndicators.manEnabled))
        //    //        popupWin = new PopUpWindow(_maxSV * .01, _maxSV, sender, 500, 500, ShowPercButtons.Show);

        //}

        //public void SVMV_MotorGui_Popup(List<MotorLogic> motorsList, TextBlock sender, bool isSenderCue, SyncMode _syncMode)
        //{
        //    if (PopupAlreadyOpen()) return;
        //    if (motorsList.Count < 1) return;

        //    int _maxSV;
        //    if (_syncMode == SyncMode.NoSync) _maxSV = Convert.ToInt32(motorsList.Max(q => q.MaxV));
        //    else _maxSV = Convert.ToInt32(motorsList.Min(q => q.MaxV));

        //    if (isSenderCue)
        //        popupWin = new PopUpWindow(_maxSV * .01, _maxSV, sender, 500, 500);
        //    else
        //        if (motorsList.All(qq => qq.iMotorIndicators.manEnabled))
        //            popupWin = new PopUpWindow(_maxSV * .01, _maxSV, sender, 500, 500);

        //}


        public void SV_Vagon_Popup(TextBlock sender)
        {
            if (PopupAlreadyOpen()) return;
            //popupWin = new PopUpWindow(1, 100, sender, 500, -700);
        }
        public void SP_Rot_Popup(TextBlock sender)
        {
            if (PopupAlreadyOpen()) return;
            //popupWin = new PopUpWindow(-5000, 5000, sender, 500, -700);
            //string t = sender.Text;
            //sender.Text = "0";
            //sender.Text = t;
        }
        public void ShowTripInfo(string tripTxt)
        {
            if (PopupAlreadyOpen()) return;
            //PopUpWindowText win = new PopUpWindowText(500, 500, "Trip info", tripTxt);
        }
        bool PopupAlreadyOpen() { return (App.Current.Windows.Count > 1); }
        public void PopupCommand(TextBlock sender, bool isString)
        {
            //transform = sender.TransformToAncestor(App.Current.MainWindow);
            //rootPoint = transform.Transform(new Point(0, 0));
            //if (!PopupAlreadyOpen())
            //    popupWin = new PopUpWindow(sender, isString, 500, 500);
        }

        //public void resetTrip(List<MotorLogic> motorsList)
        //{
        //    foreach (MotorLogic motor in motorsList)
        //    {
        //        UdpTx.UniqueInstance.SendSDOWriteRQ_Raw(motor.MotorID, CanSett.TripRstIndex, CanSett.TripRstSubIndex, 43, SdoPriority.hi);
        //        //20140512 dole je ya 9300
        //        //UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.TripRstIndex, CanSett.TripRstSubIndex, 0, SdoPriority.hi);
        //        Message += string.Format(" Motor={0}, MotID={1}, CP={2};", motor.Title, motor.MotorID, motor.CP);
        //    }
        //    if (Message != "")
        //    {
        //        Log.Write("(resetTrip)." + Message, EventLogEntryType.Information);
        //        Message = "";
        //    }
        //}

        //public void resetTrip(KompenzacionaLogic003 komp)
        //{
        //    UdpTx.UniqueInstance.SendPDORaw(512 + komp.logic.Addr, Sys._KompenzacioneCanNetworkId, new byte[8] { 1 << 2, 0, 0, 0, 0, 0, 0, 0 }, 0, 1);
        //    UdpTx.UniqueInstance.SendPDORaw(512 + komp.logic.Addr, Sys._KompenzacioneCanNetworkId, new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 }, 500, 1);
        //}
        //public void resetTripMouseUp(KompenzacionaLogic003 komp)
        //{
        //    //UdpTx.UniqueInstance.SendPDORaw(512 + komp.logic.Addr, Sys._KompenzacioneCanNetworkId, new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 }, 0, 1);
        //}

        //public void removeLimits(MotorLogic motor)
        //{
        //    UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.LimPosIndex, CanSett.LimSubIndex, motor.LimPos + 3000, SdoPriority.hi);
        //    Log.Write(string.Format("(Remove Limits). limpos moved to ={0}, Motor={1}, MotID={2}, CP={3}", motor.LimPos + 3000, motor.Title, motor.MotorID, motor.CP), EventLogEntryType.Information);

        //    UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.LimNegIndex, CanSett.LimSubIndex, motor.LimNeg - 3000, SdoPriority.hi);
        //    Log.Write(string.Format("(Remove Limits). limneg moved to ={0}, Motor={1}, MotID={2}, CP={3} ", motor.LimNeg - 3000, motor.Title, motor.MotorID, motor.CP), EventLogEntryType.Information);

        //    motor.LimNegFromServo = motor.LimPosFromServo = double.NaN;
        //}

        //public void setLimits(MotorLogic motor)
        //{
        //    UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.LimPosIndex, CanSett.LimSubIndex, motor.LimPos, SdoPriority.hi);
        //    Log.Write(string.Format("(Set Limits to normal). limpos set to ={0}, Motor={1}, MotID={2}, CP={3}", motor.LimPos, motor.Title, motor.MotorID, motor.CP), EventLogEntryType.Information);

        //    UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.LimNegIndex, CanSett.LimSubIndex, motor.LimNeg, SdoPriority.hi);
        //    Log.Write(string.Format("(Set Limits to normal). limneg set to ={0}, Motor={1}, MotID={2}, CP={3}", motor.LimNeg, motor.Title, motor.MotorID, motor.CP), EventLogEntryType.Information);

        //    motor.LimNegFromServo = motor.LimPosFromServo = double.NaN;
        //}

        //public void ReadLimitsFromServo(MotorLogic motor)
        //{
        //    UdpTx.UniqueInstance.SendSDOReadRQ(motor.MotorID, CanSett.LimPosIndex, CanSett.LimSubIndex, SdoPriority.lo);
        //    UdpTx.UniqueInstance.SendSDOReadRQ(motor.MotorID, CanSett.LimNegIndex, CanSett.LimSubIndex, SdoPriority.lo);
        //}




        //public void ResetGroup(MotorGUI motorGUI)
        //{
        //    try
        //    {
        //        motorGUI._motor.GroupNo = -1;
        //        Log.Write(string.Format("(ResetGroup MotorGUI). Motor={0}, MotID={1}, CP={2}", motorGUI._motor.Title, motorGUI._motor.MotorID, motorGUI._motor.CP), EventLogEntryType.Information);
        //        //if (Sys.MotorListbox.SelectedItem == motorGUI)
        //        //    Sys.phitem.ifkit.outputs[0] = Sys.phitem.ifkit.outputs[1] = Sys.phitem.ifkit.outputs[2] = Sys.phitem.ifkit.outputs[3] =
        //        //                  Sys.phitem.ifkit.outputs[4] = Sys.phitem.ifkit.outputs[5] = Sys.phitem.ifkit.outputs[6] = Sys.phitem.ifkit.outputs[7] = false;

        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Write(ex.Message, EventLogEntryType.Error);
        //    }
        //}

        //public void ResetGroup(GroupGUI groupGui)
        //{
        //    string s = "";
        //    try
        //    {
        //        for (int i = groupGui._group.MotorsInGroup.Count - 1; i >= 0; i--)
        //        {
        //            s += string.Format(" Motor={0}, MotID={1}, CP={2};", groupGui._group.MotorsInGroup[i].Title, groupGui._group.MotorsInGroup[i].MotorID, groupGui._group.MotorsInGroup[i].CP);
        //            groupGui._group.MotorsInGroup[i].GroupNo = -1;
        //        }
        //        Log.Write(string.Format("(ResetGroup GroupGUI). " + s), EventLogEntryType.Information);
        //        //if (Sys.GroupListBox.SelectedItem == groupGui && Sys.phitem != null)
        //        //{
        //        //    Sys.phitem.ifkit.outputs[0] = Sys.phitem.ifkit.outputs[1] = Sys.phitem.ifkit.outputs[2] = Sys.phitem.ifkit.outputs[3] =
        //        //                  Sys.phitem.ifkit.outputs[4] = Sys.phitem.ifkit.outputs[5] = Sys.phitem.ifkit.outputs[6] = Sys.phitem.ifkit.outputs[7] = false;
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //}

        //DispatcherTimer SbcResetRefTimer;
        //MotorLogic ResetRefMotor;
        //public void ResetReference(MotorLogic motor)
        //{
        //    //if (MessageBox.Show("Do you want to reset reference?\n\nFor authorized personall only!\nWARNING!\nIncorrect reference can lead to unit malfunction or damage.",
        //    //     "Confirm reset reference", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
        //    PopUpWindowText pwin = new PopUpWindowText(500, 500, "Reset reference", "Do you want to reset reference?\n\nFor authorized personall only!\nWARNING!\nIncorrect reference can lead to unit malfunction or damage.");
        //    if (pwin.DialogResult == true)
        //    {
        //        double oldCP = motor.CP;
        //        //motor.SbcLock = MotorBrake.Unlocked;//022011 da li ovo treba ovde i kako da se otkoci ky3
        //        //022011 imitira se hmi reset ref, tj plc otkoci ky3
        //        //motor.state = states.ResRef;
        //        motor.SbcLock = MotorBrake.Unlocked;//dodo 20140515
        //        //UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandRefReset, 1000, 8);
        //        //UdpTx.UniqueInstance.SendPDO_resRef(motor.MotorID, CanSett.CanCommandRefReset, 1000, 8);
        //        UdpTx.UniqueInstance.SendPDO_resRef(motor.MotorID, 1 << 6, 1000, 8);
        //        UdpTx.UniqueInstance.SendPDO_resRef(motor.MotorID, 0, 2500, 8);
        //        TimedAction.ExecuteWithDelay(new Action(delegate { motor.SbcLock = MotorBrake.Locked; }), TimeSpan.FromSeconds(3));


        //        Log.Write(string.Format("(ResetReference). Motor={0}, MotID={1}, newCP={2}, oldCP{3}", motor.Title, motor.MotorID, motor.CP, oldCP), EventLogEntryType.Information);
        //    }
        //}

        //public void ResetReference(MotorLogic motor)
        //{
        //    if (MessageBox.Show("Do you want to reset reference?\n\nFor authorized personall only!\nWARNING!\nIncorrect reference can lead to unit malfunction.",
        //         "Confirm reset reference", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
        //    {
        //        //SbcResetRefTimer = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(6) };
        //        //SbcResetRefTimer.Tick += new EventHandler(SbcResetRefTimer_Tick);
        //        motor.SbcLock = MotorBrake.Unlocked;//todo022011 da li ovo treba ovde i kako da se otkoci ky3
        //        //ResetRefMotor = motor;
        //        //SbcResetRefTimer.Start();

        //        motor.state = states.ResRef;
        //        UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanStructure.CanCommandRefReset, 1000, 2);
        //        if (motor.MotorID == 1) Debug.WriteLine(string.Format("{0:hh:mm:ss.fff} pdo res ref sent", DateTime.Now));
        //        //UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanStructure.CanCommandRefReset, 0, 2);
        //        UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanStructure.CanCommandIdle, 4000, 2);
        //        if (motor.MotorID == 1) Debug.WriteLine(string.Format("{0:hh:mm:ss.fff} pdo idle sent", DateTime.Now));


        //        Log.Write("Reference Reset activated by user. Motor: " + motor.Title);
        //    }
        //}


        //void SbcResetRefTimer_Tick(object sender, EventArgs e)
        //{
        //    ResetRefMotor.SecBrkControlLock = MotorBrake.Locked;
        //    SbcResetRefTimer.Stop();
        //}

        public void ProcessKeyInput(string key)
        {
            if (Sys.receivedTxtBlock != null)
            {
                Sys.receivedTxtBlock.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate()
                            {
                                TextComposition textComposition = new TextComposition(InputManager.Current, Sys.receivedTxtBlock, key);
                                TextCompositionEventArgs e = new TextCompositionEventArgs(Keyboard.PrimaryDevice, textComposition);
                                e.RoutedEvent = UIElement.TextInputEvent;
                                Sys.receivedTxtBlock.RaiseEvent(e);
                                Sys.receivedTxtBlock.Focus();

                            });
            }
        }

        public void ProcessKeyInput(Key key)
        {
            if (Sys.receivedTxtBlock != null)
            {
                Sys.receivedTxtBlock.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate()
                           {
                               KeyEventArgs args = new KeyEventArgs(Keyboard.PrimaryDevice, PresentationSource.FromVisual(Sys.receivedTxtBlock), 0, key);
                               args.RoutedEvent = UIElement.KeyDownEvent;
                               Sys.receivedTxtBlock.RaiseEvent(args);
                               Sys.receivedTxtBlock.Focus();
                           });
            }
        }

        //public void ResetTimer(MotorLogic motor)
        //{
        //    motor.iMotorIndicators.ManTimeOut = DateTime.Now + TimeSpan.FromSeconds(5);
        //    UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8);
        //    motor.state = states.ManStarted2;
        //}

        //public void Stopping(MotorLogic motor)
        //{
        //    motor.SbcLock = MotorBrake.Unlocked;
        //    motor.state = states.Stopping1;
        //    UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8);
        //    //Debug.WriteLine(string.Format("GRUPA: {2}, MOTOR: {0}, PRAVAC:{1}, STATE: {3}", motor.MotorID, motor.manualVelocityType.Direction, motor.GroupNo, motor.state));
        //}
        //public void Stopping2(MotorLogic motor)
        //{
        //    motor.SbcLock = MotorBrake.Unlocked;
        //    motor.state = states.Stopping1;
        //    //UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8);
        //    //Debug.WriteLine(string.Format("GRUPA: {2}, MOTOR: {0}, PRAVAC:{1}, STATE: {3}", motor.MotorID, motor.manualVelocityType.Direction, motor.GroupNo, motor.state));
        //}

        //public void StopAllMotors()//todo20131228 kandidat za brisanje. 20140413 nkk all stop?????
        //{
        //    foreach (MotorLogic motor in Sys.StaticMotorList.Where(m => m.Brake == false))
        //        Stopping(motor);

        //    //KompenzacioneRxTx.TotalStop();


        //    //20140512radovic
        //    //Sys.vagoniRxTx.TotalStop();
        //    //Sys.vagoniRxTx.rotStop();

        //}








    }
}