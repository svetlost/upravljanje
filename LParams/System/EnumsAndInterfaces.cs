﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace StClient
{
    public enum states { Idle, AutoStarting2, Started, ManStarted2, Stopping1, Stopped, Error, WaitForBrake, JoyStarting, JoyStarting2, ResRef }//ManPos, ManNeg,, ManResetTimer ManStarting,ManStarted,AutoStarting,
    public enum CueDelegateStates { Add, Remove, Clear }
    public enum SdoPriority { lo, med, hi }
    public enum SdoTimeOut { lo = 5000, med = 800, hi = 600 }
    public enum SdoType { read, write, write2byte };
    public enum MotorBrake { Locked = 0, Unlocked = 1 } //0,1
    // public enum Movement { SingleOrGroup, Cues, NoMovement }
    //enum SBCstates { OK, Erroring, Error }
    public enum CreateDefaultRow { Create, DontCreate };
    public enum CueUpdateAction { Add, Remove };
    public enum SM_State { SM_Idle, SM_Up, SM_Down, SM_Stop }
    public enum SyncMode { Sync, NoSync }
    public enum ShowPercButtons { Show, NoShow }
    public enum LogEntryType { Info = 719, Warning = 721, Init = 761, Action = 765, Error = 767, Critical = 779 }

    public interface IMotorIndicators : INotifyPropertyChanged
    {
        bool RefOk { get; set; }
        bool Local { get; set; }
        bool Can { get; set; }
        bool Inh28 { get; set; }
        bool manPosNegEnabled { get; set; }
        bool SpSvEnabled { get; set; }
        bool manEnabled { get; set; }
        DateTime ManTimeOut { get; set; }
        void CalculateEnableds();
    }

    public interface IInitResult
    {
        string initResult { get; set; }
    }

    public interface ISyncIMotorsList
    {
        bool Trip { get; set; }
        bool Moving { get; set; }
        bool Brake { get; set; }
        bool autoEnabled { get; set; }
        bool stopEnabled { get; set; }
        bool IsSync { get; set; }
        double SyncValue { get; set; }
        //List<MotorLogic> ListOfMotors();
    }

    public interface IMv  //for Single , Group  or joystick
    {
        double MV { get; set; }
        int Direction { get; set; }
    }

    public interface ISpSv // for single,Group or CueItem
    {
        double SP { get; set; }
        double SV { get; set; }
    }
    //public interface IStopGo // for single,Group or CueItem
    //{
    //    double SV { get; set; }
    //    string Title { get; set; }
    //    bool Enable { get; set; }
    //    bool Stop { get; set; }
    //    bool Go { get; set; }
    //    //VoidNoArg StopDelegate;
    //    //VoidNoArg GoDelegate;
    //}
    public interface ICpCv // for single,Group or CueItem
    {
        double CP { get; set; }
        double CP_Absolute { get; }
        double CV { get; set; }
    }
    public interface ILock // for single,Group or CueItem
    {
        bool Lock { get; set; }
    }






}
