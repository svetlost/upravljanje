﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace StClient
{
    class ListIsolator : IEnumerable

    {
        internal class IterIsolateEnumerator: IEnumerator
		{
			protected ArrayList items;
			protected int currentItem;

			internal IterIsolateEnumerator(IEnumerator enumerator)
			{
					// if this is the enumerator from another iterator, we
					// don't have to enumerate it; we'll just steal the arraylist
					// to use for ourselves. 
				IterIsolateEnumerator chainedEnumerator = 
					enumerator as IterIsolateEnumerator;

				if (chainedEnumerator != null)
				{
					items = chainedEnumerator.items;					
				}
				else
				{
					items = new ArrayList();
					while (enumerator.MoveNext() != false)
					{
						items.Add(enumerator.Current);
					}
					IDisposable disposable = enumerator as IDisposable;
					if (disposable != null)
					{
						disposable.Dispose();
					}
				}
				currentItem = -1;
			}

			public void Reset()
			{
				currentItem = -1;
			}

			public bool MoveNext()
			{
				currentItem++;
				if (currentItem == items.Count)
					return false;

				return true;
			}

			public object Current
			{
				get
				{
					return items[currentItem];
				}
			}
		}

		/// <summary>
		/// Create an instance of the IterIsolate Class
		/// </summary>
		/// <param name="enumerable">A class that implements IEnumerable</param>
        public ListIsolator(IEnumerable enumerable)
		{
			this.enumerable = enumerable;
		}

		public IEnumerator GetEnumerator()
		{
			return new IterIsolateEnumerator(enumerable.GetEnumerator());
		}

		protected IEnumerable enumerable;

    }
}
