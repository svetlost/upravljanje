﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Diagnostics;
using System.Collections;

namespace StClient
{
    public class SecondaryBreakControl
    {
        byte _canChannel = 3;
        int _plcmessagecanID = 33;
        byte[] _output = new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 };

        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;

        [XmlAttribute("CAN_BUS")]
        public byte CanChannel { get { return _canChannel; } set { _canChannel = value; } }
        [XmlAttribute("SBCPLCMessageCanID")]
        public int PlcMessageCanID { get { return _plcmessagecanID; } set { _plcmessagecanID = value; } }

        [XmlIgnore]
        public byte[] PDO_CanMsgID { get { return BitConverter.GetBytes(CanSett.PDO3_RX_BaseID + PlcMessageCanID-1); } }//todo 20110223 promenjeno, gadjalo se sa sbc dio1
        //yakrpljeno sa -1   20140512 ulepsati todo

        [XmlIgnore]
        public byte[] Output
        {
            get { return _output; }
            set
            {
                //if (_output != value)
                //    _output = value; 
                if (!_output.SequenceEqual(value))
                    Buffer.BlockCopy(value, 0, _output, 0, 8);
                UdpTx.UniqueInstance.SendPDOSBC(CanChannel, PDO_CanMsgID, value);

                //Debug.WriteLine(string.Format("{0:hh:mm:ss.fff} canCh: {1}, PDO:{2} sbcOutput:{3,10}", DateTime.Now, CanChannel, Convert.ToString(BitConverter.ToInt32(PDO_CanMsgID, 0), 2), Convert.ToString(BitConverter.ToInt32(value, 0), 2)));

            }
        }


        //public byte[] SetBitInByteArray(byte[] _in, int pos, bool value)
        //{
        //    byte byteToChange = _in[Convert.ToInt32(pos / 8)];
        //    byte maskAND = (byte)~(1 << (pos % 8));
        //    byte maskOR = (byte)(1 << (pos % 8));
        //    if (value)
        //        _in[Convert.ToInt32(pos / 8)] = (byte)(byteToChange | maskOR);
        //    else
        //        _in[Convert.ToInt32(pos / 8)] = (byte)(byteToChange & maskAND);
        //    return _in;
        //}

    }
}
