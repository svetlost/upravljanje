﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Threading;
using System.Net;
using System.Net.Sockets;
using System.Windows;
using System.Collections;
using System.Diagnostics;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Xml.Linq;
using System.IO;
using LogAlertHB;
using StClient.Properties;
//using StClient.Wagons;

namespace StClient
{
    /// <summary>
    /// 
    /// </summary>
    /// 

    public class UdpRx : IInitResult
    {

        DispatcherTimer MessagesOverUDP = new DispatcherTimer();
        //DispatcherTimer VagoniUDP = new DispatcherTimer();
        DispatcherTimer SbcCanCheck = new DispatcherTimer();
        //DispatcherTimer MotorGUIStatesCheck = new DispatcherTimer();
        //SBCstates SBCResponseState = SBCstates.OK;
        ApplicationStateLogic mainWindowEnabled = ApplicationStateLogic.Instance;
        Commands commands = new Commands();

        int RxSocketExceptionCounter;
        //PhidgetItem phitem { get { return Sys.phitem; } }

        int _canAddress, _canChannel;
        int _kompenzacioneCanNetId = CanSettings.Default.KompenzacioneCanNetworkID;

        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;

        //StateMachine stateMachine = new StateMachine();
        static Socket RxSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        //static Socket VagonRxSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        static IPEndPoint ipepClient = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.LocalIp), Properties.Settings.Default.LocalPort);
        //static IPEndPoint ipepVagoni = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.LocalIp), 3510);
        static EndPoint epClient = (EndPoint)ipepClient;
        //static EndPoint epVagoni = (EndPoint)ipepVagoni;
        static byte[] data = new byte[Properties.CanSettings.Default.CanOverUdpMessageLength];
        //static byte[] dataVagoni = new byte[32];
        double Tollerance = Properties.Settings.Default.MovementDetection;

        ServerLink serverMain, serverBackup;
        string serverMainIp = Properties.Settings.Default.ServerMainIP;
        string serverBackupIp = Properties.Settings.Default.ServerBackupIP;
        IPEndPoint tempIpEp;

        //List<MotorLogic> SysMotorList { get { return Sys.StaticMotorList; } }
        //List<DioBrakeCntrl> SysDioBrakeCntrl { get { return Sys.StaticDioList; } }

        //SUMS Panels
        //CounterSumsLogic rXtotal = new CounterSumsLogic(), RXvagoni = new CounterSumsLogic(), unconfiguredRX = new CounterSumsLogic(), UdpRxError = new CounterSumsLogic();
        //public void incrementRXvagoni() { RXvagoni.Count++; }//20140119 todo  srediti ovo

        int PDO2_RX_BaseID;

        public UdpRx()
        {
            MessagesOverUDP.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.CanOverUdpRxTimerInterval);
            MessagesOverUDP.Tick += new EventHandler(MessagesOverUDP_Tick);

            //VagoniUDP.Interval = TimeSpan.FromMilliseconds(500);
            //VagoniUDP.Tick += VagoniUDP_Tick;

            SbcCanCheck.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.SbcCANCheckInterval);
            SbcCanCheck.Tick += new EventHandler(SbcCanCheck_Tick);

            //SumsPPsPanel.Instance.AddPanel(new CountersSumsPPS(rXtotal), 0);
            //SumsPPsPanel.Instance.AddPanel(new CountersSumsPPS(unconfiguredRX), 1);
            //SumsPPsPanel.Instance.AddPanel(new CountersSumsPPS(UdpRxError), 2);
            //SumsPPsPanel.Instance.AddPanel(new CountersSumsPPS(RXvagoni), 6);
            //UdpRxError.Names = "Udp Rx Error";
            //rXtotal.Names = "RX Total";
            //unconfiguredRX.Names = "Unconfigured PDO RX";
            //RXvagoni.Names = "Vagoni UDP RX";

            //MotorGUIStatesCheck.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.CanOverUdpRxTimerInterval);
            //MotorGUIStatesCheck.Tick += new EventHandler(MotorGUIStatesCheck_Tick);

            PDO2_RX_BaseID = CanSett.UseDefaultLenzePdoAddresses ? 641 : 704;

            try
            {
                RxSocket.Bind(ipepClient);
                //VagonRxSocket.Bind(ipepVagoni);
                MessagesOverUDP.Start();
                SbcCanCheck.Start();
                initResult += string.Format("RxSck bound to {0}:{1}", ipepClient.Address, ipepClient.Port);
                //VagoniUDP.Start();
                // MotorGUIStatesCheck.Start();



            }
            catch (Exception e)
            {
                initResult += string.Format("RxSck bind to {0}:{1} FAILED", ipepClient.Address, ipepClient.Port) + e.Message;
                //Log.Write(initResult,EventLogEntryType.Error);
                //AlertLogic.Add(initResult);
            }

            serverMain = Sys.ServersList.Find(q => q.IpAddress == Properties.Settings.Default.ServerMainIP);
            serverBackup = Sys.ServersList.Find(q => q.IpAddress == Properties.Settings.Default.ServerBackupIP);
        }

        int cv, cp, status1, counter;
        bool trip, limneg, limpos, inh, brk;
        //void VagoniUDP_Tick(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        while (VagonRxSocket.Available > 0)
        //        {
        //            VagonRxSocket.ReceiveFrom(dataVagoni, ref epVagoni);

        //            //RX TOTAL COUNT                        
        //            RXvagoni.Count++;

        //            IPEndPoint iii = (IPEndPoint)epVagoni;
        //            int adresa = IPAddress.Parse(iii.Address.ToString()).GetAddressBytes()[3];
        //            if (adresa != 46) continue;

        //            //status1 = dataVagoni[2] + dataVagoni[3] * 256;
        //            counter = dataVagoni[30] + dataVagoni[31] * 256;
        //            cp = dataVagoni[8] + dataVagoni[9] * 256;
        //            //cv = dataVagoni[10] + dataVagoni[11] * 256;
        //            cv = BitConverter.ToInt16(new byte[] { dataVagoni[10], dataVagoni[11] }, 0);
        //            byte[] bbb = new byte[] { dataVagoni[2], dataVagoni[3] };
        //            limneg = GetBitFromByteArray(bbb, 6);
        //            limpos = GetBitFromByteArray(bbb, 7);
        //            inh = GetBitFromByteArray(bbb, 4);
        //            brk = GetBitFromByteArray(bbb, 3);

        //            //Debug.WriteLine(string.Format("time={7} count={8}, ip={0} cp={1} cv={2} limneg={3} limpos={4} inh={5} brk={6}", adresa, cp, cv, limneg, limpos, inh, brk, DateTime.Now.ToLongTimeString(), counter));
        //        }
        //    }
        //    catch { }
        //}

        #region TIMERS

        //Timer To Recalculate SBC States
        void SbcCanCheck_Tick(object sender, EventArgs e)//todo pomeriti ovo na neko bolje mesto 20130115
        {

            //CAN REsponse Check
            //foreach (MotorLogic ml in SysMotorList)
            //{
            //    //Debug.WriteLine(string.Format("mot={0} {1}, time={2}, count={3}, zero={4}", ml.MotorID, ml.Title, DateTime.Now.ToLongTimeString(), ml.CanResponseCount, ml.CanResponseCount == 0 ? "=============" : ""));
            //    if (ml.CanResponseCount > 0)
            //    {
            //        ml.CanResponseCount = 0;
            //        ml.Can = true;
            //    }
            //    else
            //        ml.Can = false;
            //}

            //foreach (var komp in KompenzacioneRxTx.KompList)
            //{
            //    if (komp.logic.CanResponseCount > 0)
            //    {
            //        komp.logic.CanResponseCount = 0;
            //        komp.logic.Can = true;
            //    }
            //    else komp.logic.Can = false;
            //}

            //foreach (var vag in VagoniRxTx.VagoniList)
            //{
            //    if (vag.CanResponseCount > 0)
            //    {
            //        vag.CanResponseCount = 0;
            //        vag.Can = true;
            //    }
            //    else vag.Can = false;
            //}

            foreach (var t in Sys.ServersList) t.CheckLink();
        }



        // RECIVE MESSAGES
        public void MessagesOverUDP_Tick(object sender, EventArgs e)
        {
            //if (mainWindowEnabled.isAppEnabled)//////darko skinuo, pravilo problem kad se izvadi kartica 20101201
            {
                try
                {
                    while (RxSocket.Available > 0)
                    {

                        if (RxSocket.Available > Properties.Settings.Default.UDPbuffer)
                        {
                            Log.Write("UDP RX Buffer Full", EventLogEntryType.Error);
                            AlertLogic.Add("UDP RX Buffer Full - Please Try Restart");
                            //foreach (MotorLogic ml in SysMotorList.FindAll(m => m.CanChannel == _canChannel)) //&&(m.Brake==false) dodao darko 08082010. testirati.SKINUO DARKO
                            //{
                            //    //ml.state = states.Stopping;
                            //    commands.Stopping(ml);
                            //    Log.Write("ERROR: UDP RX Buffer Full - Motor" + ml.CanAddress.ToString() + " STOPPED", EventLogEntryType.Error);
                            //    AlertLogic.Add("ERROR: UDP RX Buffer Full - Motor" + ml.CanAddress.ToString() + " STOPPED");
                            //}
                            break;
                        }
                        RxSocket.ReceiveFrom(data, ref epClient);

                        tempIpEp = (IPEndPoint)epClient;

                        if (tempIpEp.Address.ToString() == serverMainIp) serverMain.IncrementCounter();
                        else if (tempIpEp.Address.ToString() == serverBackupIp) serverBackup.IncrementCounter();

                        //20140609 todo ovo je privremeno resenje. treba generaliyovati da radi i kad ima vise servera
                        //Sys.stServer.ResponseReceived();

                        //RX TOTAL COUNT                        
                        //rXtotal.Count++;
                        //rXtotal.PPS++;


                        _canAddress = ResolveCanAddress(BitConverter.ToInt16(data, 9));
                        _canChannel = data[8];
                        int canID = data[10] * 256 + data[9];
                        // receive filter!!!!!!
                        if ((canID > 384 && canID <= 448) || (canID > 641 && canID <= 705) || (canID > 767 && canID <= 833) ||
                            (canID > 1408 && canID <= 1536) || (canID > 1728 && canID <= 1792) ||
                            (canID == 128) || (canID > 128 && canID <= 192))
                        { }
                        else
                        {
                            continue;
                        }

                        //NMT, do nothing
                        if (canID == 0)
                            continue;
                        if (canID == 128)
                        {
                            //foreach (var sd in Sys.DrivePlcList.Where(qw => qw.CanNetwork == _canChannel))
                            //    sd.ResponseReceived(); //update link statuus for driveplc

                            continue;
                        }

                        //20140523 skinuto radovic
                        //if (_canChannel == _kompenzacioneCanNetId)//todo20140115 koji can za kompenzacione????RESENO
                        //{
                        //    if (canID > 1408) continue;//sdo!!!
                        //    if (_canAddress < 16) KompenzacioneRxTx.UniqueInstance.Komp8400Rx(data, _canAddress);
                        //    else KompenzacioneRxTx.UniqueInstance.KompDioRx(data, _canAddress);

                        //    continue;
                        //}
                        //Dio1000 tempdio;
                        //L8400Logic temp8400;
                        MotorLogic tempMotor = Sys.StaticMotorList.Find(mot => (mot.CanAddress == _canAddress) && (mot.CanChannel == _canChannel));
                        //if (tempMotor == null)
                        //{
                        //    tempdio = Sys.DioList.Find(sdf => (sdf.CanAddress == _canAddress) && (sdf.CanNetwork == _canChannel));
                        //    temp8400 = Sys.StaticL8400List.Find(sdf => (sdf.CanAdr == _canAddress) && (sdf.CanNet == _canChannel));
                        //    if (tempdio != null)
                        //    {
                        //        //dio processing
                        //        tempdio.ResponseReceived();

                        //        if (canID == 34 + 384)
                        //        {
                        //            Sys.L8400_PM.LimNeg = !GetBitFromByteArray(data, 1);
                        //            Sys.L8400_PM.LimPos = !GetBitFromByteArray(data, 0);
                        //            Sys.L8400_Hoist1.LimNeg = !GetBitFromByteArray(data, 3);
                        //            Sys.L8400_Hoist1.LimPos = !GetBitFromByteArray(data, 2);
                        //            Sys.L8400_Hoist3.LimNeg = !GetBitFromByteArray(data, 6);
                        //            Sys.L8400_Hoist3.LimPos = !GetBitFromByteArray(data, 7);
                        //        }
                        //        if (canID == 36 + 384)
                        //        {
                        //            Sys.L8400_DC_PROSC1.LimNeg = !GetBitFromByteArray(data, 1);
                        //            Sys.L8400_DC_PROSC1.LimPos = !GetBitFromByteArray(data, 0);
                        //            Sys.L8400_DC_PROSC2.LimNeg = !GetBitFromByteArray(data, 2);
                        //            Sys.L8400_DC_PROSC2.LimPos = !GetBitFromByteArray(data, 3);
                        //            Sys.L8400_DC_PROSC3.LimNeg = !GetBitFromByteArray(data, 5);
                        //            Sys.L8400_DC_PROSC3.LimPos = !GetBitFromByteArray(data, 4);
                        //            Sys.L8400_RC_PROSC1.LimNeg = !GetBitFromByteArray(data, 7);
                        //            Sys.L8400_RC_PROSC1.LimPos = !GetBitFromByteArray(data, 6);
                        //            Sys.L8400_RC_PROSC2.LimNeg = !GetBitFromByteArray(data, 8);
                        //            Sys.L8400_RC_PROSC2.LimPos = !GetBitFromByteArray(data, 9);
                        //        }

                        //        continue;
                        //    }
                        //    else if (temp8400 != null)
                        //    {
                        //        temp8400.ResponseReceived();
                        //        if (canID == _canAddress + 384)
                        //        {
                        //            temp8400.Trip = GetBitFromByteArray(data, 2);
                        //            temp8400.Brake = !GetBitFromByteArray(data, 3);
                        //            temp8400.Inh28 = GetBitFromByteArray(data, 4);
                        //            //temp8400.qsp??? = GetBitFromByteArray(data, 10);
                        //            temp8400.CV = BitConverter.ToInt16(new byte[] { data[2], data[3] }, 0) / 163.84;
                        //            temp8400.Moving = Math.Abs(temp8400.CV) > 0.5;
                        //            temp8400.CanResponseCount++;
                        //            //if (_canAddress == 21) Debug.WriteLine("brk={0} inh28={1} data[2]={2} data[3]={3} data[4]={4} data[5]={5}", temp8400.Trip, temp8400.Inh28, data[2], data[3], data[4], data[5]);
                        //        }

                        //        continue;
                        //    }
                        //    else
                        //    {
                        //        continue;//neither motor nor dio, continue

                        //    }
                        //}
                        //else
                        //{ }//motor data received, contrinue

                        //DioBrakeCntrl dbc = SysDioBrakeCntrl.Find(dio => (dio.CanChannel == _canChannel) && (dio.Address == _canAddress));
                        //if (tempMotor == null && dbc == null)
                        //{
                        //    unconfiguredRX.Count++;
                        //    //unconfiguredRX.PPS++;
                        //    continue;
                        //}

                        //decode sync reply on RX PDO1 || RX PDO2 || RXPDO3
                        if (
                            ((canID > CanSett.PDO1_RX_BaseID) && (canID <= CanSett.PDO1_RX_BaseID + 64)) ||
                            ((canID > PDO2_RX_BaseID) && (canID <= PDO2_RX_BaseID + 64)))
                        {
                            //tempMotor.TempCan = true;
                            //tempMotor.CanResponseCount++;
                            //tempMotor.Trip = GetBitFromByteArray(data, CanSett.StatusBitTrip);
                            //tempMotor.RefOk = GetBitFromByteArray(data, CanSett.StatusBitRefOk);
                            //tempMotor.LimPosReached = GetBitFromByteArray(data, CanSett.StatusBitLimPos);
                            //tempMotor.LimNegReached = GetBitFromByteArray(data, CanSett.StatusBitLimNeg);
                            //tempMotor.Brake = !GetBitFromByteArray(data, CanSett.StatusBitBrake);
                            //tempMotor.Inh28 = GetBitFromByteArray(data, CanSett.StatusBitInh28);
                            //tempMotor.Local = GetBitFromByteArray(data, CanSett.StatusBitLocal);
                            //tempMotor.CP = Math.Round(BitConverter.ToInt32(data, 4) / tempMotor.Coeficient, 2);
                            //tempMotor.CV = .1 * BitConverter.ToInt16(data, 2);
                            //tempMotor.CV = Convert.ToInt32(100 * BitConverter.ToInt16(data, 2) / CanSett.PDO_WordDenominator);
                            //tempMotor.Moving = Math.Abs(tempMotor.previousCP - tempMotor.CP) > 0.05;//20110221 bilo 0.1 , zakucavalo

                            //bool previousMoving;
                            //if (Math.Abs(tempMotor.previousCP - tempMotor.CP) > 0.3)//20110224 BILO 0.1

                            //=============================20140314 stara verzija. promenjeno zato sto na malim brzinam MOV blinka i zaustavi stimung, a zbog malog VP na modulima u starteataru
                            //if (Math.Abs(tempMotor.previousCP - tempMotor.CP) > tempMotor.MaxV * 0.002)//
                            //    tempMotor.Moving = true;
                            //else
                            //    tempMotor.Moving = false;
                            //=============================

                            //bool cpChanged = Math.Abs(tempMotor.previousCP - tempMotor.CP) > tempMotor.MaxV * 0.002;

                            //tempMotor.Moving = cpChanged;

                            /* darko skinuo 20140514 radovic
                            if (!tempMotor.Moving && !cpChanged) { tempMotor.Moving = false; tempMotor.MovingCounter = 0; }//20140314 nova verzija - pokusaj da se izleci problem sa propadanjem platformi usled malog VP
                            else if (cpChanged) { tempMotor.Moving = true; tempMotor.MovingCounter = 0; }
                            else
                            {
                                if (++tempMotor.MovingCounter < Sys.movingToFalseCounter)
                                    tempMotor.Moving = true;
                                else
                                {
                                    tempMotor.Moving = false;
                                    tempMotor.MovingCounter = 0;
                                }
                            }

                            */

                            //if (tempMotor.MotorID == 3 || tempMotor.MotorID == 1333) Debug.WriteLine(string.Format("time={0} , id={1} cp={2} cv={3} state={4} inh={5} brk={6} cpChanged={7} mov={8}", DateTime.Now.ToString("hh:mm:ss.fff tt"), tempMotor.MotorID, tempMotor.CP, tempMotor.CV, tempMotor.state, tempMotor.Inh28, tempMotor.Brake, cpChanged, tempMotor.Moving));

                            #region 6 Second Brake
                            //if (tempMotor.Brake)
                            //{
                            //    tempMotor.AutoBrakingTimeStampTaken = false;
                            //    tempMotor.StopSend = false;
                            //}
                            //if (tempMotor.Moving) //  resetuje timestamp inace opali gasenje dok radi auto
                            //{
                            //    tempMotor.SetTimestamp = DateTime.Now;
                            //    //Debug.WriteLine("timestampReset - " + tempMotor.Title);
                            //}

                            //if (!tempMotor.Brake && (!tempMotor.Moving || !tempMotor.Local))
                            //{
                            //    if (!tempMotor.AutoBrakingTimeStampTaken)
                            //    {
                            //        tempMotor.SetTimestamp = DateTime.Now;
                            //        //Debug.WriteLine("timestamp - " + tempMotor.Title);
                            //        tempMotor.AutoBrakingTimeStampTaken = true; //garantuje samo jedan timestamp 
                            //    }

                            //    if (DateTime.Now.Subtract(tempMotor.SetTimestamp).Seconds > 10 && !tempMotor.StopSend) //Properties.Settings.Default.IdleDrivesBrakingTimeSpan
                            //    {
                            //        tempMotor.state = states.Stopped;
                            //        AlertLogic.Add(tempMotor.Title + ": Brake closed after period of inactivity.");
                            //        Log.Write(string.Format("Brake closed after period of inactivity. Motor={0}, motID={1}.", tempMotor.Title, tempMotor.MotorID), EventLogEntryType.Information);
                            //        //Debug.WriteLine("brake from period of inectivity");
                            //        tempMotor.StopSend = true; //garantuje samo jedno slanje

                            //    }

                            //}
                            #endregion


                            #region Check Motor Status
                            //if (tempMotor.calculatesEnabledType.ListOfMotors() != null)
                            //{
                            //    //if (Math.Abs(tempMotor.iSpSv.SP - tempMotor.CP) > tempMotor.MaxV * 0.01)
                            //    if (Math.Abs(tempMotor.iSpSv.SP - tempMotor.CP) > Properties.Settings.Default.PositionTollerance)//bilo pre
                            //    {
                            //        //ako je motor1 stao a u grupi se jos neko krece i nije stigao gde je krenuo
                            //        if (tempMotor.state == states.Stopped && tempMotor.calculatesEnabledType.ListOfMotors().Exists(qq => qq.Moving))
                            //        {
                            //            foreach (MotorLogic item in tempMotor.calculatesEnabledType.ListOfMotors())
                            //                if (item.state != states.Idle && item.state != states.Error && item.state != states.Stopped && item.state != states.ManStarted2)
                            //                {
                            //                    //Debug.WriteLine(string.Format("{2} Item: {0}, state: {1} , Stopper :{3} , AUTO", item.Title, item.state, DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"), tempMotor.Title));
                            //                    //Debug.WriteLine(string.Format("{0} tempmotor {1} cp= {2} sp={3} delta={4}", DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"), tempMotor.Title, tempMotor.iSpSv.SP, tempMotor.CP, Math.Abs(tempMotor.iSpSv.SP - tempMotor.CP)));
                            //                    item.state = states.Error;
                            //                    Debug.WriteLine(" stopper activated - group");
                            //                    Sys.printStack(item);
                            //                }
                            //        }
                            //    }

                            //    if (tempMotor.calculatesEnabledType.ListOfMotors().Exists(qq => qq.Trip))
                            //    {
                            //        foreach (MotorLogic item in tempMotor.calculatesEnabledType.ListOfMotors())
                            //            if (item.state != states.Idle && item.state != states.Error && item.state != states.Stopped)
                            //            {
                            //                //Debug.WriteLine(string.Format("{2} Item: {0}, state: {1} , Stopper :{3} , TRIP", item.Title, item.state, DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"), tempMotor.Title));
                            //                item.state = states.Error;
                            //                Debug.WriteLine(" stopper activated - trip");
                            //                Sys.printStack(item);
                            //            }
                            //    }

                            //    if (tempMotor.state == states.Stopped && tempMotor.calculatesEnabledType.ListOfMotors().Exists(qq => qq.state == states.ManStarted2)) //ovde treba detektovanje: nisam pustio joystick dugme ili nisam detektovano up ili down buttonUp event
                            //    {
                            //        foreach (MotorLogic item in tempMotor.calculatesEnabledType.ListOfMotors())
                            //            if (item.state != states.Idle && item.state != states.Error && item.state != states.Stopped && item.state != states.Started)
                            //            {
                            //                //Debug.WriteLine(string.Format("{2} Item: {0}, state: {1} , Stopper :{3} , MAN", item.Title, item.state, DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"), tempMotor.Title));
                            //                item.state = states.Error;
                            //                Debug.WriteLine(" stopper activated - joy/man");
                            //                Sys.printStack(item);
                            //            }
                            //    }
                            //}
                            #endregion

                            #region CHECK PHIDGET OUTPUTS

                            #endregion

                            //SYSTEM PANEL RECALCULATE
                            //VisibilityStatesLogic.UniqueInstance.CalculateSystemIndicators();

                            //ovo mora zadnje 
                            //stateMachine.ProcessStateMachine(tempMotor);

                            //record states 
                            //tempMotor.previousCP = tempMotor.CP;
                            //tempMotor.previousBrake = tempMotor.Brake;
                        }

                        //received CAN RX SDO1 || SDO2
                        if (((canID > CanSett.SDO1_RX_BaseID) && (canID <= CanSett.SDO1_RX_BaseID + 64)) ||
                            ((canID > CanSett.SDO2_RX_BaseID) && (canID <= CanSett.SDO2_RX_BaseID + 64)) ||
                            ((canID > CanSett.SDO3_RX_BaseID) && (canID <= CanSett.SDO3_RX_BaseID + 64)))
                        {
                            int _index = CanSett.LenzeIndexConversionConstant - data[2] * 256 - data[1];
                            byte _subindex = data[3];
                            double _value = BitConverter.ToInt32(data, 4);
                            //Int32[] _valueWord = { BitConverter.ToInt16(data, 4), BitConverter.ToInt16(data, 6) };
                            byte msgType = data[0];

                            //if received msg is diag RSP, update diag
                            //tempMotor.UpdateDiagItem(_index, _subindex, _value / 10000);

                            //if received msg is trip history, update trip history
                            //tempMotor.ReceiveTripHistoryData9300(data, _index, _subindex, (int)_value);

                            if (msgType == 65 || msgType == 198)
                            {
                                continue;
                                //65=block send read response. 9400 tako salje gesku????
                                //a sta je 198?????
                            }


                            if (_index == 168 && _subindex == 0)
                            {
                                //tempMotor.ReceiveTripHistoryData9400(data, _index, _subindex, (int)_value);



                                int tsss = BitConverter.ToInt32(data, 4);
                                int TripID = BitConverter.ToInt32(data, 4) & 16777215;
                                TripXML9400 tempTrip = Sys.StaticListOfTripsLenze9400.Find(t => t.Number == TripID);
                                //if (tempTrip != null)
                                //    Log.Write(string.Format("Motor={0} ID={1}, ErrorID: {2}, ERROR:{3}, ", tempMotor.Title, tempMotor.MotorID, tempTrip.Number, tempTrip.Value), EventLogEntryType.Error);
                            }

                            //if (_index == 168 && _subindex == 1)
                            //{
                            //    int TripID = (((((int)_value / 10000) % 3000) % 2000) % 1000);
                            //    TripXML tempTrip = Sys.StaticListOfTripsLenze9300.Find(t => t.ID == TripID);
                            //    if (tempTrip != null)
                            //        Log.Write(string.Format("Motor={5} ID={6}, ErrorID: {0}, {4} ERROR:{1}, {4} DESCRIPTION:{2}, {4} REMEDY:{3} ", tempTrip.ID, tempTrip.Error, tempTrip.ErrorDescription, tempTrip.CauseRemedy, Environment.NewLine, tempMotor.Title, tempMotor.MotorID));
                            //}

                            //if (msgType == 67) //sdo read response
                            //{
                            //    if (_index == CanSett.LimNegIndex) tempMotor.LimNegFromServo = 50 + _value / 10000;
                            //    if (_index == CanSett.LimPosIndex) tempMotor.LimPosFromServo = 50 + _value / 10000;
                            //}

                            ////if received msg is Imot, update Imot
                            //tempMotor.UpdateImot(data, _index, _value);

                            //msg RSP arrived, remove msg from queue
                            UdpTx.UniqueInstance.SdoResponseReceived(tempMotor.MotorID, _index, _subindex, data);

                            #region darko skuplja granice kretanja motora. TEST, skloniti posle
                            ////if (_index == 1223) Debug.WriteLine("{0} limPos {1}", tempMotor.MotorID, _value);
                            ////if (_index == 1224) Debug.WriteLine("{0} limNeg {1}", tempMotor.MotorID, _value);
                            //if (_index == 1223) tempMotor.gdcLimPos1223 = _value / 10000;
                            //if (_index == 1224) tempMotor.gdcLimNeg1224 = _value / 10000;
                            //if (_index == 1202) tempMotor.gdcNum1202 = _value / 10000;
                            //if (_index == 1203) tempMotor.gdcDenum1203 = _value / 10000;
                            //if (_index == 1204) tempMotor.gdcFeed1204 = _value / 10000;
                            //if (tempMotor.gdcCoef == 0 && tempMotor.gdcNum1202 != 0 && tempMotor.gdcDenum1203 != 0 && tempMotor.gdcFeed1204 != 0)
                            //{
                            //    tempMotor.gdcCoef = 65536 * tempMotor.gdcNum1202 / (tempMotor.gdcDenum1203 * tempMotor.gdcFeed1204);
                            //    //Debug.WriteLine("{0},gdcConst=,{1},scadaCoef=,{2}", tempMotor.MotorID, tempMotor.gdcCoef, tempMotor.Coeficient);\
                            //    if (tempMotor.LimNeg != tempMotor.gdcLimNeg1224)
                            //        Debug.WriteLine("{0},gdclimNeg=,{1},scada=,{2}", tempMotor.MotorID, tempMotor.gdcLimNeg1224, tempMotor.LimNeg);
                            //    if (tempMotor.LimPos != tempMotor.gdcLimPos1223)
                            //        Debug.WriteLine("{0},gdclimPos=,{1},scada=,{2}", tempMotor.MotorID, tempMotor.gdcLimPos1223, tempMotor.LimPos);

                            //}
                            #endregion

                            //#region Skupljanje Hoist CP-a
                            //if (_index == 1381 && _subindex == 61)
                            //    try
                            //    {
                            //        Sys.StaticStartecList.Find(qq => qq.EncoderMotorID == tempMotor.MotorID).CP = _value;
                            //    }
                            //    catch { AlertLogic.Add("ERROR: HOIST" + tempMotor.MotorID.ToString() + " CP"); }
                            //#endregion



                            continue;
                        }

                        //received CAN TX SDO1 || SDO2. do nothing
                        if (((canID > CanSett.SDO1_TX_BaseID) && (canID <= CanSett.SDO1_TX_BaseID + 64)) ||
                            ((canID > CanSett.SDO2_TX_BaseID) && (canID <= CanSett.SDO2_TX_BaseID + 64)) ||
                            ((canID > CanSett.SDO3_TX_BaseID) && (canID <= CanSett.SDO3_TX_BaseID + 64)))
                        { continue; }

                        if (
                            (canID > 384 && canID <= 448) || (canID > 641 && canID <= 705) || (canID > 767 && canID <= 833) || (canID > 128 && canID <= 192) || (canID == 128) ||
                             ((canID > CanSett.SDO2_TX_BaseID) && (canID <= CanSett.SDO2_TX_BaseID + 64)) || ((canID > CanSett.SDO1_TX_BaseID) && (canID <= CanSett.SDO1_TX_BaseID + 64)) ||
                           ((canID > CanSett.SDO1_TX_BaseID) && (canID <= CanSett.SDO1_TX_BaseID + 64)) || ((canID > CanSett.SDO2_TX_BaseID) && (canID <= CanSett.SDO2_TX_BaseID + 64)) ||
                           ((canID > CanSett.SDO3_TX_BaseID) && (canID <= CanSett.SDO3_TX_BaseID + 64))
                            ) { }
                        else
                        {
                            //UdpRxError.Count++;
                            //UdpRxError.PPS++;
                        }
                    }

                }
                catch (SocketException sockEx)
                {
                    RxSocketExceptionCounter++;
                    if (RxSocketExceptionCounter % 100 == 2)//todo resiti ovo, moye da puni log
                    {
                        string str = "Message send failed :" + sockEx.ErrorCode.ToString() + "\nMessage UDP send failed :" + sockEx.Message + "\nMessage UDP send failed :" + sockEx.StackTrace;
                        AlertLogic.Add(str);
                        Log.Write(str, EventLogEntryType.Error);
                    }
                }

            }
        }
        #endregion

        // da li radi dugmeta na konzoli
        //void RecalculatePidghetState(MotorLogic motor)
        //{
        //    //up //down
        //    phitem.ifkit.outputs[Properties.AIO.Default.Up] = phitem.ifkit.outputs[Properties.AIO.Default.Down] = motor.motorStateType.manPosNegEnabled;
        //    //Auto
        //    phitem.ifkit.outputs[Properties.AIO.Default.Auto] = motor.calculatesEnabledType.autoEnabled;
        //    //Stop
        //    phitem.ifkit.outputs[Properties.AIO.Default.Stop] = motor.calculatesEnabledType.stopEnabled;
        //    //Release//SP//SV//MV
        //    phitem.ifkit.outputs[Properties.AIO.Default.Release] = phitem.ifkit.outputs[Properties.AIO.Default.SP]
        //        = phitem.ifkit.outputs[Properties.AIO.Default.SP] = phitem.ifkit.outputs[Properties.AIO.Default.SP] = motor.motorStateType.SpSvEnabled;
        //}

        public int ResolveCanAddress(int idrecived)
        {
            //384-448:base+address
            //641-705:base+address-1
            //769-833:base+address-1
            //1408-1536:base+address

            if (((idrecived > 384) && (idrecived <= 448)) || ((idrecived > 1408) && (idrecived <= 1536)))
                return idrecived % 64;
            else if (((idrecived > 641) && (idrecived <= 705)) || ((idrecived > 768) && (idrecived <= 833)))
                //return idrecived % 64 - 1;
                return idrecived % 64;
            else if (idrecived == 128)
                return 128;
            else
                return idrecived % 64;
        }

        public bool GetBitFromByteArray(byte[] _in, int pos) { return ((_in[Convert.ToInt32(pos / 8)] & (byte)(1 << (pos % 8))) != 0); }





        public string initResult { get; set; }
    }
}