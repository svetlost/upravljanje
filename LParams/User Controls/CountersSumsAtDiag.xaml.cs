﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace StClient
{
    /// <summary>
    /// Interaction logic for CountersSums.xaml
    /// </summary>
    public partial class CountersSums : UserControl
    {
        public CountersSums()
        {
            InitializeComponent();
            this.DataContext = CountersLogic.UniqueInstance;
        }
    }

    public class CountersLogic : INotifyPropertyChanged
    {
        CountersLogic() { }
        class CountersLogicCreator
        {
            static CountersLogicCreator() { }
            internal static readonly CountersLogic uniqueInstance = new CountersLogic();
        }
        public static CountersLogic UniqueInstance
        {
            get { return CountersLogicCreator.uniqueInstance; }
        }

       
        #region TOTALS + PPS
        double _ImotTotal, _ImotTotalpps, _RxTotal, _TxTotal, _RxTotalpps, _TxTotalpps;
        //imot
        public double ImotTotal { get { return _ImotTotal; } set { if (_ImotTotal != value) { _ImotTotal = value; OnNotify("ImotTotal"); } } }
        public double ImotTotalPps { get { return _ImotTotalpps; } set { if (_ImotTotalpps != value) { _ImotTotalpps = value; OnNotify("ImotTotalPps"); } } }
        //rx
        public double RxTotal { get { return _RxTotal; } set { if (_RxTotal != value) { _RxTotal = value; OnNotify("RxTotal"); } } }
        public double RxTotalPps { get { return _RxTotalpps; } set { if (_RxTotalpps != value) { _RxTotalpps = value; OnNotify("RxTotalPps"); } } }
        //tx
        public double TxTotal { get { return _TxTotal; } set { if (_TxTotal != value) { _TxTotal = value; OnNotify("TxTotal"); } } }
        public double TxTotalPps { get { return _TxTotalpps; } set { if (_TxTotalpps != value) { _TxTotalpps = value; OnNotify("TxTotalPps"); } } }
        #endregion


       
        #region SDO HI-MED-LO + PPS
        double _SdoHiTimedOutCount, _SdoMedTimedOutCount, _SdoLoTimedOutCount, _SdoHiTimedOutpps, _SdoMedTimedOutpps, _SdoLoTimedOutpps;
        //hi
        public double SdoHiTimedOutCount { get { return _SdoHiTimedOutCount; } set { if (_SdoHiTimedOutCount != value) { _SdoHiTimedOutCount = value; OnNotify("SdoHiTimedOutCount"); } } }
        public double SdoHiTimedOutPps { get { return _SdoHiTimedOutpps; } set { if (_SdoHiTimedOutpps != value) { _SdoHiTimedOutpps = value; OnNotify("SdoHiTimedOutPps"); } } }
        //med
        public double SdoMedTimedOutCount { get { return _SdoMedTimedOutCount; } set { if (_SdoMedTimedOutCount != value) { _SdoMedTimedOutCount = value; OnNotify("SdoMedTimedOutCount"); } } }
        public double SdoMedTimedOutPps { get { return _SdoMedTimedOutpps; } set { if (_SdoMedTimedOutpps != value) { _SdoMedTimedOutpps = value; OnNotify("SdoMedTimedOutPps"); } } }
        //lo
        public double SdoLoTimedOutCount { get { return _SdoLoTimedOutCount; } set { if (_SdoLoTimedOutCount != value) { _SdoLoTimedOutCount = value; OnNotify("SdoLoTimedOutCount"); } } }
        public double SdoLoTimedOutPps { get { return _SdoLoTimedOutpps; } set { if (_SdoLoTimedOutpps != value) { _SdoLoTimedOutpps = value; OnNotify("SdoLoTimedOutPps"); } } }
        #endregion

       
        
        #region Errors RX-TX-Unconfigured Pdo Rx + PPS
        double _UdpTxErrorCount, _UnconfiguredPdoRx, _UdpRxErrorCount, _UdpTxErrorpps, _UdpRxErrorpps, _UnconfiguredPdoRxpps;
        //tx
        public double UdpTxErrorCount { get { return _UdpTxErrorCount; } set { if (_UdpTxErrorCount != value) { _UdpTxErrorCount = value; OnNotify("UdpTxErrorCount"); } } }
        public double UdpTxErrorPps { get { return _UdpTxErrorpps; } set { if (_UdpTxErrorpps != value) { _UdpTxErrorpps = value; OnNotify("UdpTxErrorPps"); } } }
        //rx
        public double UdpRxErrorCount { get { return _UdpRxErrorCount; } set { if (_UdpRxErrorCount != value) { _UdpRxErrorCount = value; OnNotify("UdpRxErrorCount"); } } }
        public double UdpRxErrorPps { get { return _UdpRxErrorpps; } set { if (_UdpRxErrorpps != value) { _UdpRxErrorpps = value; OnNotify("UdpRxErrorPps"); } } }
        //unconfigured
        public double UnconfiguredPdoRxCount { get { return _UnconfiguredPdoRx; } set { if (_UnconfiguredPdoRx != value) { _UnconfiguredPdoRx = value; OnNotify("UnconfiguredPdoRxCount"); } } }
        public double UnconfiguredPdoRxPps { get { return _UnconfiguredPdoRxpps; } set { if (_UnconfiguredPdoRxpps != value) { _UnconfiguredPdoRxpps = value; OnNotify("UnconfiguredPdoRxPps"); } } }
        #endregion



        public DateTime ImotTotaltimestamp, SdoHitTimedOuttimestamp, SdoMedTimedOuttimestamp, SdoLoTimedOuttimestamp, UdpTxErrortimestamp, UdpRxErrortimestamp, UnconfiguredPdoRxtimestamp, RxTotlatimestamp, TxTotlatimestamp;

        public double PPS(double counter, DateTime stamp)
        {
            return counter / DateTime.Now.Subtract(stamp).Seconds;
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
        #endregion
    }
}
