﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace StClient
{
    /// <summary>
    /// Interaction logic for Imap.xaml
    /// </summary>
    public partial class DiagItemUserControl : UserControl
    {
        public DiagItemUserControl(DiagItemValues ItemValues)
        {
            InitializeComponent();

            this.DataContext = ItemValues;
        }
    }

    public class DiagItemValues : INotifyPropertyChanged
    {
        
        double _value;
        public int LenzeIndex { get; set; }
        public byte LenzeSubIndex { get; set; }
        public byte Cluster { get; set; }
        public string Text { get; set; }
        public string Units { get; set; }
        public double Value { get { return _value; } set { if (_value != value) { _value = value; OnNotify("Value"); } } }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
    }
}
