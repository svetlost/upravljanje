﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace StClient
{
    /// <summary>
    /// Interaction logic for CountersSums.xaml
    /// </summary>
    public partial class Imot : UserControl
    {
        public Imot()
        {
            InitializeComponent();
            this.DataContext = ImotLogic.UniqueInstance;
        }
    }

    public class ImotLogic : INotifyPropertyChanged
    {
        ImotLogic() { }
        class ImotLogicCreator
        {
            static ImotLogicCreator() { }
            internal static readonly ImotLogic uniqueInstance = new ImotLogic();
        }
        public static ImotLogic UniqueInstance
        {
            get { return ImotLogicCreator.uniqueInstance; }
        }
        double _ImotTotal;
        public double ImotTotal { get { return _ImotTotal; } set { if (_ImotTotal != value) { _ImotTotal = value; OnNotify("ImotTotal"); } } }
       

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
        #endregion
    }
}
