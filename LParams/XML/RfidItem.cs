﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace StClient
{
    public class RfidItem
    {
        string _rfidtag="01075915bc", _name="no name";

        [XmlAttribute("RfidTag")]
        public string RfidTag { get { return _rfidtag; } set { _rfidtag = value; } }

        [XmlAttribute("Name")]
        public string Name { get { return _name; } set { _name = value; } }
    }
}
