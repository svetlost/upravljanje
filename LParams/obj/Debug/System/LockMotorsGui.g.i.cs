﻿#pragma checksum "..\..\..\System\LockMotorsGui.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "03684449ECF7DC5DBDAEC1D5D82806B2"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using StClient;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace StClient {
    
    
    /// <summary>
    /// LockMotorsGui
    /// </summary>
    public partial class LockMotorsGui : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 31 "..\..\..\System\LockMotorsGui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button time5m;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\System\LockMotorsGui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button time15m;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\System\LockMotorsGui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button time30m;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\System\LockMotorsGui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button time1h;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\System\LockMotorsGui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button time2h;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\System\LockMotorsGui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button time4h;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\System\LockMotorsGui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button StartOverride;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\..\System\LockMotorsGui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button StopOverride;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\System\LockMotorsGui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock timeStart;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\System\LockMotorsGui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock timeEnd;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\System\LockMotorsGui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock timeRemaining;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\System\LockMotorsGui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock OverrideSetTime;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/LParams;component/system/lockmotorsgui.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\System\LockMotorsGui.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.time5m = ((System.Windows.Controls.Button)(target));
            
            #line 31 "..\..\..\System\LockMotorsGui.xaml"
            this.time5m.Click += new System.Windows.RoutedEventHandler(this.time5m_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.time15m = ((System.Windows.Controls.Button)(target));
            
            #line 32 "..\..\..\System\LockMotorsGui.xaml"
            this.time15m.Click += new System.Windows.RoutedEventHandler(this.time15m_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.time30m = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\..\System\LockMotorsGui.xaml"
            this.time30m.Click += new System.Windows.RoutedEventHandler(this.time30m_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.time1h = ((System.Windows.Controls.Button)(target));
            
            #line 34 "..\..\..\System\LockMotorsGui.xaml"
            this.time1h.Click += new System.Windows.RoutedEventHandler(this.time1h_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.time2h = ((System.Windows.Controls.Button)(target));
            
            #line 35 "..\..\..\System\LockMotorsGui.xaml"
            this.time2h.Click += new System.Windows.RoutedEventHandler(this.time2h_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.time4h = ((System.Windows.Controls.Button)(target));
            
            #line 36 "..\..\..\System\LockMotorsGui.xaml"
            this.time4h.Click += new System.Windows.RoutedEventHandler(this.time4h_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.StartOverride = ((System.Windows.Controls.Button)(target));
            
            #line 42 "..\..\..\System\LockMotorsGui.xaml"
            this.StartOverride.Click += new System.Windows.RoutedEventHandler(this.StartOverride_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.StopOverride = ((System.Windows.Controls.Button)(target));
            
            #line 43 "..\..\..\System\LockMotorsGui.xaml"
            this.StopOverride.Click += new System.Windows.RoutedEventHandler(this.StopOverride_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.timeStart = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 10:
            this.timeEnd = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.timeRemaining = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.OverrideSetTime = ((System.Windows.Controls.TextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

