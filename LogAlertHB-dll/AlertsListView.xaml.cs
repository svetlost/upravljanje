﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.ComponentModel;


namespace LogAlertHB
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class AlertsListView : UserControl, INotifyPropertyChanged
    {      

        int CellingNumberOfMessages = 50;
        int NumberOfMessagesAfterCut;

        void LV_LayoutUpdated(object sender, EventArgs e)
        {
            try
            {
                if (AlertLogic.AlertsGet.Count > CellingNumberOfMessages)
                    for (int i = 0; i < NumberOfMessagesAfterCut; i++)
                        AlertLogic.AlertsGet.Remove(AlertLogic.AlertsGet[i]);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void AlertsGet_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            try
            {
               

                if (!(LV.Items.IsEmpty))
                {
                    LV.ScrollIntoView(LV.Items[LV.Items.Count - 1]);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public AlertsListView(double height)
        {
            InitializeComponent();
            this.Height = height;

            NumberOfMessagesAfterCut = CellingNumberOfMessages - (CellingNumberOfMessages / 2);

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {

                Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate()
                {
                    LV.ItemsSource = AlertLogic.AlertsGet;
                    AlertLogic.AlertsGet.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(AlertsGet_CollectionChanged);
                    LV.LayoutUpdated += new EventHandler(LV_LayoutUpdated);
                });
            }

        }
        public AlertsListView()
        {
            InitializeComponent();
            this.VerticalAlignment = VerticalAlignment.Stretch;

            NumberOfMessagesAfterCut = CellingNumberOfMessages - (CellingNumberOfMessages / 2);

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {

                Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate()
                {
                    LV.ItemsSource = AlertLogic.AlertsGet;
                    AlertLogic.AlertsGet.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(AlertsGet_CollectionChanged);
                    LV.LayoutUpdated += new EventHandler(LV_LayoutUpdated);
                });
            }

        }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

      


        private void LV_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //MessageBoxResult result = MessageBox.Show("Clear Messages", "CLEAR", MessageBoxButton.YesNo);
            //switch (result)
            //{
            //    case MessageBoxResult.Yes:
            //        AlertLogic.AlertsGet.Clear();
            //        break;
            //    case MessageBoxResult.No:
            //        break;
            //}
        }
    }



    public class AlertLogic
    {

        static ObservableCollection<Alerts> _alerts = new ObservableCollection<Alerts>();
        public static ObservableCollection<Alerts> AlertsGet
        { get { return _alerts; } }



        public static void Add(string text)
        {
            try
            {

                _alerts.Add(new Alerts
                {
                    AlertTime = DateTime.Now,
                    AlertText = text
                });

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public class Alerts
        {
            public DateTime AlertTime { get; set; }
            public string AlertText { get; set; }
        }
    }
}
