﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;


namespace StClient
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class CueGUI : UserControl
    {
        public MotorLogic _motor;
        

        public CueGUI(MotorLogic motor)
        {
            InitializeComponent();           

            this.DataContext = motor;
            _motor = motor;
           
        }     

        private void resetTrip_Click(object sender, RoutedEventArgs e)
        {
            Commands.Instance.resetTrip(_motor.toList());
        }



        private void Auto_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {          
            _motor.state = states.AutoStarting;
        }

        private void Stop_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {           
            _motor.state = states.Stopping;
        }

        private void SP_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Commands.Instance.SP_Popup(_motor.toList(), sender as TextBlock,false);
        
        }

        private void SV_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            Commands.Instance.SVMV_Popup(_motor.toList(), sender as TextBlock, false);
        }

        private void forceStop_Click(object sender, RoutedEventArgs e)
        {
            UdpTx.SendPDO(_motor.MotorID, CanStructure.CanCommandStop, 0, 2);
        }
   }
}
