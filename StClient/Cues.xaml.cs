﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.IO;
using System.ComponentModel;
using System.Windows.Threading;
using System.Diagnostics;
using System.Xml.Serialization;
using LogAlertHB;

namespace StClient
{
    /// <summary>
    /// Interaction logic for LV.xaml
    /// </summary>
    public partial class Cues : UserControl, INotifyPropertyChanged
    {

        public delegate void CallMotorLogic(int cueNumber, int motorID, CueDelegateStates CueDelegatStates);
        public event CallMotorLogic NotifyMlEventHandler;
        public static ObservableCollection<CueItem> _lvitems = new ObservableCollection<CueItem>();
        CollectionViewSource _sortedcvs = new CollectionViewSource();
        List<MotorLogic> tempMotorsForCues = new List<MotorLogic>();
        List<CueItem> TempCueItems = new List<CueItem>();
        List<MotorLogic> MovementMotorsList = new List<MotorLogic>();

        public double SyncCueV;
        List<MotorLogic> SysMotorList { get { return Sys.StaticMotorList; } }

        string path = Environment.CurrentDirectory + @"\Cues\";
      
        int _currentCueNo;

        public int currentCueNo { get { return _currentCueNo; } set { if (_currentCueNo != value) { _currentCueNo = value; OnNotify("currentCueNo"); } } }

        string _XmlFileName = "";
        public string XmlFileName { get { return _XmlFileName; } set { if (_XmlFileName != value) { _XmlFileName = value; OnNotify("XmlFileName"); } } }

        bool _autoEnabled;
        public bool AutoEnabled { get { return _autoEnabled; } set { if (_autoEnabled != value) { _autoEnabled = value; OnNotify("AutoEnabled"); } } }


        bool _trip;
        public bool Trip
        {
            get { return _trip; }
            set
            {
                if (_trip != value)
                {
                    _trip = value; OnNotify("Trip");
                    if (_trip == true)
                    {
                        foreach (CueItem cueitem in TempCueItems.Where(qq => qq.Motor.state != states.Stopped && qq.Motor.state != states.Stopping && qq.Motor.state != states.Idle))
                            cueitem.Motor.state = states.Stopping;
                    }
                }
            }
        }
        bool _moving, _issynccue, _Brake;
        public bool Moving { get { return _moving; } set { if (_moving != value) { _moving = value; OnNotify("Moving"); } } }
        public bool Brake { get { return _Brake; } set { if (_Brake != value) { _Brake = value; OnNotify("Brake"); } } }
        public bool IsSyncCue { get { return _issynccue; } set { if (_issynccue != value) { _issynccue = value; OnNotify("IsSyncGroup"); } } } //a sta je ovo? RecalculateVisibility();


        # region Subscribe To Motors
        public void SubscribeMotorsToCue(MotorLogic subject)
        {
            subject.NotifyCueEventHandler += Update;
        }


        void Update(MotorLogic ml, CueUpdateAction state)
        {
            if (state == CueUpdateAction.Add)
                tempMotorsForCues.Add(ml);
            else
                tempMotorsForCues.Remove(ml);
        }
        #endregion


        private static volatile Cues instance;
        private static object syncRoot = new Object();

        private Cues()
        {
            InitializeComponent();

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            _sortedcvs.SortDescriptions.Add(new System.ComponentModel.SortDescription("Cue", System.ComponentModel.ListSortDirection.Ascending));
            _sortedcvs.Source = _lvitems;

            lv.SelectionChanged += new SelectionChangedEventHandler(lv_SelectionChanged);
            lv.DataContext = _sortedcvs;
            this.DataContext = this;

            Binding b = new Binding();
            b.Source = this;
            b.Path = new PropertyPath("selectedRow");
            lv.SetBinding(ListView.SelectedItemProperty, b);
        }

        public static Cues Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new Cues();
                    }
                }

                return instance;
            }
        }




        void lv_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lv.SelectedItem != null)
                this.currentCueNo = (lv.SelectedItem as CueItem).CueNo;
            //CalculateTempCueItems(); CueGUIStates(); ;

        }

        #region Button Events

        public void updateCue_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate()
                             {
                                 if (currentCueNo == -1 || tempMotorsForCues.Count == 0) return;


                                 //izbaci iz liste cuova u motoru i iz lvitemsa
                                 foreach (CueItem item in _lvitems.Where(l => l.CueNo == currentCueNo))
                                     NotifyMlEventHandler(currentCueNo, item.MotorID, CueDelegateStates.Remove);

                                 List<CueItem> templist = _lvitems.Where(qq => qq.CueNo == currentCueNo).ToList();



                                 foreach (MotorLogic item in tempMotorsForCues)
                                 {
                                     _lvitems.Add(new CueItem
                                     {
                                         CueNo = currentCueNo,
                                         MotorID = item.MotorID,
                                         MotorTitle = item.Title,
                                         Motor = item,
                                         SP = item.SP,
                                         SV = item.SV,                                         
                                         CueDescription = ""
                                     }
                               );
                                     NotifyMlEventHandler(currentCueNo, item.MotorID, CueDelegateStates.Add);
                                 }
                                 for (int i = templist.Count - 1; i >= 0; i--) // promene 16 jun: da pravilno menja redoslede
                                     _lvitems.Remove(templist[i]);

                                 for (int i = tempMotorsForCues.Count - 1; i >= 0; i--)
                                     tempMotorsForCues[i].Cued = false;
                                 SortLV();
                                 lv.SelectedItem = _lvitems.First(qq => qq.CueNo == currentCueNo); //u selection shanged ima recalc sveg

                             });
        }


        public void Copy_Click(object sender, RoutedEventArgs e)
        {
            int number;
            double d;
            TextBlock tb = new TextBlock();
            tb.Name = "Copy"; // promene 16 jun : obavestava popup ko je
            Commands.Instance.PopupCommand(tb, false);
            if (double.TryParse(tb.Text, out d))
                number = Convert.ToInt32(d);
            else
                return;
            //if (int.TryParse(tb.Text, out number))
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate()
                                {
                                    if (currentCueNo == -1 || number == currentCueNo || _lvitems.Count == 0) return; //promene 16 jun : dodat lvitems.count


                                    List<CueItem> itemsToRemove = _lvitems.Where(i => i.CueNo == number).ToList(); //ako ima motora na tom broju obrisi ih
                                    List<CueItem> itemsToAdd = _lvitems.Where(i => i.CueNo == currentCueNo).ToList();

                                    if (itemsToRemove.Count != 0)
                                        foreach (CueItem item in itemsToRemove)
                                        {
                                            _lvitems.Remove(item);
                                            NotifyMlEventHandler(number, item.MotorID, CueDelegateStates.Remove);
                                        }

                                    foreach (CueItem item in itemsToAdd)
                                    {
                                        _lvitems.Add(new CueItem
                                        {
                                            CueNo = number,
                                            MotorID = item.MotorID,
                                            MotorTitle = item.MotorTitle,
                                            Motor = item.Motor,
                                            SP = item.SP,
                                            SV = item.SV,
                                            CueDescription = ""
                                        });
                                        NotifyMlEventHandler(number, item.MotorID, CueDelegateStates.Add);
                                    }
                                    lv.SelectedItem = _lvitems.First(qq => qq.CueNo == currentCueNo); //u selection shanged ima recalc sveg

                                });
            }
        }

        public void insertCue_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate()
                            {
                                for (int i = 0; i < _lvitems.Count; i++)
                                    if (_lvitems[i].CueNo >= currentCueNo)
                                        _lvitems[i].CueNo++;

                                _lvitems.Add(new CueItem { CueNo = currentCueNo, MotorID = -1, Motor = new MotorLogic(), MotorTitle = "NO MOTOR", CueDescription = "Cue Inserted", SP = 0, SV = 0 });
                                SortLV();
                                lv.SelectedItem = _lvitems.First(qq => qq.CueNo == currentCueNo);
                            });
        }



        public void UpdateMotors_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate()
                                {
                                    if (currentCueNo == -1) return;

                                    foreach (MotorLogic m in SysMotorList)
                                        m.Cued = false;
                                    if (lv.SelectedItem != null)
                                    {
                                        foreach (CueItem item in _lvitems.Where(items => items.CueNo == (this.lv.SelectedItem as CueItem).CueNo))
                                        {
                                            MotorLogic temp = SysMotorList.Find(m => m.MotorID == item.MotorID);
                                            temp.SP = item.SP;
                                            temp.SV = item.SV;
                                            temp.Cued = true;
                                        }
                                    }
                                });
            }
            catch (Exception ex)
            {
                AlertLogic.Add(ex.Message);
                Log.Write(ex.Message);
            }
        }

        public void ClearCurrent_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate()
                            {
                                List<CueItem> temp = _lvitems.Where(t => t.CueNo == this.currentCueNo).ToList();
                                foreach (CueItem t in temp)
                                {
                                    _lvitems.Remove(t);
                                    NotifyMlEventHandler(currentCueNo, t.MotorID, CueDelegateStates.Remove);
                                }

                                //Remove
                                if (_lvitems.Count > 0)
                                {
                                    if (_lvitems.Where(qq => qq.CueNo == currentCueNo) != null)
                                        lv.SelectedItem = _lvitems.First(qq => qq.CueNo == currentCueNo); //nadji 
                                }
                                else
                                {
                                    TempCueItems.Clear(); 
                                    AutoEnabled = Trip = Moving = false;
                                }
                            });
        }
        public void DeleteMotor_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate()
                            {
                                if (lv.SelectedIndex == -1) return;
                                int tempIndex = lv.SelectedIndex;
                                CueItem ml = lv.SelectedItem as CueItem;

                                NotifyMlEventHandler(currentCueNo, ml.MotorID, CueDelegateStates.Remove);
                                _lvitems.Remove(lv.SelectedItem as CueItem);

                                //Remove
                                if (_lvitems.Count > 0)
                                    lv.SelectedItem = _lvitems.First(qq => qq.CueNo == currentCueNo); //nadji 
                                else
                                {
                                    TempCueItems.Clear(); 
                                    AutoEnabled = Trip = Moving = false;
                                }
                            });
        }
        public void DeleteAllCues_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate()
                           {
                               foreach (int motorID in _lvitems.Select(qq => qq.MotorID).Distinct())
                               {
                                   NotifyMlEventHandler(0, motorID, CueDelegateStates.Clear);
                               }
                               _lvitems.Clear();
                               //Remove
                               TempCueItems.Clear(); 
                               AutoEnabled = Trip = Moving = false;
                           });
        }

        public void CopyCP_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate()
                            {
                                if (lv.SelectedIndex == -1) return;

                                foreach (CueItem item in _lvitems.Where(t => t.CueNo == this.currentCueNo))
                                    item.SP = SysMotorList.Find(m => m.MotorID == item.MotorID).CP;
                            });
        }
        public void CueAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (MotorLogic motor in SysMotorList)
                motor.Cued = true;
        }
        public void CueNone_Click(object sender, RoutedEventArgs e)
        {
            foreach (MotorLogic motor in SysMotorList)
                motor.Cued = false;
        }
        public void ViewCued_Click(object sender, RoutedEventArgs e)
        {
            MotorsVisibilityStates.ShowCuedMotors();
        }


        #endregion

        #region SaveLoadPlayStop
        public void FileOpen_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate()
                            {
                                if (XmlFileName == "") return;
                                string fileName = path + this.XmlFileName;
                                XmlUtil<CueItem> x = new XmlUtil<CueItem>();
                                List<CueItem> tempList = x.ReadXml(fileName, CreateDefaultRow.DontCreate);
                                if (tempList == null) return;

                                _lvitems.Clear();
                                foreach (CueItem c in tempList)
                                {
                                    if (SysMotorList.Find(m => m.MotorID == c.MotorID) != null)
                                    {
                                        _lvitems.Add(new CueItem
                                        {
                                            CueNo = c.CueNo,
                                            MotorID = c.MotorID,
                                            MotorTitle = c.MotorTitle,
                                            SP = c.SP,
                                            SV = c.SV,
                                            CueDescription = c.CueDescription

                                        });
                                        SortLV();
                                        NotifyMlEventHandler(c.CueNo, c.MotorID, CueDelegateStates.Add);
                                    }
                                }
                                foreach (CueItem ci in _lvitems)
                                {
                                    ci.Motor = SysMotorList.Find(qq => qq.MotorID == ci.MotorID);
                                    TempCueItems.Add(ci);

                                }
                                //CalculateTempCueItems(); CueGUIStates();
                                try
                                {
                                    lv.SelectedItem = _lvitems.First(c => c.CueNo == _lvitems.Min(qq => qq.CueNo));
                                }
                                catch
                                {
                                    AlertLogic.Add("Please Select Cue");
                                }
                            });
        }
        public void FileSave_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate()
                            {
                                if (this.XmlFileName == "")
                                {
                                    AlertLogic.Add("Error: Please input name"); // promene 16 jun: 
                                    return;
                                }


                                string fileName = "";
                                if (XmlFileName.EndsWith(".xml"))
                                    fileName = path + this.XmlFileName;
                                else
                                    fileName = path + this.XmlFileName + ".xml";

                                File.Delete(fileName);

                                XmlUtil<CueItem> x = new XmlUtil<CueItem>();

                                x.WriteXml(fileName, _lvitems.ToList());
                            });
        }
        public void Play_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate()
                              {
                                  if (lv.SelectedIndex == -1) return;
                                  //List<MotorLogic> tempCueItems = new List<MotorLogic>();
                                  foreach (CueItem item in TempCueItems)
                                  {
                                      //MotorLogic ml = SysMotorList.Find(m => m.MotorID == item.MotorID);
                                      //item.Motor.SP = item.SP;
                                      //item.CueSP=;
                                      //item.Motor.state = states.AutoStarting;
                                      item.Motor.CueSP = item.SP;
                                      if (Math.Abs(item.SP - item.Motor.CP) > Properties.Settings.Default.Tollerance)
                                      {
                                          double k = (IsSyncCue ? SyncCueV / item.Motor.MaxV : 1);
                                          item.Motor.SecBrkControlLock = MotorBrake.Unlocked;
                                          UdpTx.SendSDOWriteRQ(item.Motor.MotorID, CanStructure.SpIndex, CanStructure.SpSubIndex, item.SP, SdoPriority.hi);
                                          UdpTx.SendSDOWriteRQ(item.Motor.MotorID, CanStructure.SvIndex, CanStructure.SvSubIndex, item.SV * k, SdoPriority.hi);
                                          UdpTx.SendPDO(item.Motor.MotorID, CanStructure.CanCommandAuto, CanStructure.SdoProcTime * 2, 2);
                                          item.Motor.state = states.AutoStarting2;
                                          item.Motor.MovementControler = Movement.Cues;
                                      }
                                      else
                                          item.Motor.state = states.Idle;
                                  }
                              });
        }
        public void Stop_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate()
                            {
                                if (lv.SelectedIndex == -1) return;

                                foreach (CueItem item in _lvitems.Where(t => t.CueNo == this.currentCueNo))
                                {
                                    MotorLogic temp = SysMotorList.Find(m => m.MotorID == item.MotorID);
                                    if (temp == null) continue;
                                    temp.MovementControler = Movement.NoMovement;
                                    if (temp.Moving == false) continue;
                                    temp.state = states.Stopping;
                                }
                            });
        }
        void SortLV()
        {
            _sortedcvs.SortDescriptions.Clear();
            _sortedcvs.SortDescriptions.Add(new System.ComponentModel.SortDescription("CueNo", System.ComponentModel.ListSortDirection.Ascending));
            _sortedcvs.SortDescriptions.Add(new System.ComponentModel.SortDescription("MotorID", System.ComponentModel.ListSortDirection.Ascending));
        }
        #endregion

        private void ReadCueFilesInFolder(object sender, EventArgs e)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            CueName.Items.Clear();
            foreach (FileInfo fileinfo in directoryInfo.GetFiles("*.xml"))
                CueName.Items.Add(fileinfo.Name.ToString());
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion



        private void goToPreviousCue_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int _cueNo = _lvitems.Where(t => t.CueNo < currentCueNo).Max(t => t.CueNo);
                lv.SelectedItem = _lvitems.First(c => c.CueNo == _cueNo);
            }
            catch { }
            //finally { CalculateTempCueItems(); CueGUIStates(); }
        }
        private void goToNextCue_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int _cueNo = _lvitems.Where(t => t.CueNo > currentCueNo).Min(t => t.CueNo);
                lv.SelectedItem = _lvitems.First(c => c.CueNo == _cueNo);
            }
            catch { }
            //finally { CalculateTempCueItems(); CueGUIStates(); }
        }
        private void CueIncrement_Click(object sender, RoutedEventArgs e)
        {
            currentCueNo++;

            try { lv.SelectedItem = _lvitems.First(l => l.CueNo == currentCueNo); }
            catch { lv.SelectedItem = -1; }
            //finally { CalculateTempCueItems(); CueGUIStates(); }
        }
        private void CueDecrement_Click(object sender, RoutedEventArgs e)
        {
            if (currentCueNo > 0)
                currentCueNo--;

            try { lv.SelectedItem = _lvitems.First(l => l.CueNo == currentCueNo); }
            catch { lv.SelectedItem = -1; }
            //finally { CalculateTempCueItems(); CueGUIStates(); }
        }

        private void EditBoxSP_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Commands.Instance.SP_Popup((lv.SelectedItem as CueItem).Motor.toList(), sender as TextBlock, true);
        }
        private void EditBoxSV_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Commands.Instance.SVMV_Popup((lv.SelectedItem as CueItem).Motor.toList(), sender as TextBlock, true);
        }
        private void EditBoxDescription_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Commands.Instance.PopupCommand(sender as TextBlock, true);
        }


        private void StopAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (MotorLogic motor in SysMotorList.FindAll(m => m.Moving == true))
                motor.state = states.Stopping;
        }

        public void CueMovementStatus(MotorLogic motor)
        {
            if (motor.state != states.Idle)
            {
                
                if (motor.MovementControler == Movement.Cues && DateTime.Now.Subtract(motor.autoSatrtedTimeStamp).Seconds > 1)
                {
                    if (Math.Abs(TempCueItems.Find(q => q.MotorID == motor.MotorID).SP - motor.CP) > Properties.Settings.Default.Tollerance)
                    {
                        //Debug.WriteLine("Motor: " + motor1.Title + " Difference: " +(Math.Abs(gl.SP - motor1.CP).ToString()));
                        if (motor.state == states.AutoStarted && !motor.Moving)
                        {
                            foreach (MotorLogic item in MovementMotorsList.Where(qq => qq.state != states.Stopped && qq.state != states.Stopping && qq.state != states.Idle))
                                item.state = states.Stopping;
                        }

                        //ako je motor1 stao a u grupi se jos neko krece i nije stigao gde je krenuo
                        if (motor.state == states.Stopped && Moving)
                        {
                            foreach (MotorLogic item in MovementMotorsList.Where(qq => qq.state != states.Stopped || qq.state != states.Idle || qq.state != states.Stopping))
                                if (item.MotorID != motor.MotorID)
                                    item.state = states.Stopping;
                        }
                    }
                }
            }

            //if (motor.state != states.Idle)
            //{
            //    //ako nema motora u temp cue items ovo prsa
            //    //CueItem CI = TempCueItems.Find(q => q.MotorID == motor.MotorID);
            //    if (motor != null && TempCueItems.Count > 0 && TempCueItems.Exists(qq => qq.MotorID == motor.MotorID)) //patch
            //    {
            //        //Debug.WriteLine(string.Format("{0} motor={1,2} cueSP={2,5:0.0} motorCP={3,5:0.0} mov={4,5} motorState={5}", DateTime.Now.ToLongTimeString(), motor.MotorID, motor.CueSP, motor.CP, motor.Moving, motor.state));

            //        //if (Math.Abs(TempCueItems.Find(q => q.MotorID == motor.MotorID).SP - motor.CP) > Properties.Settings.Default.Tollerance &&
            //        //    ((!Moving && motor.state == states.AutoStarted) || (motor.state == states.Stopped && Moving)))
            //        if (Math.Abs(motor.CueSP - motor.CP) > Properties.Settings.Default.Tollerance &&
            //            ((!Moving && motor.state == states.AutoStarted) || (motor.state == states.Stopped && Moving)) && motor.MovementControler == Movement.Cues)
            //        {
            //            foreach (CueItem item in TempCueItems.Where(qq => qq.Motor.state != states.Stopped && qq.Motor.state != states.Stopping && qq.Motor.state != states.Idle))
            //                item.Motor.state = states.Stopping;
            //        }
            //    }
            //}
        }
        public void CalculateTempCueItems() // trenutno vazeci (obelezen na currentCueNo) stimung i njegovi motori
        {
            TempCueItems = _lvitems.Where(qq => qq.CueNo == currentCueNo).ToList();
            if (TempCueItems.Count != 0)
                SyncCueV = TempCueItems.Min(qq => qq.Motor.MaxV);
            else
                SyncCueV = 1;

            MovementMotorsList.Clear();
            foreach (CueItem item in TempCueItems)
            {
                MovementMotorsList.Add(Sys.StaticMotorList.Find(qq => qq.MotorID == item.MotorID));
            }

        }

        /// <summary>
        /// proveravamo stanje play dugmeta posle sledecih dogadjaja
        /// 1.promena broja stimunga
        /// 2.otvorimo stimung fajl
        /// 3.prvi sledeci manji stimung 
        /// 4.prvi sledeci veci stimung
        /// 5.uvecamo cueCurretnumber za 1
        /// 6.smanjimo cuecurrentnumber za 1
        /// 7.u tajmeru pri primanju novih poruka sa cana
        /// u prvih 6 slucajeva izracunavamo i CalculateTempCueItems() posto smo ih sigurno promenili 
        ///  slucaj 7. se izvrsava na izracunatim CalculateTempCueItems() svakih 200ms 
        /// </summary>
        public void CueGUIStates() // Play button,Trip i moving indikatori
        {
            AutoEnabled = TempCueItems.Exists(q => !q.Motor.Trip) && TempCueItems.All(q => !q.Motor.Moving && q.Motor.RefOk && !q.Motor.Local && q.Motor.Can);
            Trip = TempCueItems.Exists(q => q.Motor.Trip);
            Moving = TempCueItems.Exists(q => q.Motor.Moving);           

            //foreach (CueItem item in TempCueItems)
            //{
            //    if (Moving)
            //        item.Motor.grpResetEnabled = false;
            //    else
            //        item.Motor.grpResetEnabled = true;
            //}
        }

        private void SyncV_Checked(object sender, RoutedEventArgs e)
        {
            if (SyncV.IsChecked == true)
                SyncV.Content = "SyncV V";
            else
                SyncV.Content = "No Sync";
        }

        private void FileDelete_Click(object sender, RoutedEventArgs e) //promene 17 jun: 
        {
            if (this.XmlFileName == "")
            {
                AlertLogic.Add("Error: Please input name"); // promene 17 jun: 
                return;
            }
            if (File.Exists(path + this.XmlFileName)) // promene 17 jun:  
            {
                File.Delete(path + this.XmlFileName);
                CueName.SelectedValue = "";
            }
        }

    }

   
}