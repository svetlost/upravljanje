﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.IO;
using System.ComponentModel;
using System.Windows.Threading;
using System.Diagnostics;
using System.Xml.Serialization;
using LogAlertHB;


namespace StClient
{
    /// <summary>
    /// Interaction logic for LV.xaml
    /// </summary>
    public partial class Cues : UserControl
    {           
      
      

        public Cues()
        {
            InitializeComponent();                
            this.DataContext = CueLogic.UniqueInstance;

        }     

        #region Button Events

        public void updateCue_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.updateCue();           
        }

        public void Copy_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.Copy();            
        }

        public void insertCue_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.insertCue();          
        }

        public void UpdateMotors_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.UpdateMotors();          
        }

        public void ClearCurrent_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.ClearCurrent();
        }

        public void DeleteMotor_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.DeleteMotor();
        }

        public void DeleteAllCues_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.DeleteAllCues();
        }

        public void CopyCP_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.CopyCP();
        }

        public void CueAll_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.CueAll();
        }

        public void CueNone_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.CueNone();
        }

        public void ViewCued_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.ViewCued();
        }

        private void resetTrip_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.ResetTrip();
        }

        #endregion

        #region SaveLoadPlayStopDelete

        public void FileOpen_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.FileOpen();
        }

        public void FileSave_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.FileSave();
        }

        private void FileDelete_Click(object sender, RoutedEventArgs e) 
        {
            CueLogic.UniqueInstance.FileDelete();
        }

        public void Play_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.Play();
        }

        public void Stop_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.Stop();
        }

        private void StopAll_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.StopAll();
        }       
      
        #endregion

        private void ReadCueFilesInFolder(object sender, EventArgs e)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(CueLogic.UniqueInstance.path);
            CueName.Items.Clear();
            foreach (FileInfo fileinfo in directoryInfo.GetFiles("*.xml"))
                CueName.Items.Add(fileinfo.Name.ToString());
        }


        #region NextPreviousIncrementDecrement
        private void goToPreviousCue_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.PreviousCue();
        }
        private void goToNextCue_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.NextCue();
        }
        private void CueIncrement_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.Increment();
        }
        private void CueDecrement_Click(object sender, RoutedEventArgs e)
        {
            CueLogic.UniqueInstance.Decrement();
        }
        #endregion

        private void CueNumber_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            CueLogic.UniqueInstance.ChangeCueNumber();
        }

       

       

       


         

      

    }


}