﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Media.Media3D;
using System.IO;
using System.Windows.Markup;
using System.Windows.Media.Animation;

namespace StClient
{
    /// <summary>
    /// Interaction logic for _3Dview.xaml
    /// </summary>
    public partial class view3d : UserControl
    {
        //DispatcherTimer updatePos = new DispatcherTimer();
        DispatcherTimer updatePos = new DispatcherTimer();
        //DispatcherTimer updatePos = new DispatcherTimer(TimeSpan.FromSeconds(1), DispatcherPriority.Background, new EventHandler(dispPos), MainWindow.mainWindowDispacher);
        //EventHandler eh = new EventHandler(dispPos);
        public void dispPos(object sender, EventArgs e)
        {
            PerspectiveCamera PCamera = (PerspectiveCamera)m_CameraController.PCamera;
            CamPos.Text = string.Format("CamPos (point) x={0} y={1} z={2}", PCamera.Position.X, PCamera.Position.Y, PCamera.Position.Z);
            CamTargetPos.Text = string.Format("CamTarget (look vector) x={0} y={1} z={2}", PCamera.LookDirection.X, PCamera.LookDirection.Y, PCamera.LookDirection.Z);
        }
        private OrbitalCameraController m_CameraController;

        //private PerspectiveCamera PCamera = new PerspectiveCamera();
        //private OrthographicCamera OCamera = new OrthographicCamera();
        //PerspectiveCamera c;
        string XamlFilename = Environment.CurrentDirectory + @"\Config\204-034zamResources.xaml";
        public AnimateMotor am;


        Viewport3D vp3d;
        //Viewport3D ZAM3DViewport3D = new Viewport3D();


        public enum XamlInputType { MaxExport_ResourceDirectory, Zam3dExport_ResourceDirectory, Zam3dExport_ControlTemplate, Zam3dExport_Inline, DeepExploration_Inline }


        public view3d()
        {
            InitializeComponent();

            updatePos.Tick += new EventHandler(dispPos);
            updatePos.Interval = TimeSpan.FromSeconds(1);
            updatePos.Start();
            CamPos.TextWrapping = CamTargetPos.TextWrapping = TextWrapping.Wrap;

            m_CameraController = new OrbitalCameraController(new Vector3D(2820, 336, -1174), new Vector3D(0, 0, 0));
            //ZAM3DViewport3D.Camera = m_CameraController.Camera;        

            vp3d = ParseZamResourceDirectory();
            border.Child = vp3d;
            am = new AnimateMotor(Sys.StaticMotorList, vp3d);
             
              
            //c = (border.Child as Viewport3D).Camera as PerspectiveCamera;
        }

        Viewport3D ParseZamResourceDirectory()
        {
            Viewport3D t;

            using (FileStream fs = new FileStream(XamlFilename, FileMode.Open))
            {
                t = (Viewport3D)XamlReader.Load(fs);
            }

            Model3DCollection m3dc = ((Model3DGroup)((ModelVisual3D)t.Children[0]).Content).Children;

            //remove backMaterial
            for (int i = 0; i < m3dc.Count; i++)
            {
                if (m3dc[i] is Model3DGroup)
                {
                    if (((Model3DGroup)m3dc[i]).Children[0] is GeometryModel3D)
                    {
                        GeometryModel3D klj = ((GeometryModel3D)((Model3DGroup)m3dc[i]).Children[0]);
                        klj.BackMaterial = null;
                    }
                }
            }
            //if (true) return t;//test
            foreach (MotorLogic motor in Sys.StaticMotorList)
            {
                TranslateTransform3D tt3d = new TranslateTransform3D();

                //bajndujes transform na cp od motora
                //BindingOperations.SetBinding(tt3d, TranslateTransform3D.OffsetYProperty, new Binding("CP") { Source = motor });
                if (motor.Name3D == 0) continue;

                if (motor.Name3D >= m3dc.Count) continue;

                ((Transform3DGroup)m3dc[Convert.ToInt32(motor.Name3D)].Transform).Children.Add(tt3d);


                ////ovo ne treba, stavio za test
                //Slider s = new Slider() { Minimum = 0, Margin = new Thickness(5), Maximum = 1000, Height = 30 };
                //BindingOperations.SetBinding(s, Slider.ValueProperty, new Binding("CP") { Source = motor });
                //_wrapPanel.Children.Add(s);
            }

            t.Camera = m_CameraController.Camera;
            t.Width = t.Height = Double.NaN;

            return t;
        }




        private void Orth_Click(object sender, RoutedEventArgs e)
        {
            //vp3d.Camera = m_CameraController.OCamera;
            //vp3d.Camera = m_CameraController.PCamera;
            m_CameraController.Position = new Vector3D(1263, 56, -848);
            m_CameraController.Target = new Vector3D(0.5829, 0.3524, -0.7321);
        }

        private void Persp_Click(object sender, RoutedEventArgs e)
        {
            //vp3d.Camera = m_CameraController.PCamera;
            m_CameraController.Position = new Vector3D(2250, 89, -1765);
            m_CameraController.Target = new Vector3D(-0.7658, 0.2284, 0.6010);
        }

        private void OnViewportMouseUp(object sender, MouseButtonEventArgs e)
        {
            Mouse.Capture(e.MouseDevice.Target, CaptureMode.None);
        }

        private void OnViewportMouseDown(object sender, MouseButtonEventArgs e)
        {
            Mouse.Capture(e.MouseDevice.Target, CaptureMode.Element);
        }

        private void OnViewportMouseMove(object sender, MouseEventArgs e)
        {
            m_CameraController.Update(e);
        }

        private void CLeft_Click(object sender, RoutedEventArgs e)
        {
            m_CameraController.Position = new Vector3D(2243, 532, -1156);
            m_CameraController.Target = new Vector3D(-0.9607, -0.2759, -0.0294);
        }

        private void CBack_Click(object sender, RoutedEventArgs e)
        {
            m_CameraController.Position = new Vector3D(425, 246, -1174);
            m_CameraController.Target = new Vector3D(0.999, -0.009, 0.0311);
        }

        private void CFront_Click(object sender, RoutedEventArgs e)
        {
            m_CameraController.Position = new Vector3D(953, 70, -1201);
            m_CameraController.Target = new Vector3D(0.9631, 0.2674, 0.0301);
        }

        private void CPersp_Click(object sender, RoutedEventArgs e)
        {
            m_CameraController.Position = new Vector3D(1631, 531, -916);
            m_CameraController.Target = new Vector3D(-0.5865, -0.5721, -0.5732);
        }
        string GetObjectName(string s)
        {
            s = s.Replace("{", "");
            s = s.Replace("}", "");
            s = s.Replace("StaticResource", "");
            s = s.Replace(" ", "");
            return s;
        }
    }
    public class AnimateMotor
    {
        //public animateMotor(List<MotorLogic> motors, Viewport3D vp)
        //{
        //    if (vp == null || motors == null) return; //todo handle this

        //    Model3DCollection m3dc = ((Model3DGroup)((ModelVisual3D)vp.Children[0]).Content).Children;
        //    foreach (MotorLogic motor in motors)
        //    {
        //        Transform3DGroup t3dg = (Transform3DGroup)m3dc[motor.Name3D].Transform;//todo handle this
        //        if (t3dg == null) continue;

        //        list3D.Add(new motor3DTransform(motor.MotorID, TimeSpan.FromMilliseconds(300), (TranslateTransform3D)t3dg.Children[0]));
        //    }
        //}
        public AnimateMotor(List<MotorLogic> motors, Viewport3D vp)
        {
            if (vp == null || motors == null) return; //todo handle this

            Model3DCollection m3dc = ((Model3DGroup)((ModelVisual3D)vp.Children[0]).Content).Children;
            foreach (MotorLogic motor in motors)
            {
                if (motor.Name3D == 0) continue;
                Transform3DGroup t3dg = (Transform3DGroup)m3dc[motor.Name3D].Transform;//todo handle this
                TranslateTransform3D tt3d = new TranslateTransform3D();
                //tt3d.Offset
                t3dg.Children.Add(tt3d);
                if (t3dg == null) continue;

                list3D.Add(new motor3DTransform(motor.MotorID, TimeSpan.FromMilliseconds(300), tt3d));
            }
        }

        public static List<motor3DTransform> list3D = new List<motor3DTransform>();
        static motor3DTransform motTransf;
        public static void StartAnimation(int MotorId, double from, double to)
        {
            motTransf = list3D.Find(q => q.MotorId == MotorId);
            if (motTransf == null) return;//todo handle this

            motTransf.da.From = (from - 0) / 2.54;
            motTransf.da.To = (to - 0) / 2.54;
            //motTransf.da.From = (from - 120) / 2.54;   promenio II 2012
            //motTransf.da.To = (to - 120) / 2.54;

            motTransf.tt3d.BeginAnimation(TranslateTransform3D.OffsetYProperty, motTransf.da);
            //motTransf.tt3d.BeginAnimation(TranslateTransform3D.OffsetYProperty, motTransf.da,HandoffBehavior.Compose);
        }
        //public static void startAnimation(int motorID, double to)
        //{
        //    motTransf = list3D.Find(q => q.MotorId == motorID);
        //    if (motTransf == null) return;//todo handle this

        //    //motTransf.da.From = from;
        //    motTransf.da.To = to;

        //    motTransf.tt3d.BeginAnimation(TranslateTransform3D.OffsetYProperty, motTransf.da);
        //}
    }

    //klasa koja sadrzi animaciju i poiunter na transform3D
    public class motor3DTransform
    {
        public motor3DTransform(int motorID, Duration duration, TranslateTransform3D tt)
        {
            this.MotorId = motorID;
            this.da = new DoubleAnimation() { Duration = duration };
            this.tt3d = tt;
        }
        public TranslateTransform3D tt3d;
        public DoubleAnimation da;
        public int MotorId;
    }
}
