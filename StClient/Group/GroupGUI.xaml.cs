﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using LogAlertHB;

namespace StClient
{
    /// <summary>
    /// Interaction logic for Groups.xaml
    /// 
    /// 
    /// </summary>

    public partial class GroupGUI : UserControl
    {
        public GroupLogic _group;
        Commands commands = new Commands();

        public GroupGUI(GroupLogic group)
        {
            InitializeComponent();

            DataContext = group;
            MotorsInGroupListView.ItemsSource = group.ListviewMotorCollection;
            groupNumber.Text = "GROUP " + group.groupNo;
            _group = group;

            if (_group.groupNo > 1) RelJoy.Visibility = Visibility.Hidden;//hide button for group 3

        }

        private void resetTrip_Click(object sender, RoutedEventArgs e)
        {
            commands.resetTrip(_group.MotorsInGroup);
            Log.WriteDetailed(string.Format("groupGUI {0}: TRIP RST pressed", groupNumber.Text));
        }

        private void resetGroup_Click(object sender, RoutedEventArgs e)
        {
            commands.ResetGroup(this);
            Log.WriteDetailed(string.Format("groupGUI {0}: GRP RST pressed", groupNumber.Text));
        }

        private void view_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ShowMotor(this._group.MotorsInGroup);
        }

        private void Release_Click(object sender, RoutedEventArgs e)
        {
            commands.ManualStarting(_group.MotorsInGroup, _group);
            Log.WriteDetailed(string.Format("groupGUI {0}: RELEASE BRK pressed", groupNumber.Text));
        }
        private void ReleaseJoy_Click(object sender, RoutedEventArgs e)
        {
            if (_group.MotorsInGroup.Count == 0) return;
            double _maxV = _group.IsSync ? _group.MotorsInGroup.Min(we => we.MaxV) : _group.MotorsInGroup.Max(er => er.MaxV);
            //commands.ManualStarting(_group.MotorsInGroup, PhidgetsInterfaces.UniqueInstance.joystickList[_group.groupNo]);
            Sys.joystickList[_group.groupNo].SetJoystickMaxV(_maxV);
            commands.ManualStarting(_group.MotorsInGroup, Sys.joystickList[_group.groupNo]);
            Log.WriteDetailed(string.Format("groupGUI {0}: RELEASE JOY pressed", groupNumber.Text));
        }

        private void Up_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.Up_PreviewMouseLeftButtonDown(_group.MotorsInGroup);
            Log.WriteDetailed(string.Format("groupGUI {0}: MAN UP pressed", groupNumber.Text));
        }

        private void Up_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            commands.Up_PreviewMouseLeftButtonUp(_group.MotorsInGroup);
            Log.WriteDetailed(string.Format("groupGUI {0}: MAN UP released", groupNumber.Text));
        }

        private void Down_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.Down_PreviewMouseLeftButtonDown(_group.MotorsInGroup);
            Log.WriteDetailed(string.Format("groupGUI {0}: MAN DOWN pressed", groupNumber.Text));
        }

        private void Down_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            commands.Down_PreviewMouseLeftButtonUp(_group.MotorsInGroup);
            Log.WriteDetailed(string.Format("groupGUI {0}: MAN DOWN released", groupNumber.Text));
        }

        private void SP_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.SP_Popup(_group.MotorsInGroup, sender as TextBlock, false);
            Log.WriteDetailed(string.Format("groupGUI: SP changed to {1}, group={0}.", _group.groupNo, _group.SP));
        }

        private void SV_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.SVMV_Popup(_group.MotorsInGroup, sender as TextBlock, false, _group.IsSync ? SyncMode.Sync : SyncMode.NoSync, ShowPercButtons.Show);
            Log.WriteDetailed(string.Format("groupGUI: SV changed to {1}, group={0}.", _group.groupNo, _group.SV));
        }

        private void MV_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.SVMV_Popup(_group.MotorsInGroup, sender as TextBlock, false, _group.IsSync ? SyncMode.Sync : SyncMode.NoSync, ShowPercButtons.Show);
            Log.WriteDetailed(string.Format("groupGUI: MV changed to {1}, group={0}.", _group.groupNo, _group.MV));
        }

        private void Auto_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.AutoStarting(_group.MotorsInGroup, _group);
            Log.WriteDetailed(string.Format("groupGUI {0}: AUTO pressed", groupNumber.Text));
        }

        private void Stop_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.Stop(_group.MotorsInGroup);
            Log.WriteDetailed(string.Format("groupGUI {0}: STOP pressed", groupNumber.Text));
        }

        private void SyncV_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)SyncV.IsChecked) return;

            var tempV = _group.MotorsInGroup.Min(asd => asd.MaxV);
            _group.MV = _group.MV > tempV ? tempV : _group.MV;
            _group.SV = _group.SV > tempV ? tempV : _group.SV;
        }


    }
}


