﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StClient
{
    /// <summary>
    /// Interaction logic for Vagoni003.xaml
    /// </summary>
    public partial class L8400GUI : UserControl
    {
        //KompenzacionaLogic003 komp { get; set; }
        Commands commands = new Commands();
        L8400Logic l8400;

        public L8400GUI(L8400Logic _l8400)
        {
            InitializeComponent();

            this.DataContext = _l8400;
            l8400 = _l8400;
            //this.komp = _komp;


        }
        public L8400GUI()
        {
            InitializeComponent();
        }

        private void Up_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UdpTx.UniqueInstance.SendHoistPDO(l8400.Up);
        }

        private void Down_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UdpTx.UniqueInstance.SendHoistPDO(l8400.Down);
        }

        private void Up_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            UdpTx.UniqueInstance.SendHoistPDO(0);
        }

        private void Down_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            UdpTx.UniqueInstance.SendHoistPDO(0);
        }

        //private void ToggleBB_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { ((ToggleButton)sender).IsChecked = true; }
        //private void ToggleBB_MouseUp(object sender, MouseButtonEventArgs e) { ((ToggleButton)sender).IsChecked = false; }

        private void resetTrip_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { commands.resetTrip(this.DataContext as KompenzacionaLogic003); }
        private void resetTrip_MouseUp(object sender, MouseButtonEventArgs e) { commands.resetTripMouseUp(this.DataContext as KompenzacionaLogic003); }
    }
}
