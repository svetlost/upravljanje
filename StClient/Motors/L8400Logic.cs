﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace StClient
{
    public class L8400Logic : INotifyPropertyChanged, ILinkCheck
    {
        int _motorID = 10, _row = 0, _column = 0;
        short _encodermotorid = 10;
        string _title = "hoist";
        double _cp = 0;
        double _coef = 1;
        long _up = 0, _down = 0;

        [XmlAttribute("UP")]
        public long Up { get { return _up; } set { _up = value; } }
        [XmlAttribute("DOWN")]
        public long Down { get { return _down; } set { _down = value; } }
        [XmlAttribute("EncoderMotorID")]
        public short EncoderMotorID { get { return _encodermotorid; } set { _encodermotorid = value; } }
        [XmlAttribute("Title")]
        public string Title { get { return _title; } set { if (_title != value) { _title = value; OnNotify("Title"); } } }
        [XmlAttribute("MotorID")]
        public int MotorID { get { return _motorID; } set { _motorID = value; } }

        int _CanAdr, _CanNet;
        string _CabinetTitle;
        [XmlAttribute("CanAdr")]
        public int CanAdr { get { return _CanAdr; } set { _CanAdr = value; } }
        [XmlAttribute("CanNet")]
        public int CanNet { get { return _CanNet; } set { _CanNet = value; } }
        [XmlAttribute("CabinetTitle")]
        public string CabinetTitle { get { return _CabinetTitle; } set { _CabinetTitle = value; } }

        bool _limneg = false, _limpos = false, _Group = false;
        [XmlIgnore]
        public bool LimNeg { get { return _limneg; } set { if (_limneg != value) { _limneg = value; OnNotify("LimNeg"); } } }
        [XmlIgnore]
        public bool LimPos { get { return _limpos; } set { if (_limpos != value) { _limpos = value; OnNotify("LimPos"); } } }
        [XmlIgnore]
        public bool Group { get { return _Group; } set { if (_Group != value) { _Group = value; OnNotify("Group"); } } }

        bool _can, _brake = false, _trip = false, _inh28 = false, _moving;
        [XmlIgnore]
        public bool Can { get { return _can; } set { if (_can != value) { _can = value; OnNotify("Can"); } } }
        public bool Brake { get { return _brake; } set { if (_brake != value) { _brake = value; OnNotify("Brake"); } } }
        public bool Trip { get { return _trip; } set { if (_trip != value) { _trip = value; OnNotify("Trip"); } } }
        public bool Inh28 { get { return _inh28; } set { if (_inh28 != value) { _inh28 = value; OnNotify("Inh28"); } } }
        public bool Moving { get { return _moving; } set { if (_moving != value) { _moving = value; OnNotify("Moving"); } } }
        double _cv;
        public double CV { get { return _cv; } set { if (_cv != value) { _cv = value; OnNotify("CV"); } } }
        public int CanResponseCount { get; set; }

        [XmlAttribute("ROW")]
        public int Row { get { return _row; } set { _row = value; } }
        [XmlAttribute("COLUMN")]
        public int Column { get { return _column; } set { _column = value; } }
        [XmlAttribute("COEFICIENT")]
        public double Coeficient { get { return _coef; } set { _coef = value; } }

        bool ischkchecked = false;
        [XmlIgnore]
        public bool isCHKchecked { get { return ischkchecked; } set { if (ischkchecked != value) { ischkchecked = value; OnNotify("isCHKchecked"); } } }

        [XmlIgnore]
        public double CP { get { return _cp; } set { if (_cp != value) { _cp = value / Coeficient; OnNotify("CP"); } } }

        public void PingDevice() { }

        //public void ResponseReceived() { _LinkIndicator = Can = true; }
        //public void ResetIndicator() { _LinkIndicator = Can = false; }
        public void ResponseReceived() { }
        public void ResetIndicator()
        {
            if (CanResponseCount > 0)
                _LinkIndicator = Can = true;
            else
                _LinkIndicator = Can = false;
            CanResponseCount = 0;
        }

        bool _LinkIndicator = false;
        [XmlIgnore]
        public bool LinkIndicator { get { return _LinkIndicator; } set { _LinkIndicator = value; OnNotify("LinkIndicator"); } }


        public L8400Logic() { }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion
    }
}
