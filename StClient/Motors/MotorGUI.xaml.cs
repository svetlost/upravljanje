﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using LogAlertHB;
using System.Net.Sockets;
using System.Windows.Controls.Primitives;
using System.Diagnostics;


namespace StClient
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class MotorGUI : UserControl
    {
        public MotorLogic _motor;
        List<MotorLogic> motors;
        Commands commands = new Commands();

        public MotorGUI(MotorLogic motor)
        {
            InitializeComponent();
            this.DataContext = motor;
            _motor = motor;
            motors = _motor.toList();

            BindingOperations.SetBinding(CanOffGrid, Grid.VisibilityProperty, new Binding("CanOffVisibility") { Source = motor });


            for (byte i = 0; i < Properties.Settings.Default.NumberOfGroups; i++)
                combo.Items.Add(i.ToString());

        }

        private void Release_Click(object sender, RoutedEventArgs e)
        {
            commands.ManualStarting(motors, _motor);
            Log.Write(string.Format("motorGUI {0}: RELEASE BRK pressed", _motor.Title), EventLogEntryType.Information);

        }

        private void Up_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.Up_PreviewMouseLeftButtonDown(motors);
            Log.Write(string.Format("motorGUI {0}: MAN UP pressed", _motor.Title), EventLogEntryType.Information);

        }

        private void Up_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            commands.Up_PreviewMouseLeftButtonUp(motors);
            Log.Write(string.Format("motorGUI {0}: MAN UP released", _motor.Title), EventLogEntryType.Information);
        }

        private void Down_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.Down_PreviewMouseLeftButtonDown(motors);
            Log.Write(string.Format("motorGUI {0}: MAN DOWN pressed", _motor.Title), EventLogEntryType.Information);
        }

        private void Down_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            commands.Down_PreviewMouseLeftButtonUp(motors);
            Log.Write(string.Format("motorGUI {0}: MAN DOWN released", _motor.Title), EventLogEntryType.Information);
        }

        private void resetTrip_Click(object sender, RoutedEventArgs e)
        {
            commands.resetTrip(motors);
            Log.Write(string.Format("motorGUI {0}: TRIP RST pressed", _motor.Title), EventLogEntryType.Information);
        }

        private void Auto_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.AutoStarting(motors, _motor);
            Log.Write(string.Format("motorGUI {0}: AUTO pressed", _motor.Title), EventLogEntryType.Information);
        }

        private void Stop_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.Stop(motors);
            Log.Write(string.Format("motorGUI {0}: STOP pressed", _motor.Title), EventLogEntryType.Information);

        }

        private void resetGroup_Click(object sender, RoutedEventArgs e)
        {
            commands.ResetGroup(this);
            Log.Write(string.Format("motorGUI {0}: GRP RST pressed", _motor.Title), EventLogEntryType.Information);
        }

        private void SP_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!_motor.SpSvEnabled) return;
            commands.SP_Popup(motors, sender as TextBlock, false);
            Log.Write(string.Format("motorGUI: SP changed to {4}, motor ID={0}, title={1}, cp={2}, cv={3}.", _motor.MotorID, _motor.Title, _motor.CP, _motor.CV, _motor.iSpSv.SP), EventLogEntryType.Information);
        }

        private void SV_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!_motor.SpSvEnabled) return;
            commands.SVMV_Popup(motors, sender as TextBlock, false, SyncMode.NoSync, ShowPercButtons.Show);
            Log.Write(string.Format("motorGUI: SV changed to {4}, motor ID={0}, title={1}, cp={2}, cv={3}.", _motor.MotorID, _motor.Title, _motor.CP, _motor.CV, _motor.iSpSv.SV), EventLogEntryType.Information);
        }

        private void MV_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!_motor.SpSvEnabled) return;
            commands.SVMV_Popup(motors, sender as TextBlock, false, SyncMode.NoSync, ShowPercButtons.Show);
            Log.Write(string.Format("motorGUI: MV changed to {4}, motor ID={0}, title={1}, cp={2}, cv={3}.", _motor.MotorID, _motor.Title, _motor.CP, _motor.CV, _motor.iMv.MV), EventLogEntryType.Information);
        }
    }

}