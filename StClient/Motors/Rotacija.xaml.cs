﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
//using System.Windows.Controls.Primitives.

namespace StClient
{
    /// <summary>
    /// Interaction logic for Rotacija.xaml
    /// </summary>
    public partial class Rotacija : UserControl
    {
        Commands commands = new Commands();
        public Rotacija()
        {
            InitializeComponent();

            //20140512radovic
            //DataContext = Sys.vagoniRxTx;
            //c01.DataContext = c02.DataContext = c03.DataContext = c04.DataContext = c05.DataContext = c06.DataContext = c07.DataContext = c08.DataContext = Sys.vagoniRxTx;
            //c34.DataContext = c35.DataContext = Sys.vagoniRxTx;

            //AutoPanel.DataContext = Sys.vagoniRxTx;

            //RotInn.DataContext = Sys.vagoniRxTx.RotInn;
            //RotOut.DataContext = Sys.vagoniRxTx.RotOut;
        }

        private void ToggleBB_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { ((ToggleButton)sender).IsChecked = true; }
        private void ToggleBB_MouseUp(object sender, MouseButtonEventArgs e) { ((ToggleButton)sender).IsChecked = false; }

        private void SP_Inn_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //20140512radovic
            //commands.SP_Rot_Popup((TextBlock)sender);
            //Sys.vagoniRxTx.UpdateRotSP();
        }
        private void SP_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.SP_Rot_Popup((TextBlock)sender);
        }
        private void StopRot(object sender, RoutedEventArgs e)
        {
            //20140512radovic
            //Sys.vagoniRxTx.rotStop();
        }
    }
}
