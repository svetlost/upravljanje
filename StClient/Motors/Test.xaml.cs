﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace StClient
{
    /// <summary>
    /// Interaction logic for Test.xaml
    /// </summary>
    public partial class Test : UserControl
    {
        List<CheckBox> selectedMotors = new List<CheckBox>();
        DispatcherTimer timer = new DispatcherTimer();
        DispatcherTimer timerUp = new DispatcherTimer();
        DispatcherTimer timerDown = new DispatcherTimer();
        DispatcherTimer timerX = new DispatcherTimer();
        TimeSpan TimeUp, TimeDown, TimeInit = TimeSpan.FromMilliseconds(1000);
        public Test()
        {
            InitializeComponent();
            for (int i = 6; i < 20; i++)
            {
                if (i == 10 || i == 15) continue;
                CheckBox t = new CheckBox() { IsChecked = false, Content = "BAR " + i.ToString(), Tag = i };
                selectedMotors.Add(t);
                MotorSelection.Children.Add(t);
            }
            //timer.Tick += new EventHandler(timer_Tick);
            //timerUp.Tick += new EventHandler(timerUp_Tick);
            //timerDown.Tick += new EventHandler(timerDown_Tick);
            timerX.Interval = TimeInit;
            timerX.Tick += new EventHandler(timerX_Tick);
        }

        void timerX_Tick(object sender, EventArgs e)
        {
            if ((timerX.Interval == TimeInit) || (timerX.Interval == TimeDown))
            {
                timerUp_Tick(sender, e);
                timerX.Interval = TimeUp;
            }
            else
            {
                timerDown_Tick(sender, e);
                timerX.Interval = TimeDown;
            }
        }

        void timerDown_Tick(object sender, EventArgs e)
        {
            foreach (CheckBox c in selectedMotors.FindAll(w => w.IsChecked == true))
            {
                int delay = Convert.ToInt32(_dT.Text) * ((int)c.Tag - 5);
                UdpTx.SendSDOWriteRQ((int)c.Tag, CanStructure.SpIndex, CanStructure.SpSubIndex, Convert.ToInt32(_INITPOS.Text), SdoPriority.hi);
                UdpTx.SendSDOWriteRQ((int)c.Tag, CanStructure.SvIndex, CanStructure.SvSubIndex, Convert.ToInt32(_SV.Text), SdoPriority.hi);
                UdpTx.SendPDO((int)c.Tag, CanStructure.CanCommandAuto, delay, 2);
                UdpTx.SendPDO((int)c.Tag, CanStructure.CanCommandIdle, delay + 2200, 2);
            }
        }

        void timerUp_Tick(object sender, EventArgs e)
        {
            foreach (CheckBox c in selectedMotors.FindAll(w => w.IsChecked == true))
            {
                int delay = Convert.ToInt32(_dT.Text) * ((int)c.Tag - 5);
                UdpTx.SendSDOWriteRQ((int)c.Tag, CanStructure.SpIndex, CanStructure.SpSubIndex, Convert.ToInt32(_INITPOS.Text) + Convert.ToInt32(_HEIGHT.Text), SdoPriority.hi);
                UdpTx.SendSDOWriteRQ((int)c.Tag, CanStructure.SvIndex, CanStructure.SvSubIndex, Convert.ToInt32(_SV.Text), SdoPriority.hi);
                UdpTx.SendPDO((int)c.Tag, CanStructure.CanCommandAuto, delay, 2);
                UdpTx.SendPDO((int)c.Tag, CanStructure.CanCommandIdle, delay + 2200, 2);
            }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            timerDown.Start();
        }

        private void InitButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (CheckBox c in selectedMotors.FindAll(w => w.IsChecked == true))
            {
                UdpTx.SendSDOWriteRQ((int)c.Tag, CanStructure.SpIndex, CanStructure.SpSubIndex, Convert.ToInt32(_INITPOS.Text), SdoPriority.hi);
                UdpTx.SendSDOWriteRQ((int)c.Tag, CanStructure.SvIndex, CanStructure.SvSubIndex, Convert.ToInt32(_SV.Text), SdoPriority.hi);
                UdpTx.SendPDO((int)c.Tag, CanStructure.CanCommandAuto, 300, 2);
                UdpTx.SendPDO((int)c.Tag, CanStructure.CanCommandIdle, 2200, 2);
            }
            timerX.Interval = TimeSpan.FromMilliseconds(1000);
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            //timer.Stop();
            //timerDown.Stop();
            //timerUp.Stop();
            timerX.Stop();
            timerX.Interval = TimeInit;
            foreach (CheckBox c in selectedMotors.FindAll(w => w.IsChecked == true))
            {
                UdpTx.SendPDO((int)c.Tag, CanStructure.CanCommandStop, 0, 2);
                UdpTx.SendPDO((int)c.Tag, CanStructure.CanCommandIdle, 2200, 2);
            }
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            //UpDown = true;
            //timerX.Interval = TimeSpan.FromMilliseconds(Convert.ToDouble(_CYCLETIME.Text));
            TimeUp = TimeSpan.FromMilliseconds(Convert.ToDouble(_CYCLETIME.Text));
            TimeDown = TimeSpan.FromMilliseconds(Convert.ToDouble(_CYCLETIME.Text)+300);
            timerX.Interval = TimeInit;
            timerX.Start();
            //timerUp.Interval = TimeSpan.FromMilliseconds(Convert.ToDouble( _CYCLETIME.Text));
            //timerDown.Interval = TimeSpan.FromMilliseconds(Convert.ToDouble(_CYCLETIME.Text));
            //timer.Interval = TimeSpan.FromMilliseconds(Convert.ToDouble(_CYCLETIME.Text)/2);
            //timerUp.Start();
            //timer.Start();
        }
    }
}
