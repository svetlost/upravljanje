﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Threading;
using LogAlertHB;

namespace StClient
{
    class ApplicationStateLogic : INotifyPropertyChanged
    {
        private static volatile ApplicationStateLogic instance;
        private static object syncRoot = new Object();

        bool _isappenabled = false;
        //bool _isappenabled = true;  //todo vrati na false test 20130929

        public bool isAppEnabled
        {
            get { return _isappenabled; }
            set
            {
                if (_isappenabled != value)
                {
                    _isappenabled = value; OnNotify("isAppEnabled");
                    Log.WriteDetailed(string.Format("App enabled={0}", _isappenabled));
                }
            }
        }

        bool _PilzActive = false;
        public bool PilzActive
        {
            get { return _PilzActive; }
            set
            {
                if (_PilzActive != value)
                {
                    _PilzActive = value; OnNotify("PilzActive");
                    Log.WriteDetailed(string.Format("Pilz active={0}", value));
                    WriteAxesCurrentData();
                }
            }
        }
        bool _Estop1Pressed = false;
        public bool Estop1Pressed
        {
            get { return _Estop1Pressed; }
            set
            {
                if (_Estop1Pressed != value)
                {
                    _Estop1Pressed = value;
                    OnNotify("Estop1Pressed"); OnNotify("Estop_Pressed");
                    Log.WriteDetailed(string.Format("Estop1Pressed={0}", value));
                    WriteAxesCurrentData();
                }
            }
        }

        bool _Estop2Pressed = false;
        public bool Estop2Pressed
        {
            get { return _Estop2Pressed; }
            set
            {
                if (_Estop2Pressed != value)
                {
                    _Estop2Pressed = value;
                    OnNotify("Estop2Pressed"); OnNotify("Estop_Pressed");
                    Log.WriteDetailed(string.Format("Estop2Pressed={0}", value));
                    WriteAxesCurrentData();
                }
            }
        }
        bool _GelbauOk = false;
        public bool GelbauOk
        {
            get { return _GelbauOk; }
            set
            {
                if (_GelbauOk != value)
                {
                    _GelbauOk = value;
                    OnNotify("GelbauOk");
                    Log.WriteDetailed(string.Format("GelbauActivated={0}", value)); WriteAxesCurrentData();
                }
            }
        }
        public bool Estop_Pressed { get { return _Estop1Pressed || _Estop2Pressed; } }
        public string tempText;
        private ApplicationStateLogic()
        { }

        public static ApplicationStateLogic Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new ApplicationStateLogic();
                    }
                }

                return instance;
            }
        }

        public void UpdateValues() { OnNotify("Estop_Pressed"); OnNotify("GelbauOk"); }

        void WriteAxesCurrentData()
        {
            foreach (MotorLogic ml in Sys.StaticMotorList)
                Log.WriteDetailed(string.Format("\t Axis id={0,5} name={1,10} cp={2,10} cv={3,10} state={4,10} ky3={5,5}",
                        ml.MotorID, ml.Title, ml.CP, ml.CV, ml.state, ml.SbcLock));
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }

        #endregion
    }
}
