﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace StClient
{
    public class Cluster
    {
        string _name="cluster1";
        int _ClusterID;

        [XmlAttribute("ClusterID")]
        public int ClusterID { get { return _ClusterID; } set { _ClusterID = value; } }

        [XmlAttribute("Name")]
        public string Name { get { return _name; } set { _name = value; } }
    }
}
