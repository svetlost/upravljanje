﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace StClient
{
    /// <summary>
    /// Interaction logic for GelbauGUI.xaml
    /// </summary>
    public partial class GelbauGUI : UserControl
    {
        public GelbauGUI()
        {
            InitializeComponent();

            dt.Interval = TimeSpan.FromSeconds(3);
            dt.Tick += Dt_Tick;
            dt.Start();

        }
        int _gelbauCount;
        private void Dt_Tick(object sender, EventArgs e)
        {
            if (_gelbauCount != GelbauLogic.gelbauCount && GelbauLogic.gelbauCount > 0) { Refresh(); _gelbauCount = GelbauLogic.gelbauCount; }//todo cleanup
        }

        DispatcherTimer dt = new DispatcherTimer();//todo cleanup
        public void Refresh()
        {
            wpGelbau.Children.Clear();

            foreach (GelbauLogic glb in GelbauLogic.gelbauList)
            {
                Signal sig = new Signal() { Width = 85, text = glb.Name, VerticalAlignment = VerticalAlignment.Center, onColor = Brushes.Red, offColor = Brushes.Black, Margin = new Thickness(2) };
                BindingOperations.SetBinding(sig, Signal.valueProperty, new Binding("Trip") { Source = glb });
                wpGelbau.Children.Add(sig);
            }
        }
    }
}
