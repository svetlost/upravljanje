﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace StConfig
{
    public class StClusters
    {
        int _projectID = 7, _clusterID = 1;
        string _clusterName = "T1", _desc = "hall t1";
        bool _used = false;

        [XmlAttribute("projectID")]
        public int projectID { get { return _projectID; } set { _projectID = value; } }
        [XmlAttribute("clusterID")]
        public int clusterID { get { return _clusterID; } set { _clusterID = value; } }

        [XmlAttribute("clusterName")]
        public string clusterName { get { return _clusterName; } set { _clusterName = value; } }

        [XmlAttribute("desc")]
        public string desc { get { return _desc; } set { _desc = value; } }
        [XmlAttribute("used")]
        public bool used { get { return _used; } set { _used = value; } }



    }
}
