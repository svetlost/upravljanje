﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace StConfig
{
    public class StProject
    {
        int _projectID = 7;
        string _projectName = "T1", _serverAddress = "10.1.1.11", _clientAddress = "10.1.1.22", _desc = "";

        [XmlAttribute("projectID")]
        public int projectID { get { return _projectID; } set { _projectID = value; } }

        [XmlAttribute("projectName")]
        public string projectName { get { return _projectName; } set { _projectName = value; } }

        [XmlAttribute("serverAddress")]
        public string serverAddress { get { return _serverAddress; } set { _serverAddress = value; } }

        [XmlAttribute("clientAddress")]
        public string clientAddress { get { return _clientAddress; } set { _clientAddress = value; } }

        [XmlAttribute("desc")]
        public string desc { get { return _desc; } set { _desc = value; } }

    }

    public class StRfids
    {
        int _projectID = 7;
        string _projectName = "T1", _userName = "Tom", _userID= "0106fabe47", _desc = "tom the operator";

        [XmlAttribute("projectID")]
        public int projectID { get { return _projectID; } set { _projectID = value; } }

        [XmlAttribute("projectName")]
        public string projectName { get { return _projectName; } set { _projectName = value; } }

        [XmlAttribute("userName")]
        public string userName { get { return _userName; } set { _userName = value; } }

        [XmlAttribute("userID")]
        public string userID { get { return _userID; } set { _userID = value; } }

        [XmlAttribute("desc")]
        public string desc { get { return _desc; } set { _desc = value; } }

    }

}
