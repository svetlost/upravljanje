﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.IO;
using System.Windows.Data;
using System.Windows.Markup;
using System.Diagnostics;
using LogAlertHB;
using System.Windows.Input;
using StClient.Properties;
using StClient.Wagons;
using System.Xml.Serialization;
using StConfig;

namespace StClient
{
    public class Sys
    {
        //Unique Instances

        //public static TabControl TabControl;
        public static MotorGUI SelectedMotor;
        public static GroupGUI SelectedGroup;
        public static CuesMotorGUI SelectedCuesMotorGUIMotor;
        public static ListBox MotorListbox, GroupListBox, CuesMotorGUIListBox;
        //public static PhidgetItem phitem;
        public static TextBox receivedTxtBlock;
        public static string ServerIP { get; private set; }
        public static string ClientIP { get; private set; }
        Commands commands = new Commands();

        public NkkControl Nkk;

        //static LISTS
        public static List<MotorLogic> StaticMotorList;
        List<MotorLogic> ReadMotorList;
        public static List<GroupLogic> StaticGroupList = new List<GroupLogic>();
        //public static List<DioBrakeCntrl> StaticDioList = new List<DioBrakeCntrl>();
        public static List<DiagItemValues> StaticClusterElectricList = new List<DiagItemValues>();
        public static List<TripXML9300> StaticListOfTripsLenze9300 = new List<TripXML9300>();
        public static List<TripXML9400> StaticListOfTripsLenze9400 = new List<TripXML9400>();
        public static List<L8400Logic> StaticL8400List = new List<L8400Logic>();
        public static List<StartecLogic> StaticStartecList = new List<StartecLogic>();
        public static StartecGroupLogic StartecGroupLogic = new StartecGroupLogic();
        public static List<SecondaryBreakControl> SbcList = new List<SecondaryBreakControl>();
        public static List<ServerLink> ServersList = new List<ServerLink>();
        public static List<StJoystick> joystickList;
        public static List<RfidItem> rfidList;
        public static List<ILinkCheck> linkCheckDeviceList = new List<ILinkCheck>();
        public static List<Cabinet> cabinetList = new List<Cabinet>();
        public static List<Dio1000> DioList = new List<Dio1000>();
        public static List<DrivePlc> DrivePlcList = new List<DrivePlc>();
        public static List<StServer> StServerList = new List<StServer>();
        public static List<GelbauLogic> gelbauList = new List<GelbauLogic>();
        public static StServer stServer;
        //public static List<VagonLogic003> L8400List = new List<VagonLogic003>();
        //        public static L8400Logic L8400_PM, L8400_Hoist1, L8400_Hoist3, L8400_DC_PROSC1, L8400_DC_PROSC2, L8400_DC_PROSC3, L8400_RC_PROSC1, L8400_RC_PROSC2;

        //string path = Environment.CurrentDirectory + @"\Config\";
        static string _pathConfig = Environment.CurrentDirectory + @"\Config\";
        public static string pathConfig { get { return _pathConfig; } set { if (_pathConfig != value) { _pathConfig = value; } } }

        public static byte _KompenzacioneCanNetworkId = (byte)Properties.CanSettings.Default.KompenzacioneCanNetworkID;

        //Class Specific
        GroupLogic _tempGroupLogic;
        //DioBrakeCntrl _dioItem;

        DispatcherTimer SecBrkControlSendTimer = new DispatcherTimer();
        //DispatcherTimer LinkIndicator_SendPing = new DispatcherTimer();
        //DispatcherTimer LinkIndicator_ResetIndicator = new DispatcherTimer();
        //public DispatcherTimer DiagItemsUpdateTimer = new DispatcherTimer();
        //public DispatcherTimer HoistCPUpdateTimer = new DispatcherTimer();

        //init
        //XmlUtil<TripXML9300> t = new XmlUtil<TripXML9300>();
        XmlUtil<TripXML9400> t9400 = new XmlUtil<TripXML9400>();
        XmlUtil<MotorLogic> x = new XmlUtil<MotorLogic>();
        //XmlUtil<L8400Logic> s = new XmlUtil<L8400Logic>();
        XmlUtil<SecondaryBreakControl> sbc = new XmlUtil<SecondaryBreakControl>();
        XmlUtil<StJoystick> JoystickXml = new XmlUtil<StJoystick>();
        XmlUtil<RfidItem> rfidXml = new XmlUtil<RfidItem>();

        XmlUtil<StProject> xProj = new XmlUtil<StProject>();
        XmlUtil<StClusters> xCluster = new XmlUtil<StClusters>();
        public static List<StClusters> ReadClusterList { get; private set; }
        StProject selectedProj;

        //XmlUtil<StServer> StServerXml = new XmlUtil<StServer>();
        //XmlUtil<Dio1000> Dio1000Xml = new XmlUtil<Dio1000>();
        //XmlUtil<DrivePlc> DrivePlcXml = new XmlUtil<DrivePlc>();


        public static LockMotors lockMotors;

        //static VagoniRxTx _vagoniRxTx = new VagoniRxTx();
        //public static VagoniRxTx vagoniRxTx { get { return _vagoniRxTx; } }
        //public static VagoniRxTx vagoniRxTx = new VagoniRxTx();

        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;

        public static bool LocksEnabled = Properties.Settings.Default.LckEnabled;
        public static bool LocksOverrideEnabled = Properties.Settings.Default.LckOvrEnabled;

        //double _SvRotInner, _SvRotOuter;
        //public double SvRotInner { get { return _SvRotInner; } set { if (_SvRotInner != value) { _SvRotInner = value; OnNotify("SvRotInner"); } } }
        //public double SvRotOuter { get { return _SvRotOuter; } set { if (_SvRotOuter != value) { _SvRotOuter = value; OnNotify("SvRotOuter"); } } }

        //public static int potSvRotInner;
        //public static int potSvRotOuter;

        public static bool render3D = Properties.Settings.Default.Render3D;
        public static int movingToFalseCounter = Properties.Settings.Default.MovingToFalseCounter;

        public static string initResult { get; set; }
        public static void AddInitResult(string s) { initResult += s + "\n"; }

        public static string ParseResult(string filename, object list, int itemsImported)
        {
            if (list == null) return "Parsing " + filename + " failed, no records imported.";

            return "Parsing " + filename + " OK, implrted " + itemsImported.ToString() + " items";
        }
        public static string ParseResult2<T>(string filename, List<T> list)
        {
            if (list == null) return "Parsing " + filename + " FAILED, no items imported.";

            return "Parsing " + filename + " OK, imported " + list.Count + " items";
        }



        public Sys()
        {
            if (!Directory.Exists(pathConfig))
                Directory.CreateDirectory(pathConfig);

            //CREATE GROUPS
            for (byte i = 0; i < Properties.Settings.Default.NumberOfGroups; i++)
            {
                _tempGroupLogic = new GroupLogic(i);
                StaticGroupList.Add(_tempGroupLogic);
            }

            //must be after for needed cues
            #region NKK
            Nkk = new NkkControl(MainWindow.mainWindowDispacher, Properties.Settings.Default.NkkComPort);
            NkkControl.CommInitCheck();

            TimedAction.ExecuteWithDelay(new Action(delegate
            {
                MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
                           {
                               AddInitResult(string.Format("NkkInitOK={0} on port {1}", NkkControl.CommCheckOk, Properties.Settings.Default.NkkComPort));
                               Sys.lockMotors.enableMotion = false;
                           });
            }), TimeSpan.FromSeconds(2));

            NkkControl.NkkButtons.Add(new NkkControl.NkkButton() { ButtonNo = 3, Color = NkkControl.NkkColors.cyan, Text = "CUE NEXT", OnSwitchPress = CueLogic.UniqueInstance.NextCue });
            NkkControl.NkkButtons.Add(new NkkControl.NkkButton() { ButtonNo = 2, Color = NkkControl.NkkColors.cyan, Text = "CUE PREV", OnSwitchPress = CueLogic.UniqueInstance.PreviousCue });

            NkkControl.NkkButtons.Add(new NkkControl.NkkButton() { ButtonNo = 1, Color = NkkControl.NkkColors.red, Text = "ALL STOP", OnSwitchPress = commands.StopAllMotors });

            NkkControl.NkkButtons.Add(new NkkControl.NkkButton() { ButtonNo = 6, Color = NkkControl.NkkColors.red, Text = "CUE STOP", OnSwitchPress = CueLogic.UniqueInstance.Stop });

            NkkControl.NkkButtons.Add(new NkkControl.NkkButton()
            {
                ButtonNo = 4,
                Color = NkkControl.NkkColors.white,
                Text = "ENA BLE",
                OnSwitchPress = new Action(delegate () { Sys.lockMotors.enableMotion = true; }),
                OnSwitchRelease = new Action(delegate () { Sys.lockMotors.enableMotion = false; })
            });
            NkkControl.NkkButtons.Add(new NkkControl.NkkButton() { ButtonNo = 5, Color = NkkControl.NkkColors.green, Text = "CUE AUTO", OnSwitchPress = CueLogic.UniqueInstance.Play });

            //NkkControl.NkkButtons.Add(new NkkControl.NkkButton() { ButtonNo = 3, Color = NkkControl.NkkColors.cyan, Text = "CUE NEXT", OnSwitchPress = CueLogic.UniqueInstance.NextCue });
            //NkkControl.NkkButtons.Add(new NkkControl.NkkButton() { ButtonNo = 2, Color = NkkControl.NkkColors.cyan, Text = "CUE PREV", OnSwitchPress = CueLogic.UniqueInstance.PreviousCue });
            //NkkControl.NkkButtons.Add(new NkkControl.NkkButton() { ButtonNo = 1, Color = NkkControl.NkkColors.red, Text = "STOPALL", OnSwitchPress = new Action(delegate() { MessageBox.Show("1 stopall"); }) });
            //NkkControl.NkkButtons.Add(new NkkControl.NkkButton() { ButtonNo = 6, Color = NkkControl.NkkColors.red, Text = "CUE STOP", OnSwitchPress = new Action(delegate() { MessageBox.Show("6 stop"); }) });
            //NkkControl.NkkButtons.Add(new NkkControl.NkkButton() { ButtonNo = 4, Color = NkkControl.NkkColors.white, Text = "ENA BLE", OnSwitchPress = new Action(delegate() { MessageBox.Show("4 enable"); }), OnSwitchRelease = new Action(delegate() { MessageBox.Show("4 enable"); }) });
            //NkkControl.NkkButtons.Add(new NkkControl.NkkButton() { ButtonNo = 5, Color = NkkControl.NkkColors.green, Text = "CUE AUTO", OnSwitchPress = new Action(delegate() { MessageBox.Show("5 cue auto"); }) });

            NkkControl.Init_4N_4N();

            //CanSett.PDO2_RX_BaseID = CanSett.UseDefaultLenzePdoAddresses ? 641 : 704;

            #region old Nkk init
            //NkkControl.Write_4N_4N_Chars(3, "CUE NEXT");
            //NkkControl.SetColorToButton(NkkControl.NkkColors.cyan, 3);
            //NkkControl.NkkButtons[2].OnSwitchPress = CueLogic.UniqueInstance.NextCue;
            //NkkControl.Write_4N_4N_Chars(2, "CUE PREV");
            //NkkControl.SetColorToButton(NkkControl.NkkColors.cyan, 2);
            //NkkControl.NkkButtons[1].OnSwitchPress = CueLogic.UniqueInstance.PreviousCue;

            //NkkControl.Write_4N_4N_Chars(1, "STOPALL");
            //NkkControl.SetColorToButton(NkkControl.NkkColors.red, 1);
            //NkkControl.NkkButtons[0].OnSwitchPress = CueLogic.UniqueInstance.StopAll;

            //NkkControl.Write_4N_4N_Chars(6, "CUE STOP");
            //NkkControl.SetColorToButton(NkkControl.NkkColors.red, 6);
            //NkkControl.NkkButtons[5].OnSwitchPress = CueLogic.UniqueInstance.Stop;

            //NkkControl.Write_4N_4N_Chars(4, "ENA BLE");
            //NkkControl.SetColorToButton(NkkControl.NkkColors.white, 4);
            //NkkControl.NkkButtons[3].OnSwitchPress = new Action(delegate() { });
            ////NkkControl.NkkEvents[3].OnSwitchRelease = ;
            //NkkControl.Write_4N_4N_Chars(5, "CUE AUTO");
            //NkkControl.SetColorToButton(NkkControl.NkkColors.green, 5);
            //NkkControl.NkkButtons[4].OnSwitchPress = CueLogic.UniqueInstance.Play;
            #endregion

            #endregion

            //TRIP LIST            
            //StaticListOfTripsLenze9300 = t.ReadXml(pathConfig + "tripListL9300-20.xml", CreateDefaultRow.DontCreate);
            //AddInitResult(ParseResult2("tripListL9300-20.xml", StaticListOfTripsLenze9300));

            StaticListOfTripsLenze9400 = t9400.ReadXml(pathConfig + "002Codelist_12_00_07.xml", CreateDefaultRow.DontCreate);
            AddInitResult(ParseResult2("002Codelist_12_00_07.xml", StaticListOfTripsLenze9400));

            //CREATE HOIST
            //StaticL8400List = s.ReadXml(pathConfig + "L8400_List.xml", CreateDefaultRow.Create);
            //AddInitResult(ParseResult2("L8400_List.xml", StaticL8400List));
            ////foreach (var m8400 in StaticL8400List) L8400List.Add(new L8400GUI(m8400.CanAdr, m8400.CanNet, m8400.Title, m8400.Up, m8400.Down, m8400.CabinetTitle));
            //L8400_PM = StaticL8400List.Find(dfg => dfg.CanAdr == 12);
            //L8400_Hoist1 = StaticL8400List.Find(dfg => dfg.CanAdr == 13);
            //L8400_Hoist3 = StaticL8400List.Find(dfg => dfg.CanAdr == 15);
            //L8400_DC_PROSC1 = StaticL8400List.Find(dfg => dfg.CanAdr == 17);
            //L8400_DC_PROSC2 = StaticL8400List.Find(dfg => dfg.CanAdr == 18);
            //L8400_DC_PROSC3 = StaticL8400List.Find(dfg => dfg.CanAdr == 19);
            //L8400_RC_PROSC1 = StaticL8400List.Find(dfg => dfg.CanAdr == 20);
            //L8400_RC_PROSC2 = StaticL8400List.Find(dfg => dfg.CanAdr == 21);

            //CREATE MOTORS           
            ReadMotorList = x.ReadXml(pathConfig + "motors_List.xml", CreateDefaultRow.Create);
            AddInitResult(ParseResult2("motors_List.xml", ReadMotorList));

            //CREATE JOYSTICKS
            joystickList = JoystickXml.ReadXml(Sys.pathConfig + "PhJoystick.xml", CreateDefaultRow.Create);
            AddInitResult(ParseResult2("PhJoystick.xml", joystickList));

            //READ RFIDS
            rfidList = rfidXml.ReadXml(Sys.pathConfig + "Rfids.xml", CreateDefaultRow.Create);
            AddInitResult(ParseResult2("Rfids.xml", rfidList));

            ReadClusterList = xCluster.ReadXml(Sys.pathConfig + "clusters.xml", CreateDefaultRow.Create);
            AddInitResult(ParseResult2("clusters.xml", ReadClusterList));

            selectedProj = xProj.ReadXml(Sys.pathConfig + "selectedProj.xml", CreateDefaultRow.Create)[0];
            List<StProject> tempList = new List<StProject>();
            tempList.Add(selectedProj);
            AddInitResult(ParseResult2("selectedProj.xml", tempList));
            Sys.ServerIP = selectedProj.serverAddress;
            Sys.ClientIP = selectedProj.clientAddress;

            //DioList = Dio1000Xml.ReadXml(Sys.pathConfig + "Dio1000.xml", CreateDefaultRow.Create);
            //AddInitResult(ParseResult2("Dio1000.xml", DioList));

            //DrivePlcList = DrivePlcXml.ReadXml(Sys.pathConfig + "DrvPlc.xml", CreateDefaultRow.Create);
            //AddInitResult(ParseResult2("DrvPlc.xml", DrivePlcList));



            //Log.Write(File.ReadAllText(path + "motors_List.xml"));
            StaticMotorList = ReadMotorList.FindAll(m => m.Enabled);

            //setup parent platforms
            foreach (var mot in ReadMotorList.Where(q => q.ParentMotorID != 0)) mot.ParentMotor = ReadMotorList.Find(w => w.MotorID == mot.ParentMotorID);

            //setup child motors
            var parentPlatforms = ReadMotorList.Where(w => w.ParentMotorID != 0).GroupBy(p => p.ParentMotorID).Select(q => q.First());
            foreach (var mmm in parentPlatforms) mmm.ChildMotors.AddRange(ReadMotorList.Where(q => q.ParentMotorID == mmm.MotorID));

            //CREATE SBC PLEC LIST
            SbcList = sbc.ReadXml(pathConfig + "sbc_List.xml", CreateDefaultRow.Create);
            AddInitResult(ParseResult2("sbc_List.xml", SbcList));

            //link check list
            //foreach (var mot in StaticMotorList) linkCheckDeviceList.Add(mot as ILinkCheck);
            //DioList.Add(new Dio1000(32, 0, "DIO32", "URD1"));
            //DioList.Add(new Dio1000(33, 0, "DIO33", "URD2"));

            //DioList.Add(new Dio1000(32, 1, "DIO32", "URG1"));
            //DioList.Add(new Dio1000(33, 1, "DIO33", "URG2"));
            //DioList.Add(new Dio1000(34, 1, "DIO34", "URG3"));
            //DioList.Add(new Dio1000(36, 1, "DIO36", "URG4"));

            //DrivePlcList.Add(new DrivePlc(40, 0, "PLC40DP", "ROS"));
            //DrivePlcList.Add(new DrivePlc(40, 1, "PLC40GP", "ROS"));

            //linkCheckDeviceList.Add(stServer = new StServer("Server", "ROS", Sett.ServerMainIP, Sett.ServerPort));

            //foreach (var ert in DioList) linkCheckDeviceList.Add(ert);
            //foreach (var ert in DrivePlcList) linkCheckDeviceList.Add(ert);
            //foreach (var ert in StaticL8400List) linkCheckDeviceList.Add(ert);

            //foreach (var ccc in Sys.linkCheckDeviceList.GroupBy(mnm => mnm.CabinetTitle)) cabinetList.Add(new Cabinet(ccc.ToList()));

            //LinkIndicator_SendPing.Interval = TimeSpan.FromMilliseconds(570);
            //LinkIndicator_SendPing.Tick += LinkIndicator_SendPing_Tick;
            //LinkIndicator_SendPing.Start();

            //LinkIndicator_ResetIndicator.Interval = TimeSpan.FromMilliseconds(1490);
            //LinkIndicator_ResetIndicator.Tick += LinkIndicator_ResetIndicator_Tick;
            //LinkIndicator_ResetIndicator.Start();

            //send goOperational
            TimedAction.ExecuteWithDelay(new Action(delegate
            {
                foreach (SecondaryBreakControl sbcs in SbcList) UdpTx.UniqueInstance.SendGoOperational(sbcs.CanChannel);
            }), TimeSpan.FromSeconds(4));

            //set sp to cp
            TimedAction.ExecuteWithDelay(new Action(delegate
            {
                foreach (MotorLogic motor in Sys.StaticMotorList.FindAll(mm => mm.Can)) motor.iSpSv.SP = motor.CP;
            }), TimeSpan.FromSeconds(4));



            if (StaticMotorList.Count > 0) lockMotors = new LockMotors();

            ServersList.Add(new ServerLink("Server MAIN", Properties.Settings.Default.ServerMainIP));
            ServersList.Add(new ServerLink("Server BACKUP", Properties.Settings.Default.ServerBackupIP));

            ////Diag Items Update TIMER
            //DiagItemsUpdateTimer.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.DiagItemsUpdateTimerInterval);
            //DiagItemsUpdateTimer.Tick += new EventHandler(DiagItemsUpdate);

            //Hoist Items CP Update TIMER
            //HoistCPUpdateTimer.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.HoistCPUpdateTimerInterval);
            //HoistCPUpdateTimer.Tick += new EventHandler(HoistCPUpdateTimer_Tick);

            AddInitResult("Motors parsed:");
            foreach (MotorLogic mm in StaticMotorList) AddInitResult(string.Format("Motor, {0}, id={1}, lim-={2} lim+={3} adr={4}, maxV={5}", mm.Title, mm.MotorID, mm.LimNeg, mm.LimPos, mm.CanAddress, mm.MaxV));
            AddInitResult("Hoists parsed:");
            foreach (L8400Logic mm in StaticL8400List) AddInitResult(string.Format("Hoist, {0}, id={1}", mm.Title, mm.MotorID));

            foreach (MotorLogic motor in StaticMotorList)
            {
                motor.SetMotorDirection();
                CueLogic.UniqueInstance.SubscribeMotorsToCue(motor);
                motor.SubscribeToCueLogic(CueLogic.UniqueInstance);

                motor.sbc = SbcList.Find(qq => qq.CanChannel == motor.CanChannel);


                //_dioItem = StaticDioList.Find(q => q.CanChannel == motor.CanChannel && q.Address == motor.SecBrkControlAddress);

                //if (_dioItem == null)
                //{
                //    _dioItem = new DioBrakeCntrl { Address = (byte)motor.SecBrkControlAddress, CanChannel = motor.CanChannel };
                //    //_dioItem.motorListinDIO.Add(motor);
                //    StaticDioList.Add(_dioItem);
                //}
                //else
                //{
                //    //_dioItem.motorListinDIO.Add(motor);
                //}

                //motor.FillDiagItems();

                if (motor.SdoChannel == 1)
                    motor.SdoCanMsgID = CanSett.SDO1_TX_BaseID;
                else if (motor.SdoChannel == 3)
                    motor.SdoCanMsgID = CanSett.SDO3_TX_BaseID;
                else
                    motor.SdoCanMsgID = CanSett.SDO2_TX_BaseID;

                motor.SDO_CanMsgID = BitConverter.GetBytes(motor.SdoCanMsgID + motor.CanAddress);
                //motor.PDO_CanMsgID = BitConverter.GetBytes(CanStructure.PDO2_TX_BaseID + motor.CanAddress);
                motor.PDO_CanMsgID = BitConverter.GetBytes(CanSett.PDO3_TX_BaseID + motor.CanAddress);

                //201102
                ////SBC SENDER TIMER - works only if SBC exists
                //if (StaticDioList.Count > 0)
                //{
                //SecBrkControlSendTimer.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.SBCSendInterval);
                //SecBrkControlSendTimer.Tick += new EventHandler(SecBrkSend);
                //SecBrkControlSendTimer.Start();
                //}

                if (!VisibilityStatesLogic.UniqueInstance.AllClusters.Contains(motor.ClusterLogical))
                    VisibilityStatesLogic.UniqueInstance.AllClusters.Add(motor.ClusterLogical);
            }

            VisibilityStatesLogic.UniqueInstance.FillOnStartup();

            //CLUSTER ELECTRIC
            foreach (byte _ClusterID in StaticMotorList.Select(q => q.ClusterElectric).Distinct())
            {
                DiagItemValues item = new DiagItemValues
                {
                    Cluster = _ClusterID,
                    Text = "Cabinet " + _ClusterID.ToString(),
                    Units = "A"
                };
                StaticClusterElectricList.Add(item);
            }

            if (Sett.L9400_init)
            {
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param(4011, 0, 200); }), TimeSpan.FromSeconds(4));//digdel2
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param(2700, 0, 1); }), TimeSpan.FromSeconds(5));//uklj pos limiter
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param(2702, 0, 1); }), TimeSpan.FromSeconds(7));//uklj spped limiter
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param3(2703, 0); }), TimeSpan.FromSeconds(8));//speed limit cm/s
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400Sdo_CobID(372, 3, 1664); }), TimeSpan.FromSeconds(9));//sdo za slavenaduRX
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400Sdo_CobID(373, 3, 1728); }), TimeSpan.FromSeconds(6));//sdo za slavenaduTX

                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param(5902, 1, 1); }), TimeSpan.FromSeconds(10));//sdo za slavenaduTX
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param(5902, 2, 1); }), TimeSpan.FromSeconds(11));//sdo za slavenaduTX
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param(582, 0, 1); }), TimeSpan.FromSeconds(12));//sdo za slavenaduTX
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param(589, 0, 1); }), TimeSpan.FromSeconds(13));//sdo za slavenaduTX
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param(600, 0, 1); }), TimeSpan.FromSeconds(14));//sdo za slavenaduTX
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param(604, 0, 1); }), TimeSpan.FromSeconds(15));//sdo za slavenaduTX
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param(596, 0, 1900); }), TimeSpan.FromSeconds(16));//sdo za slavenaduTX
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param(606, 0, 1); }), TimeSpan.FromSeconds(17));//sdo za slavenaduTX
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param(573, 0, 1); }), TimeSpan.FromSeconds(18));//sdo za slavenaduTX
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param(574, 0, 1); }), TimeSpan.FromSeconds(19));//sdo za slavenaduTX
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param(585, 0, 1); }), TimeSpan.FromSeconds(20));//sdo za slavenaduTX
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param(2716, 3, 0); }), TimeSpan.FromSeconds(21));//sdo za slavenaduTX

                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400Sdo_CobID(372, 2, 1600); }), TimeSpan.FromSeconds(4));//sdo za hmi
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400Sdo_CobID(373, 2, 1472); }), TimeSpan.FromSeconds(6));//sdo za hmi
                ////TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param(4513, 2, 2); }), TimeSpan.FromSeconds(8));//profil za pozicioniranje
                ////TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param(322, 1, 1); }), TimeSpan.FromSeconds(8));//pdo1 da bude sync a ne event

                //setup clock on 9400
                //DateTime dat = DateTime.Now;
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param2byte(1215, 1, dat.Second); }), TimeSpan.FromSeconds(24));
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param2byte(1215, 2, dat.Minute); }), TimeSpan.FromSeconds(25));
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param2byte(1215, 3, dat.Hour); }), TimeSpan.FromSeconds(26));
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param2byte(1215, 4, dat.Day); }), TimeSpan.FromSeconds(27));
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param2byte(1215, 5, dat.Month); }), TimeSpan.FromSeconds(28));
                //TimedAction.ExecuteWithDelay(new Action(delegate { send9400_Param2byte(1215, 6, dat.Year); }), TimeSpan.FromSeconds(29));
            }

            TimedAction.ExecuteWithDelay(new Action(delegate { foreach (var m in Sys.StaticMotorList) m.MV = m.SV = 0.5 * m.MaxV; }), TimeSpan.FromSeconds(3));


            //at init end write log string
            Log.Write(initResult, EventLogEntryType.Information);
        }

        //void LinkIndicator_ResetIndicator_Tick(object sender, EventArgs e)
        //{
        //    //2016 todo foreach (var poi in linkCheckDeviceList) poi.ResetIndicator();
        //}

        //void LinkIndicator_SendPing_Tick(object sender, EventArgs e)
        //{
        //   //2016 todo foreach (var poi in linkCheckDeviceList) poi.PingDevice();
        //}

        public static bool getBitOfInt(int i, int bitNumber) { return (i & (1 << bitNumber)) != 0; }

        public static bool IsOn(int Value, byte Bit) { return (Value >> Bit & 1) == 1; }
        public static int SetBitOfInt(int Value, byte Bit) { return SetBitOfInt(Value, Bit, true); }
        public static int SetBitOfInt(int Value, byte Bit, bool On) { return On ? Value | (1 << Bit) : ClearBitOfInt(Value, Bit); }
        public static int ClearBitOfInt(int Value, byte Bit) { return Value & ~(1 << Bit); }

        //20140514 dodato da se na 9400 nasetuju sdo kanali da bi hmi mogao da prima. todo napraviti ovo sa plc-a
        void send9400Sdo_CobID(int _index, byte _subindex, int _baseCobId)
        {
            foreach (var mot in Sys.StaticMotorList.Where(y => y.CanAddress <= 11))
                UdpTx.UniqueInstance.SendSDOWriteRQ_Raw(mot.MotorID, _index, _subindex, (_baseCobId + mot.CanAddress), SdoPriority.hi);
        }

        void send9400_Param(int _index, byte _subindex, int _value)
        {
            foreach (var mot in Sys.StaticMotorList.Where(y => y.CanAddress <= 11))
                UdpTx.UniqueInstance.SendSDOWriteRQ_Raw(mot.MotorID, _index, _subindex, _value, SdoPriority.hi);
        }
        void send9400_Param3(int _index, byte _subindex)
        {
            foreach (var mot in Sys.StaticMotorList.Where(y => y.CanAddress <= 11))
                UdpTx.UniqueInstance.SendSDOWriteRQ_Raw(mot.MotorID, _index, _subindex, mot.MaxV * 1.3 * 10000, SdoPriority.hi);
        }
        void send9400_Param2byte(int _index, byte _subindex, int _value)
        {
            foreach (var mot in Sys.StaticMotorList.Where(y => y.CanAddress <= 11))
                UdpTx.UniqueInstance.SendSDOWriteRQ_Raw2byte(mot.MotorID, _index, _subindex, _value, SdoPriority.hi);
        }
        void send9400_Read(int _index, byte _subindex)
        {
            foreach (var mot in Sys.StaticMotorList.Where(y => y.CanAddress <= 11))
                UdpTx.UniqueInstance.SendSDOReadRQ(mot.MotorID, _index, _subindex, SdoPriority.hi);
        }

        public static void printStack(MotorLogic ml)/////////skinuo2011
        {
            return;

            StackTrace st = new StackTrace();
            StackFrame[] sf = st.GetFrames();
            //Debug.WriteLine("*********");
            //Debug.WriteLine(string.Format("{0:hh:mm:ss.fff} stack=", DateTime.Now));
            string s = "";
            int max = Math.Min(6, sf.Length);
            //for (int i = 1; i < max; i++) Debug.Write(sf[i].GetMethod().Name + " ,");
            for (int i = 1; i < max; i++) s += sf[i].GetMethod().Name + " ,";

            //Debug.WriteLine("{0:hh:mm:ss.fff}>{1,15} id={2,3} state={3,10} cp={4,7} cpold={5,7}  deltaCp={6,5} sbc={7} mov={8,6} stack={9}",
            //    DateTime.Now, ml.Title, ml.MotorID, ml.state, ml.CP, ml.previousCP, Math.Abs(ml.CP - ml.previousCP), ml.SbcLock, ml.Moving, BitConverter.ToInt32(ml.sbc.PDO_CanMsgID, 0), s);

            //Debug.WriteLine("*********");
        }
        public static string printStack2()/////////skinuo2011
        {

            StackTrace st = new StackTrace();
            StackFrame[] sf = st.GetFrames();
            string s = "";
            int max = Math.Min(6, sf.Length);
            for (int i = 1; i < max; i++) s += sf[i].GetMethod().Name + " ,";
            return s;
        }

        void HoistCPUpdateTimer_Tick(object sender, EventArgs e)
        {
            int index = 1381;
            byte subindex = 61;
            for (int i = 26; i <= 29; i++)
                UdpTx.UniqueInstance.SendSDOReadRQ(i, index, subindex, SdoPriority.med);
            //UdpTx.UniqueInstance.SendSDOReadRQ(32, index, subindex, SdoPriority.med);
            //UdpTx.UniqueInstance.SendSDOReadRQ(34, index, subindex, SdoPriority.med);
            //UdpTx.UniqueInstance.SendSDOReadRQ(38, index, subindex, SdoPriority.med);
            //UdpTx.UniqueInstance.SendSDOReadRQ(42, index, subindex, SdoPriority.med);
        }
        // #endregion

        #region TIMERS
        void SecBrkSend(object sender, EventArgs e)
        {

            //DateTime ddd = DateTime.Now;
            //int data = 0;
            //foreach (DioBrakeCntrl dio in StaticDioList)
            //{
            //foreach (MotorLogic ml in Sys.StaticMotorList)
            //    if (ml.SbcLock == MotorBrake.Unlocked)
            //        data += 1 << ml.SbcBit;

            foreach (SecondaryBreakControl sbc in Sys.SbcList)
                //    if (starteclogic.SecBrkControlLock == MotorBrake.Unlocked)
                //        data += 1 << starteclogic.SecBrkControlBit;

                UdpTx.UniqueInstance.SendPDOSBC(sbc.CanChannel, sbc.PDO_CanMsgID, sbc.Output);
            //}
            //Debug.WriteLine(string.Format("{0} {1}", DateTime.Now.ToLongTimeString(), data));
            //Debug.WriteLine(DateTime.Now.ToLongTimeString() + "  " + (DateTime.Now - ddd).TotalMilliseconds);
        }
        void DiagItemsUpdate(object sender, EventArgs e)
        {
            foreach (MotorLogic m in StaticMotorList.FindAll(q => q.Can == true))
                foreach (DiagItemValues di in m.DiagItemsList)
                    UdpTx.UniqueInstance.SendSDOReadRQ(m.MotorID, di.LenzeIndex, di.LenzeSubIndex, SdoPriority.lo);

        }
        #endregion
    }

    public class TripXML9300
    {
        public int ID { get; set; }
        public string Error { get; set; }
        public string ErrorDescription { get; set; }
        public string CauseRemedy { get; set; }
    }

    //public class ErrorList
    //{
    //    // ELEMENTS
    //    [XmlElement("Error")]
    //    public List<Error> Error { get; set; }
    //}

    public class TripXML9400
    {
        // ATTRIBUTES
        [XmlAttribute("Number")]
        public int Number { get; set; }

        [XmlAttribute("RID")]
        public string RID { get; set; }

        // ELEMENTS
        [XmlText]
        public string Value { get; set; }
    }


    public class ServerLink : INotifyPropertyChanged
    {
        public string Name { get; set; }
        public string IpAddress { get; set; }
        int counter = 0;
        bool _LinkOk = false;
        public bool LinkOk { get { return _LinkOk; } set { if (_LinkOk != value) { _LinkOk = value; OnNotify("LinkOk"); } } }
        public void IncrementCounter() { counter++; }
        public void CheckLink() { LinkOk = counter > 0; counter = 0; }

        public ServerLink(string _name, string _IpAdress)
        {
            Name = _name;
            IpAddress = _IpAdress;
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
        #endregion
    }

    public static class TimedAction
    {
        public static void ExecuteWithDelay(Action action, TimeSpan delay)
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = delay;
            timer.Tag = action;
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }

        static void timer_Tick(object sender, EventArgs e)
        {
            DispatcherTimer timer = (DispatcherTimer)sender;
            Action action = (Action)timer.Tag;

            action.Invoke();
            timer.Stop();
        }
    }
}



