﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StClient
{
    /// <summary>
    /// Interaction logic for LinkCheckCabinet.xaml
    /// </summary>
    public partial class LinkCheckCabinetGUI : UserControl
    {
        public LinkCheckCabinetGUI(Cabinet cab)
        {
            InitializeComponent();

            DataContext = cab;

            foreach (var pp in cab.Devices) wrpPanel.Children.Add(new LinkCheckDeviceGUI() { DataContext = pp });
        }
        //public LinkCheckCabinetGUI(Cabinet cab)
        //{
        //    InitializeComponent();

        //    DataContext = cab;

        //    foreach (var pp in cab.Devices) wrpPanel.Children.Add(new LinkCheckDeviceGUI() { DataContext = pp });
        //}
    }
}
