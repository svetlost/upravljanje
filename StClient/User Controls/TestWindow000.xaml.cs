﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace StClient
{
    /// <summary>
    /// Interaction logic for TestWindow000.xaml
    /// </summary>
    public partial class TestWindow000 : Window
    {
        public TestWindow000()
        {
            InitializeComponent();
        }


        private void button_Click(object sender, RoutedEventArgs e)
        {


            try
            {
                //XmlReader xmlFile = XmlReader.Create("config\\motors_List.xml", new XmlReaderSettings());
                DataSet ds = new DataSet();
                //ds.ReadXml(xmlFile);
                ds.ReadXml("config\\motors_List.xml");
                dataGrid.ItemsSource = ds.Tables[0].DefaultView;

                // DataSet dataSet = new DataSet();
                //dataSet.ReadXml(@"C:\Books\Books.xml");
                //dataGrid1.ItemsSource = dataSet.Tables[0].DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }


        }
    }
}
