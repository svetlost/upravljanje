﻿#pragma checksum "..\..\..\Cues\CuesGUI.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "37789893F32844EF2731789BF07BC942"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using StClient;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace StClient {
    
    
    /// <summary>
    /// Cues
    /// </summary>
    public partial class Cues : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 16 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid CueGrid;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CueName;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button FileOpen;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button FileSave;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button FileDelete;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Folder;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tbPath;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button insertCue;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button updateCue;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ClearCurrent;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Copy;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.ToggleButton AutoView;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ViewCued;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CopyCP;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DeleteAllCues;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CueNone;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button UpdateMotors;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DeleteMotor;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SP_All;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SV_All;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CuePrevious;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CueDecrement;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox CueNumber;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CueIncrement;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CueNext;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal StClient.Signal sOvrd;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button resetTrip;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CuePrevious2;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Play;
        
        #line default
        #line hidden
        
        
        #line 97 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CueNext2;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Stop;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\..\Cues\CuesGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button StopAll;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/StClient;component/cues/cuesgui.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Cues\CuesGUI.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.CueGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.CueName = ((System.Windows.Controls.ComboBox)(target));
            
            #line 31 "..\..\..\Cues\CuesGUI.xaml"
            this.CueName.DropDownOpened += new System.EventHandler(this.ReadCueFilesInFolder);
            
            #line default
            #line hidden
            return;
            case 3:
            this.FileOpen = ((System.Windows.Controls.Button)(target));
            
            #line 34 "..\..\..\Cues\CuesGUI.xaml"
            this.FileOpen.Click += new System.Windows.RoutedEventHandler(this.FileOpen_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.FileSave = ((System.Windows.Controls.Button)(target));
            
            #line 35 "..\..\..\Cues\CuesGUI.xaml"
            this.FileSave.Click += new System.Windows.RoutedEventHandler(this.FileSave_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.FileDelete = ((System.Windows.Controls.Button)(target));
            
            #line 36 "..\..\..\Cues\CuesGUI.xaml"
            this.FileDelete.Click += new System.Windows.RoutedEventHandler(this.FileDelete_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.Folder = ((System.Windows.Controls.Button)(target));
            
            #line 41 "..\..\..\Cues\CuesGUI.xaml"
            this.Folder.Click += new System.Windows.RoutedEventHandler(this.ChangeFolder_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.tbPath = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.insertCue = ((System.Windows.Controls.Button)(target));
            
            #line 47 "..\..\..\Cues\CuesGUI.xaml"
            this.insertCue.Click += new System.Windows.RoutedEventHandler(this.insertCue_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.updateCue = ((System.Windows.Controls.Button)(target));
            
            #line 48 "..\..\..\Cues\CuesGUI.xaml"
            this.updateCue.Click += new System.Windows.RoutedEventHandler(this.updateCue_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.ClearCurrent = ((System.Windows.Controls.Button)(target));
            
            #line 49 "..\..\..\Cues\CuesGUI.xaml"
            this.ClearCurrent.Click += new System.Windows.RoutedEventHandler(this.ClearCurrent_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.Copy = ((System.Windows.Controls.Button)(target));
            
            #line 50 "..\..\..\Cues\CuesGUI.xaml"
            this.Copy.Click += new System.Windows.RoutedEventHandler(this.Copy_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.AutoView = ((System.Windows.Controls.Primitives.ToggleButton)(target));
            return;
            case 13:
            this.ViewCued = ((System.Windows.Controls.Button)(target));
            
            #line 55 "..\..\..\Cues\CuesGUI.xaml"
            this.ViewCued.Click += new System.Windows.RoutedEventHandler(this.ViewCued_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.CopyCP = ((System.Windows.Controls.Button)(target));
            
            #line 56 "..\..\..\Cues\CuesGUI.xaml"
            this.CopyCP.Click += new System.Windows.RoutedEventHandler(this.CopyCP_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.DeleteAllCues = ((System.Windows.Controls.Button)(target));
            
            #line 57 "..\..\..\Cues\CuesGUI.xaml"
            this.DeleteAllCues.Click += new System.Windows.RoutedEventHandler(this.DeleteAllCues_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.CueNone = ((System.Windows.Controls.Button)(target));
            
            #line 58 "..\..\..\Cues\CuesGUI.xaml"
            this.CueNone.Click += new System.Windows.RoutedEventHandler(this.CueNone_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.UpdateMotors = ((System.Windows.Controls.Button)(target));
            
            #line 64 "..\..\..\Cues\CuesGUI.xaml"
            this.UpdateMotors.Click += new System.Windows.RoutedEventHandler(this.UpdateMotors_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.DeleteMotor = ((System.Windows.Controls.Button)(target));
            
            #line 65 "..\..\..\Cues\CuesGUI.xaml"
            this.DeleteMotor.Click += new System.Windows.RoutedEventHandler(this.DeleteMotor_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.SP_All = ((System.Windows.Controls.Button)(target));
            
            #line 66 "..\..\..\Cues\CuesGUI.xaml"
            this.SP_All.Click += new System.Windows.RoutedEventHandler(this.SP_All_Click);
            
            #line default
            #line hidden
            return;
            case 20:
            this.SV_All = ((System.Windows.Controls.Button)(target));
            
            #line 67 "..\..\..\Cues\CuesGUI.xaml"
            this.SV_All.Click += new System.Windows.RoutedEventHandler(this.SV_All_Click);
            
            #line default
            #line hidden
            return;
            case 21:
            this.CuePrevious = ((System.Windows.Controls.Button)(target));
            
            #line 73 "..\..\..\Cues\CuesGUI.xaml"
            this.CuePrevious.Click += new System.Windows.RoutedEventHandler(this.goToPreviousCue_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.CueDecrement = ((System.Windows.Controls.Button)(target));
            
            #line 74 "..\..\..\Cues\CuesGUI.xaml"
            this.CueDecrement.Click += new System.Windows.RoutedEventHandler(this.CueDecrement_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            this.CueNumber = ((System.Windows.Controls.TextBox)(target));
            
            #line 75 "..\..\..\Cues\CuesGUI.xaml"
            this.CueNumber.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.CueNumber_PreviewMouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 24:
            this.CueIncrement = ((System.Windows.Controls.Button)(target));
            
            #line 76 "..\..\..\Cues\CuesGUI.xaml"
            this.CueIncrement.Click += new System.Windows.RoutedEventHandler(this.CueIncrement_Click);
            
            #line default
            #line hidden
            return;
            case 25:
            this.CueNext = ((System.Windows.Controls.Button)(target));
            
            #line 77 "..\..\..\Cues\CuesGUI.xaml"
            this.CueNext.Click += new System.Windows.RoutedEventHandler(this.goToNextCue_Click);
            
            #line default
            #line hidden
            return;
            case 26:
            this.sOvrd = ((StClient.Signal)(target));
            return;
            case 27:
            this.resetTrip = ((System.Windows.Controls.Button)(target));
            
            #line 88 "..\..\..\Cues\CuesGUI.xaml"
            this.resetTrip.Click += new System.Windows.RoutedEventHandler(this.resetTrip_Click);
            
            #line default
            #line hidden
            return;
            case 28:
            this.CuePrevious2 = ((System.Windows.Controls.Button)(target));
            
            #line 95 "..\..\..\Cues\CuesGUI.xaml"
            this.CuePrevious2.Click += new System.Windows.RoutedEventHandler(this.goToPreviousCue_Click);
            
            #line default
            #line hidden
            return;
            case 29:
            this.Play = ((System.Windows.Controls.Button)(target));
            
            #line 96 "..\..\..\Cues\CuesGUI.xaml"
            this.Play.Click += new System.Windows.RoutedEventHandler(this.Play_Click);
            
            #line default
            #line hidden
            return;
            case 30:
            this.CueNext2 = ((System.Windows.Controls.Button)(target));
            
            #line 97 "..\..\..\Cues\CuesGUI.xaml"
            this.CueNext2.Click += new System.Windows.RoutedEventHandler(this.goToNextCue_Click);
            
            #line default
            #line hidden
            return;
            case 31:
            this.Stop = ((System.Windows.Controls.Button)(target));
            
            #line 98 "..\..\..\Cues\CuesGUI.xaml"
            this.Stop.Click += new System.Windows.RoutedEventHandler(this.Stop_Click);
            
            #line default
            #line hidden
            return;
            case 32:
            this.StopAll = ((System.Windows.Controls.Button)(target));
            
            #line 99 "..\..\..\Cues\CuesGUI.xaml"
            this.StopAll.Click += new System.Windows.RoutedEventHandler(this.StopAll_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

