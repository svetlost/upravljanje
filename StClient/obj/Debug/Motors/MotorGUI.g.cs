﻿#pragma checksum "..\..\..\Motors\MotorGUI.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "C90CBC1B07F5414D5BC43A8DD74C1727"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using StClient;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace StClient {
    
    
    /// <summary>
    /// MotorGUI
    /// </summary>
    public partial class MotorGUI : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 33 "..\..\..\Motors\MotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.ToggleButton Cue;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\Motors\MotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock title;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\Motors\MotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Auto;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\Motors\MotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Stop;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\Motors\MotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock CP;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\Motors\MotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock CV;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\Motors\MotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock SP;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\Motors\MotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock SV;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\Motors\MotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock MV;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\Motors\MotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Up;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\Motors\MotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Release;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\Motors\MotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Down;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\Motors\MotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button resetTrip;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\Motors\MotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button resetGroup;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\Motors\MotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox combo;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\Motors\MotorGUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid CanOffGrid;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/StClient;component/motors/motorgui.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Motors\MotorGUI.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Cue = ((System.Windows.Controls.Primitives.ToggleButton)(target));
            return;
            case 2:
            this.title = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.Auto = ((System.Windows.Controls.Button)(target));
            
            #line 40 "..\..\..\Motors\MotorGUI.xaml"
            this.Auto.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Auto_PreviewMouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 4:
            this.Stop = ((System.Windows.Controls.Button)(target));
            
            #line 41 "..\..\..\Motors\MotorGUI.xaml"
            this.Stop.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Stop_PreviewMouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 5:
            this.CP = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.CV = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.SP = ((System.Windows.Controls.TextBlock)(target));
            
            #line 50 "..\..\..\Motors\MotorGUI.xaml"
            this.SP.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.SP_PreviewMouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 8:
            this.SV = ((System.Windows.Controls.TextBlock)(target));
            
            #line 54 "..\..\..\Motors\MotorGUI.xaml"
            this.SV.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.SV_PreviewMouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 9:
            this.MV = ((System.Windows.Controls.TextBlock)(target));
            
            #line 57 "..\..\..\Motors\MotorGUI.xaml"
            this.MV.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.MV_PreviewMouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 10:
            this.Up = ((System.Windows.Controls.Button)(target));
            
            #line 59 "..\..\..\Motors\MotorGUI.xaml"
            this.Up.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Up_PreviewMouseLeftButtonDown);
            
            #line default
            #line hidden
            
            #line 59 "..\..\..\Motors\MotorGUI.xaml"
            this.Up.PreviewMouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.Up_PreviewMouseLeftButtonUp);
            
            #line default
            #line hidden
            return;
            case 11:
            this.Release = ((System.Windows.Controls.Button)(target));
            
            #line 60 "..\..\..\Motors\MotorGUI.xaml"
            this.Release.Click += new System.Windows.RoutedEventHandler(this.Release_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.Down = ((System.Windows.Controls.Button)(target));
            
            #line 63 "..\..\..\Motors\MotorGUI.xaml"
            this.Down.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Down_PreviewMouseLeftButtonDown);
            
            #line default
            #line hidden
            
            #line 63 "..\..\..\Motors\MotorGUI.xaml"
            this.Down.PreviewMouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.Down_PreviewMouseLeftButtonUp);
            
            #line default
            #line hidden
            return;
            case 13:
            this.resetTrip = ((System.Windows.Controls.Button)(target));
            
            #line 64 "..\..\..\Motors\MotorGUI.xaml"
            this.resetTrip.Click += new System.Windows.RoutedEventHandler(this.resetTrip_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.resetGroup = ((System.Windows.Controls.Button)(target));
            
            #line 66 "..\..\..\Motors\MotorGUI.xaml"
            this.resetGroup.Click += new System.Windows.RoutedEventHandler(this.resetGroup_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.combo = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 16:
            this.CanOffGrid = ((System.Windows.Controls.Grid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

