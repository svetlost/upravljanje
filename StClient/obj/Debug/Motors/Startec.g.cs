﻿#pragma checksum "..\..\..\Motors\Startec.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "E15ED66E4A8AD87F36DC1491C5AC86FD"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace StClient {
    
    
    /// <summary>
    /// Startec
    /// </summary>
    public partial class Startec : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 20 "..\..\..\Motors\Startec.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock title;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\Motors\Startec.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox CHKGroup;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\Motors\Startec.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Up;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\Motors\Startec.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Down;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/StClient;component/motors/startec.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Motors\Startec.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.title = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 2:
            this.CHKGroup = ((System.Windows.Controls.CheckBox)(target));
            
            #line 30 "..\..\..\Motors\Startec.xaml"
            this.CHKGroup.Checked += new System.Windows.RoutedEventHandler(this.CHKGroup_Checked);
            
            #line default
            #line hidden
            
            #line 30 "..\..\..\Motors\Startec.xaml"
            this.CHKGroup.Unchecked += new System.Windows.RoutedEventHandler(this.CHKGroup_Unchecked);
            
            #line default
            #line hidden
            return;
            case 3:
            this.Up = ((System.Windows.Controls.Button)(target));
            
            #line 32 "..\..\..\Motors\Startec.xaml"
            this.Up.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Up_PreviewMouseLeftButtonDown);
            
            #line default
            #line hidden
            
            #line 32 "..\..\..\Motors\Startec.xaml"
            this.Up.PreviewMouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.Up_PreviewMouseLeftButtonUp);
            
            #line default
            #line hidden
            return;
            case 4:
            this.Down = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\..\Motors\Startec.xaml"
            this.Down.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Down_PreviewMouseLeftButtonDown);
            
            #line default
            #line hidden
            
            #line 33 "..\..\..\Motors\Startec.xaml"
            this.Down.PreviewMouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.Down_PreviewMouseLeftButtonUp);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

