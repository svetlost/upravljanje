﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Diagnostics;
using System.Windows.Threading;
using System.Xml.Linq;
using System.IO;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Sockets;
using System.ComponentModel;
using System.Reflection;
using LogAlertHB;


namespace Server
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private System.Windows.Forms.NotifyIcon TippuTrayNotify;
        private System.Windows.Forms.ContextMenuStrip ctxTrayMenu;
        AlertsListView _alv = new AlertsListView(1000);
        CanRx _toClient;
        Configs _config;
        CanTx _toCan;
        private Thread _rxThread;
        public Counters counters = new Counters();
        //IPEndPoint IpEpClientHeartBeat = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.ClientIP), Properties.Settings.Default.ClientHeartbeatPort);
        //IPEndPoint IpEpServerHeartBeat = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.ServerIP), Properties.Settings.Default.ServerHeartbeatPort);
        //IPEndPoint IpEpClientHeartBeat1 = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.ClientIP1), Properties.Settings.Default.ClientHeartbeatPort1);
        //IPEndPoint IpEpServerHeartBeat1 = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.ServerIP), Properties.Settings.Default.ServerHeartbeatPort1);
        //Heartbeat hb, hb1;


        public Window1()
        {
            InitializeComponent();

            #region MINIMIZE TO TRAY
            //Create an instance of the NotifyIcon Class
            TippuTrayNotify = new System.Windows.Forms.NotifyIcon();

            // This icon file needs to be in the bin folder of the application
            TippuTrayNotify.Icon = Properties.Resources.ikona_server_000;
            //new System.Drawing.Icon(Environment.CurrentDirectory + @"\Config\" + "App.ico");

            //show the Tray Notify Icon
            TippuTrayNotify.Visible = true;

            //Create a object for the context menu
            ctxTrayMenu = new System.Windows.Forms.ContextMenuStrip();

            //Add the Menu Item to the context menu

            System.Windows.Forms.ToolStripMenuItem mnuExit = new System.Windows.Forms.ToolStripMenuItem();
            mnuExit.Text = "Exit";
            mnuExit.Click += new EventHandler(mnuExit_Click);

            System.Windows.Forms.ToolStripMenuItem mnuRestore = new System.Windows.Forms.ToolStripMenuItem();
            mnuRestore.Text = "Restore";
            mnuRestore.Click += new EventHandler(mnuRestore_Click);

            ctxTrayMenu.Items.Add(mnuRestore);
            ctxTrayMenu.Items.Add(mnuExit);

            //Add the Context menu to the Notify Icon Object
            TippuTrayNotify.ContextMenuStrip = ctxTrayMenu;
            #endregion

            canvasXaml.Children.Add(_alv);


            AlertLogic.Add("Server Log started.");

            ServerLeft.DataContext = counters;

            if (Properties.Settings.Default.IsVectorCardWorking)
            {
                _config = new VectorConfig();
                _toCan = new VectorCanTx(_config.MotorsConfigurationList, counters);

            }
            else
            {
                _config = new PeakConfig();
                _toCan = new PeakCanTx(_config.MotorsConfigurationList, counters);
            }

            if (_config.CanDriverInitialized)
            {
                //hb = new Heartbeat(IpEpServerHeartBeat, "Server", IpEpClientHeartBeat, "Client", stopingAll);
                //hb1 = new Heartbeat(IpEpServerHeartBeat1, "Server", IpEpClientHeartBeat1, "Client1", stopingAll);
                //if (hb.UDPInitialized && hb1.UDPInitialized)
                {
                    //hb.StartServer();
                    //hb1.StartServer();

                    if (Properties.Settings.Default.IsVectorCardWorking)

                        _toClient = new VectorCanRx(counters);
                    else
                        _toClient = new PeakCanRx(counters);


                    ThreadStart threadDelegate = new System.Threading.ThreadStart(_toClient.CanRxLoop);
                    _rxThread = new System.Threading.Thread(threadDelegate);
                    _rxThread.IsBackground = true;
                    _rxThread.Name = "stRxThread";
                    _rxThread.Start();

                    //_rxThread = new Thread(new ThreadStart(_toClient.CanRxLoop));
                    //_rxThread.Start();

                    // _alv.Add("Secundary Break Control initialization started.");
                    // _toCan.SbcInit();

                }
                //else
                //    AlertLogic.Add("UDP initialization failed.");
            }
            else
            {
                AlertLogic.Add("Can Driver initialization failed.");
                Log.Write("Can Driver initialization failed. Closing driver.", EventLogEntryType.Error);
                _config.CloseCanDriver();
            }

        }

        public void stopingAll()
        {
            AlertLogic.Add("Stop all motors clicked");
            Log.Write("Stop all motors clicked.", EventLogEntryType.Information);

            _toCan.canStopAll();
        }
        void mnuRestore_Click(object sender, EventArgs e)
        {
            this.WindowState = WindowState.Maximized;
        }

        void mnuExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //mnuExit zove ovo 
        protected override void OnClosed(EventArgs e)
        {
            TippuTrayNotify.Visible = false;
            base.OnClosed(e);
        }

        private void StopAll_Click(object sender, RoutedEventArgs e)
        {
            _toCan.canStopAll();
        }

        private void SyncSend_Click(object sender, RoutedEventArgs e)
        {
            string s = "CanSync ";
            if (CanTx.SyncTimer.IsEnabled == false)
            {
                CanTx.SyncTimer.Start();
                SyncSend.Content = "Stop Can Sync";
                s += "stopped.";
            }
            else
            {
                CanTx.SyncTimer.Stop();
                SyncSend.Content = "Start Can Sync";
                s += "started.";
            }
            AlertLogic.Add(s);
            Log.Write(s, EventLogEntryType.Information);
        }



    }







}
