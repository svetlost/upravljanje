﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
//using System.Windows.Forms;
//using LogAlertHB;

namespace WagonTst001
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            init();
        }

        //VagonLogic003 vl54 = new VagonLogic003(54);
        VagonLogic003 vl_1L_46 = new VagonLogic003(46);

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
        #endregion
        #region gui
        Socket s;
        DispatcherTimer timer = new DispatcherTimer();
        void init()
        {
            timer.Tick += timer_Tick4;
            timer.Interval = TimeSpan.FromMilliseconds(200);

            s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
            //timer.Start();
        }
        private void bNP_1_Checked(object sender, RoutedEventArgs e) { bNZ_1.IsChecked = !bNP_1.IsChecked; }
        private void bNZ_1_Checked(object sender, RoutedEventArgs e) { bNP_1.IsChecked = !bNZ_1.IsChecked; }
        private void bSTOPALL_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            PackBytes(); data2[37] = 1; s.SendTo(data2, iep1);
            //Log.Write("Wagon 1-6: STOP pressed.");
        }

        private void bSTOPALL_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) { data2[37] = 0; s.SendTo(data2allzero, iep1); }

        private void bAUTO_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            PackBytes(); data2[38] = 1;
            s.SendTo(data2, iep1);
            string msg = "Wagon 1-6: AUTO pressed.";
            msg += string.Format("WAG1 FW={0}, BW={1}, GRP={2};", bNP_1.IsChecked, bNZ_1.IsChecked, bGRP_1.IsChecked);
            //Log.Write(msg);
        }

        private void bAUTO_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) { data2[38] = 0; s.SendTo(data2allzero, iep1); }

        byte[] data2 = new byte[100]{ 0x00, 0x2d, 0x53, 0x33, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x30, 0x00, 0x64, 0x00, 0xf6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00    };
        byte[] data2allzero = new byte[100]{ 0x00, 0x2d, 0x53, 0x33, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x30, 0x00, 0x64, 0x00, 0xf6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                      0x00, 0x00, 0x00, 0x00    };
        IPEndPoint iep1 = new IPEndPoint(IPAddress.Broadcast, 1202);
        void timer_Tick4(object sender, EventArgs e)
        {
            PackBytes();
            s.SendTo(data2, iep1);
        }

        void PackBytes()
        {
            data2[20] = (byte)((bool)bNP_1.IsChecked ? 1 : 0);
            data2[21] = (byte)((bool)bNZ_1.IsChecked ? 1 : 0);

            if ((bool)!bGRP_1.IsChecked) data2[20] = data2[21] = 0;

            data2[52] = data2allzero[52] = NumerizeTB(tbSV);

        }
        byte NumerizeTB(TextBox tbSV)
        {
            int i;
            try { i = int.Parse(tbSV.Text); }
            catch { i = 0; }

            if (i < 00 || i > 100) i = 0;
            //tbSV.Text = i.ToString();

            return (byte)i;
        }
        #endregion gui

        bool _bRotInn_M = false;
        public bool bRotInn_M { get { return _bRotInn_M; } set { if (_bRotInn_M != value) { _bRotInn_M = value; OnNotify("bRotInn_M"); Debug.WriteLine("_bRotInn_M=" + _bRotInn_M.ToString()); } } }
        private void tst_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { ((ToggleButton)sender).IsChecked = true; }
        private void tst_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) { ((ToggleButton)sender).IsChecked = false; }

      

    }

    public class VagonLogic003 : INotifyPropertyChanged
    {
        Socket sockRx, sockTx;
        IPEndPoint ipepClient, ipepPlc;
        EndPoint epClient;
        DispatcherTimer timerRx, timerTx;
        public const int msgLength = 32;
        byte[] data = new byte[msgLength];
        byte[] dataTx = new byte[msgLength];

        int _cp, _cv;
        int _sp = 500;
        int _sv = 10;
        UInt16 status1, status2;
        public int state, _Counter;
        public int CP { get { return _cp; } set { if (_cp != value) { _cp = value; OnNotify("CP"); } } }
        public int CV { get { return _cv; } set { if (_cv != value) { _cv = value; OnNotify("CV"); } } }
        public int SP { get { return _sp; } set { if (_sp != value) { _sp = value; OnNotify("SP"); } } }
        public int SV { get { return _sv; } set { if (_sv != value) { _sv = value; OnNotify("SV"); } } }
        public double commandGo0, commandGo1, commandGo2, commandStop, commandGoSP;
        public int Counter { get { return _Counter; } set { if (_Counter != value) { _Counter = value; OnNotify("Counter"); } } }
        bool _brake, _trip, _inh28, _refok, _limneg, _limpos, _local;
        public bool Brake { get { return _brake; } set { if (_brake != value) { _brake = value; OnNotify("Brake"); } } }
        public bool Trip { get { return _trip; } set { if (_trip != value) { _trip = value; OnNotify("Trip"); } } }
        public bool Inh28 { get { return _inh28; } set { if (_inh28 != value) { _inh28 = value; OnNotify("Inh28"); } } }
        public bool RefOk { get { return _refok; } set { if (_refok != value) { _refok = value; OnNotify("RefOk"); } } }
        public bool LimNeg { get { return _limneg; } set { if (_limneg != value) { _limneg = value; OnNotify("LimNeg"); } } }
        public bool LimPos { get { return _limpos; } set { if (_limpos != value) { _limpos = value; OnNotify("LimPos"); } } }
        public bool Local { get { return _local; } set { if (_local != value) { _local = value; OnNotify("Local"); } } }

        int _addr, _tempAddress, i;
        public double Addr { get { return _addr; } }

        public VagonLogic003(int addr)
        {
            _addr = addr;

            sockRx = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            sockTx = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            //ipepClient = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.LocalIp), 3510);
            ipepClient = new IPEndPoint(IPAddress.Parse("10.4.4.40"), 3510);
            epClient = (EndPoint)ipepClient;

            ipepPlc = new IPEndPoint(IPAddress.Parse("10.4.4.54"), 4500);


            try { sockRx.Bind(ipepClient); }
            catch { }

            timerRx = new DispatcherTimer();
            timerRx.Tick += timerRx_Tick;
            timerRx.Interval = TimeSpan.FromMilliseconds(100);
            timerRx.Start();

            timerTx = new DispatcherTimer();
            timerTx.Tick += timerTx_Tick;
            timerTx.Interval = TimeSpan.FromMilliseconds(200);
            //timerTx.Start();
        }

        void timerRx_Tick(object sender, EventArgs e)
        {
            try
            {
                while (sockRx.Available > 0)
                {
                    sockRx.ReceiveFrom(data, ref epClient);

                    _tempAddress = (ushort)(data[0] + 256 * data[1]);
                    status1 = (ushort)(data[2] + 256 * data[3]);
                    status2 = (ushort)(data[4] + 256 * data[5]);
                    state = (ushort)(data[6] + 256 * data[7]);
                    //CP = (ushort)(data[8] + 256 * data[9]);
                    //CV = (ushort)(data[10] + 256 * data[11]);
                    CP = BitConverter.ToInt16(new byte[] { data[8], data[9] }, 0);
                    CV = BitConverter.ToInt16(new byte[] { data[10], data[11] }, 0);
                    //CV = (ushort)(data[10] + 256 * data[11]);
                    Counter = (ushort)(data[30] + 256 * data[31]);
                    Trip = Sys.getBitOfInt(status1, 2);
                    Brake = Sys.getBitOfInt(status1, 3);
                    Inh28 = Sys.getBitOfInt(status1, 4);
                    RefOk = Sys.getBitOfInt(status1, 5);
                    LimNeg = Sys.getBitOfInt(status1, 6);
                    LimPos = Sys.getBitOfInt(status1, 7);
                    Local = Sys.getBitOfInt(status1, 8);
                    if (_tempAddress == _addr)
                        Debug.WriteLine(string.Format("adress={0} counter={1:D6} cp={2:D4} cv={3:D3} status1={4:D5} state={5} trip={6} brk={7} inh={8} limneg={9} limpos={10}", _tempAddress, Counter, CP, CV, status1, state, Trip, Brake, Inh28, LimNeg, LimPos));
                }
            }
            catch { };
        }

        void timerTx_Tick(object sender, EventArgs e)
        {
            dataTx[0] = 0; dataTx[1] = 0;//adresa
            dataTx[2] = 0; dataTx[3] = 0;
            dataTx[4] = 0; dataTx[5] = 0;
            dataTx[6] = 0; dataTx[7] = 0;
            dataTx[8] = BitConverter.GetBytes(SP)[0]; dataTx[9] = BitConverter.GetBytes(SP)[1];//sp
            dataTx[10] = BitConverter.GetBytes(SV)[0]; dataTx[11] = BitConverter.GetBytes(SV)[1]; //sv
            dataTx[12] = BitConverter.GetBytes(commandGo0)[0]; dataTx[13] = 0;//park, go0
            dataTx[14] = BitConverter.GetBytes(commandGo1)[0]; dataTx[15] = 0;//stage, go1
            dataTx[16] = BitConverter.GetBytes(commandGo2)[0]; dataTx[17] = 0;//go prosc (rotation only)
            dataTx[18] = BitConverter.GetBytes(commandStop)[0]; dataTx[19] = 0;//stop
            dataTx[20] = BitConverter.GetBytes(commandGoSP)[0]; dataTx[21] = 0;//goSP


            try { sockTx.SendTo(dataTx, ipepPlc); }
            catch { }

        }

        public void tstTx()
        {
            try { sockTx.SendTo(new byte[] { (byte)i++, 0, 0 }, ipepPlc); }
            catch { }
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
        #endregion

    }

    public class Sys
    {
        public static bool getBitOfInt(int i, int bitNumber) { return (i & (1 << bitNumber)) != 0; }
    }
}
