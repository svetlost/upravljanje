﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Sharpduino;
using Sharpduino.Constants;

namespace LampCntrl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            ard = new ArduinoUno(Properties.Settings.Default.ArdComPort);
            //ard = new ArduinoUno("COM2");
            //ard = new ArduinoUno("COM4");

            //ard.i
            //ard.SetPinMode(ArduinoUnoPins.D4, PinModes.Output);
            //ard.SetDO(Sharpduino.Constants.ArduinoUnoPins.D4, true);
            //ard.SetDO(Sharpduino.Constants.ArduinoUnoPins.D4, false);
            //ard.SetPinMode(ArduinoUnoPins.D7, PinModes.Output);
            //ard.SetDO(Sharpduino.Constants.ArduinoUnoPins.D7, false);

            ard.SetPinMode(ArduinoUnoPins.D3_PWM, PinModes.PWM);
            ard.SetPWM(Sharpduino.Constants.ArduinoUnoPWMPins.D3_PWM, 0);
            ard.SetPinMode(ArduinoUnoPins.D11_PWM, PinModes.PWM);
            ard.SetPWM(Sharpduino.Constants.ArduinoUnoPWMPins.D11_PWM, 0);
            ard.SetPinMode(ArduinoUnoPins.D3_PWM, PinModes.PWM);
            ard.SetPinMode(ArduinoUnoPins.D11_PWM, PinModes.PWM);
        }

        ArduinoUno ard;

        private void lamp0_Click(object sender, RoutedEventArgs e)
        {
            ard.SetPinMode(ArduinoUnoPins.D11_PWM, PinModes.PWM);
            ard.SetPinMode(ArduinoUnoPins.D3_PWM, PinModes.PWM);
            slLamp.Value = 0;
            //ard.SetPinMode(ArduinoUnoPins.D11_PWM, PinModes.PWM);
            //ard.SetPinMode(ArduinoUnoPins.D3_PWM, PinModes.PWM);
            //ard.SetPWM(Sharpduino.Constants.ArduinoUnoPWMPins.D11_PWM, 0);
            //ard.SetPWM(Sharpduino.Constants.ArduinoUnoPWMPins.D3_PWM, 0);
            ////ard.SetDO(Sharpduino.Constants.ArduinoUnoPins.D3_PWM, false);
            ////ard.SetDO(Sharpduino.Constants.ArduinoUnoPins.D11_PWM, false);
        }

        private void lamp100_Click(object sender, RoutedEventArgs e)
        {
            ard.SetPinMode(ArduinoUnoPins.D11_PWM, PinModes.PWM);
            ard.SetPinMode(ArduinoUnoPins.D3_PWM, PinModes.PWM);
            slLamp.Value = 120;
            //ard.SetPinMode(ArduinoUnoPins.D11_PWM, PinModes.PWM);
            //ard.SetPinMode(ArduinoUnoPins.D3_PWM, PinModes.PWM);
            //ard.SetPWM(Sharpduino.Constants.ArduinoUnoPWMPins.D11_PWM, 99);
            //ard.SetPWM(Sharpduino.Constants.ArduinoUnoPWMPins.D3_PWM, 99);
            ////ard.SetDO(Sharpduino.Constants.ArduinoUnoPins.D3_PWM, true);
            ////ard.SetDO(Sharpduino.Constants.ArduinoUnoPins.D11_PWM, true);
        }

        private void lampFull_Click(object sender, RoutedEventArgs e)
        {
            ard.SetPinMode(ArduinoUnoPins.D11_PWM, PinModes.PWM);
            ard.SetPinMode(ArduinoUnoPins.D3_PWM, PinModes.PWM);
            slLamp.Value = 254;
            //ard.SetPinMode(ArduinoUnoPins.D11_PWM, PinModes.PWM);
            //ard.SetPinMode(ArduinoUnoPins.D3_PWM, PinModes.PWM);
            //ard.SetPWM(Sharpduino.Constants.ArduinoUnoPWMPins.D11_PWM, 254);
            //ard.SetPWM(Sharpduino.Constants.ArduinoUnoPWMPins.D3_PWM, 254);
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //ard.SetPinMode(ArduinoUnoPins.D11_PWM, PinModes.PWM);
            //ard.SetPinMode(ArduinoUnoPins.D3_PWM, PinModes.PWM);
            ard.SetPWM(Sharpduino.Constants.ArduinoUnoPWMPins.D11_PWM, Convert.ToInt32(slLamp.Value));
            ard.SetPWM(Sharpduino.Constants.ArduinoUnoPWMPins.D3_PWM, Convert.ToInt32(slLamp.Value));

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) { ard.Dispose(); }

        //private void btUp_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { ard.SetDO(Sharpduino.Constants.ArduinoUnoPins.D4, true); }
        //private void btUp_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) { ard.SetDO(Sharpduino.Constants.ArduinoUnoPins.D4, false); }

        //private void btDown_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { ard.SetDO(Sharpduino.Constants.ArduinoUnoPins.D7, true); }
        //private void btDown_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) { ard.SetDO(Sharpduino.Constants.ArduinoUnoPins.D7, false); }


    }
}
